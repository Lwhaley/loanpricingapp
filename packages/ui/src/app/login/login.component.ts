import { Component } from '@angular/core'
import { Router } from '@angular/router'

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent {
  readonly isGuestMode = process.env.NODE_ENV === 'production'

  constructor(private readonly router: Router) {}

  skipLogin(): void {
    console.log('test')
    this.router.navigate(['authenticated'])
  }
}
