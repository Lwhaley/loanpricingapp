import { Component, OnInit } from '@angular/core'
import { Router } from '@angular/router'

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.scss'],
})
export class LoginFormComponent implements OnInit {
  readonly isGuestMode = process.env.NODE_ENV !== 'production'

  constructor(private readonly router: Router) {}

  skipLogin(): void {
    this.router.navigate(['authenticated'])
  }

  ngOnInit(): void {}
}
