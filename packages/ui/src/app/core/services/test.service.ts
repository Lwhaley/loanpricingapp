import { HttpClient } from '@angular/common/http'
import { Injectable } from '@angular/core'
import { Observable } from 'rxjs'

@Injectable({
  providedIn: 'root',
})
export class TestService {
  constructor(private readonly http: HttpClient) {}

  testApiCall(): Observable<any> {
    return this.http.get('/api/v1')
  }

  testApiPostCall(): Observable<any> {
    return this.http.post('/api/v1', { name: 'John Doe' })
  }
}
