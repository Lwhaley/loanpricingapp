import {
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
} from '@angular/common/http'
import { Injectable } from '@angular/core'
import { Observable } from 'rxjs'

import { environment } from '../../../environments/environment'

interface IEnvironment {
  production: boolean
  mockApiServer?: string
}

@Injectable()
export class MockApiInterceptor implements HttpInterceptor {
  constructor() {
    if (!environment.production) {
      console.log(
        '>> Mock interceptor enabled...:',
        (environment as IEnvironment).mockApiServer,
      )
    }
  }

  intercept(
    request: HttpRequest<any>,
    next: HttpHandler,
  ): Observable<HttpEvent<any>> {
    let newReq = request
    if (!environment.production && !request.url.startsWith('/assets/i18n')) {
      newReq = request.clone({
        url: `${(environment as IEnvironment).mockApiServer}${request.url}`,
      })
    }

    return next.handle(newReq)
  }
}
