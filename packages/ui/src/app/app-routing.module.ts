import { NgModule } from '@angular/core'
import { RouterModule, Routes } from '@angular/router'

const routes: Routes = [
  {
    path: 'login',
    loadChildren: () => import('./login/login.module').then(m => m.LoginModule),
  },
  {
    path: 'authenticated',
    loadChildren: () =>
      import('./authenticated/authenticated.module').then(
        m => m.AuthenticatedModule,
      ),
  },
  {
    path: '**',
    redirectTo: 'authenticated',
    pathMatch: 'full',
  },
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
