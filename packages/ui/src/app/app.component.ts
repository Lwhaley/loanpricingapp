import { Component, OnInit } from '@angular/core'
import { tap } from 'rxjs/operators'
import { TestService } from './core/services/test.service'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  title = 'ui'

  constructor(readonly test: TestService) {}

  ngOnInit(): void {
    //   this.test
    //     .testApiCall()
    //     .pipe(tap(x => console.log('GET response', x)))
    //     .subscribe()
    //   this.test
    //     .testApiPostCall()
    //     .pipe(tap(x => console.log('POST response', x)))
    //     .subscribe()
  }
}
