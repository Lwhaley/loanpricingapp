import { Component } from '@angular/core'
import { ActivatedRoute, Router } from '@angular/router'
import html2canvas from 'html2canvas'
import jspdf from 'jspdf'

// Quality Assurance
let localURL = 'https://pricingqa.azurewebsites.net/api/v1/'

// UAT
if (window.location.href.startsWith('https://pricinguat')) {
  localURL = 'https://pricinguat.azurewebsites.net/api/v1/'
}

// Production
if (window.location.href.startsWith('https://pricingprod')) {
  localURL = 'https://pricingprod.azurewebsites.net/api/v1/'
}

// Local
if (window.location.href.startsWith('http://localhost')) {
  localURL = 'http://localhost:3000/api/v1/'
}

@Component({
  selector: 'app-rental30',
  templateUrl: './rental30.component.html',
  styleUrls: ['./rental30.component.scss'],
})
export class Rental30Component {
  // Page Controls
  CurrentDate: any
  savable: any
  printable: any
  output: any
  outputs: any
  premierPricing: any
  overrideMaster: any
  ltvOptions: any
  calcModel: any
  calcModelTemplate: any
  quoteModel: any
  quoteModelTemplate: any
  updateModel: any
  currentQuote: any
  username: any
  quoteName: any
  validationErrors: any
  loading: any
  frontEndValidation: any
  quoteType: any
  productNode: any
  submissionNode: any
  rateBuyUpDownNode: any
  prepaymentPenaltyNode: any
  includeNode1: any
  includeNode2: any
  includeNode3: any
  columnWidth: any
  mainColumn: any
  arm: any
  whiteLabel: any
  previewRental: any
  previewPremier: any
  premierNode: any

  productTypeSelected: any
  productTypePicklist: any
  submissionTypeSelected: any
  submissionTypePicklist: any
  borrowingEntity: any
  borrowerNetWorth: any
  fico: any
  ficoWarning: any
  sreoPropertyNumber: any
  numberOfProperties: any
  timeOwnedSelected: any
  timeOwnedPicklist: any
  totalCosts: any
  propertyLessThanTenThousand: any
  propertyLessThanTenThousandNode: any
  purchasePrice: any
  condo: any
  condoNode: any
  mortgagePayoffs: any
  monthlyRent: any
  annualTaxes: any
  annualInsurance: any
  annualFloodInsurance: any
  annualHOA: any
  propertiesNY: any
  propertiesNYNode: any
  fix2Rent: any
  fix2RentNode: any
  referral: any
  referralNode: any
  yieldSpreadPremium: any
  yieldSpreadPremiumNode: any
  yieldSpreadPicklist: any
  refinanceL1CFixNFlip: any
  refinanceL1CFixNFlipNode: any
  buyUpDownSelected: any
  buyUpDownPicklist: any
  buyUpDownPercentage: any
  buyUpDown: any
  prePaymentPenaltySelected: any
  prepaymentPenaltyPicklist: any
  brokerFee: any
  brokerFee1: any
  brokerProcessingFee: any
  appraisalFee: any

  ltvPercentage1: any
  include1: any
  ltvPercentage2: any
  include2: any
  ltvPercentage3: any
  include3: any
  includeOptions: any

  ltvEligibility: any
  borrower: any
  estimatedValueOfProperty: any
  // numberOfProperties: any
  estimatedLoanToValue: any
  borrowerCreditScore: any
  loanAmount: any
  payoffOfMortgageOnProperties: any
  purchasePriceOfProperties: any
  estimatedClosingCosts: any
  originationFee: any
  buyUpbuyDownSelected: any
  buyUpbuyDownPicklist: any
  processingFee: any
  floodTaxInsuranceCertification: any
  estimatedTaxImpound: any
  estimatedInsuranceImpound: any
  interestReserveEscrow: any
  cashToBorrowerAtClose: any
  qualifyingInterestRate: any
  rateUpRateDown: any
  prepaymentPenalty: any
  interestRate: any
  termAndAmortizationRate: any
  montlyPrincipleAndInterest: any
  monthlyTaxesInsurance: any
  monthlyHOA: any
  PITI: any
  estimatedMonthlyRentalIncome: any
  rentToDebtServiceRatio: any
  lifetimeCap: any
  floor: any
  // prePaymentPentalty: any
  prepayOption: any
  guarantyType: any
  requiredReserves: any

  // Premier Pricing Fields
  propertyManagementFee: any
  greaterThanTwentyFiveCollateralCondos: any
  greaterThanTwentyFiveCollateralCondosLessThanSeventyFiveThousand: any
  greaterThanTwentyFiveCollateralCondosLessThanSeventyFiveThousandNode: any
  estimatedManagementReserve: any
  legalFee: any
  spread: any
  indexSelected: any
  indexPicklist: any
  indexRate: any

  // Outputs
  id: any
  workbookId: any
  validQuote: any
  lastUpdated: any
  termSheetTitle: any
  estimatedValOfProperty: any
  numOfProperties: any
  totalCosts1: any
  totalCosts2: any
  appraisedVal1: any
  appraisedVal2: any
  appraisedVal3: any
  estimatedLoanToVal: any
  loanAmt: any
  payoffMortageProperties: any
  purchasePriceProperties: any
  estimatedClosingCost: any
  buyUpDownNotOpted: any
  insuranceCertifications: any
  taxImpound: any
  insuranceImpound: any
  closingCashToBorrower: any
  rateBuyUpDown: any
  initFixedInterestRate: any
  amortizationPeriod: any
  monthlyPrincipalInterest: any
  monthlyHOADues: any
  monthlyTotalPayment: any
  monthlyEstimatedRentalIncome: any
  rentDebtRatio: any
  capStructure: any
  lookbackPeriod: any
  prepaymentPenaltyText: any
  prepaymentPenaltyArray: any
  openPrepayOptionPicklist: any
  openPrepayOptionSelected: any
  openPrepayOptionSelected1: any
  liquidReserves: any
  cashToFrom: any
  rate: any
  buyUpDownFee: any
  rdsr: any
  margin: any
  eocTitle: any

  ltvEligibility2: any
  eocTitle2: any
  borrower2: any
  estimatedValOfProperty2: any
  numOfProperties2: any
  estimatedLoanToVal2: any
  borrowerCreditScore2: any
  loanAmt2: any
  payoffMortageProperties2: any
  purchasePriceProperties2: any
  estimatedClosingCost2: any
  originationFee2: any
  buyUpDownNotOpted2: any
  processingFee2: any
  legalFee2: any
  brokerFee2: any
  insuranceCertifications2: any
  taxImpound2: any
  insuranceImpound2: any
  interestReserveEscrow2: any
  closingCashToBorrower2: any
  qualifyingInterestRate2: any
  rateBuyUpDown2: any
  prepaymentPenalty2: any
  initFixedInterestRate2: any
  amortizationPeriod2: any
  monthlyPrincipalInterest2: any
  monthlyTaxesInsurance2: any
  monthlyHOADues2: any
  monthlyTotalPayment2: any
  monthlyEstimatedRentalIncome2: any
  rentDebtRatio2: any
  capStructure2: any
  lifetimeCap2: any
  floor2: any
  lookbackPeriod2: any
  prepaymentPenaltyText2: any
  openPrepayOption2: any
  guarantyType2: any
  liquidReserves2: any
  buyUpDownFee2: any
  margin2: any

  ltvEligibility3: any
  eocTitle3: any
  borrower3: any
  estimatedValOfProperty3: any
  numOfProperties3: any
  estimatedLoanToVal3: any
  borrowerCreditScore3: any
  loanAmt3: any
  payoffMortageProperties3: any
  purchasePriceProperties3: any
  estimatedClosingCost3: any
  originationFee3: any
  buyUpDownNotOpted3: any
  processingFee3: any
  legalFee3: any
  brokerFee3: any
  insuranceCertifications3: any
  taxImpound3: any
  insuranceImpound3: any
  interestReserveEscrow3: any
  closingCashToBorrower3: any
  qualifyingInterestRate3: any
  rateBuyUpDown3: any
  prepaymentPenalty3: any
  initFixedInterestRate3: any
  amortizationPeriod3: any
  monthlyPrincipalInterest3: any
  monthlyTaxesInsurance3: any
  monthlyHOADues3: any
  monthlyTotalPayment3: any
  monthlyEstimatedRentalIncome3: any
  rentDebtRatio3: any
  capStructure3: any
  lifetimeCap3: any
  floor3: any
  lookbackPeriod3: any
  prepaymentPenaltyText3: any
  openPrepayOption3: any
  guarantyType3: any
  liquidReserves3: any
  buyUpDownFee3: any
  margin3: any
  interestRateValue1: any
  interestRateValue2: any
  interestRateValue3: any
  spread1: any
  spread2: any
  spread3: any
  interestRateName1: any
  openPrepay1: any
  indexRate1: any
  interestRateName2: any
  openPrepay2: any
  indexRate2: any
  interestRateName3: any
  openPrepay3: any
  indexRate3: any
  dateRate1: any
  dateRate2: any
  dateRate3: any
  index1: any
  index2: any
  index3: any

  /**
   * method abs()
   *
   * returns the absolute value of an integer
   *
   * @param value integer
   * @returns float
   */
  abs(value: any): any {
    const absValue = Math.abs(value)
    return absValue
  }

  /**
   * method updatePrepay()
   *
   * Updates the prepayment penalty checklist when product type
   * changed
   */
  updatePrepay(): void {
    switch (this.productTypeSelected) {
      case '5/1 ARM':
        this.prepaymentPenaltyPicklist = [
          '3 Year Prepayment Penalty: 3, 2, 1',
          '5 Year Prepayment Penalty: 3, 3, 3, 2, 1',
          '7 Year Prepayment Penalty: 5, 5, 4, 4, 3, 2, 1',
        ]
        // this.prePaymentPenaltySelected = '3 Year Prepayment Penalty: 3, 2, 1'
        break
      case '10/1 ARM':
      case 'Fixed':
        this.prepaymentPenaltyPicklist = [
          '3 Year Prepayment Penalty: 5, 4, 3',
          '5 Year Prepayment Penalty: 5, 4, 3, 2, 1',
          '7 Year Prepayment Penalty: 5, 5, 4, 4, 3, 2, 1',
        ]
        // this.prePaymentPenaltySelected = '3 Year Prepayment Penalty: 5, 4, 3'
        break
      case 'Premier - 5/1 ARM':
      case 'Premier - 5 Year Balloon':
      case 'Premier - 5 Year, 30 Am':
        this.prepaymentPenaltyPicklist = [
          'Yield Spread Maintenance',
          '5 Year Prepayment Penalty: 5, 5, 4, 4, 3',
          '5 Year Prepayment Penalty: 5, 4, 3, 2, 1',
        ]
        // this.prePaymentPenaltySelected = 'Yield Spread Maintenance'
        break
      case 'Premier - 10 Year Balloon':
      case 'Premier - 10 Year, 30 Am':
      case 'Premier - 30 Year Fixed':
        this.prepaymentPenaltyPicklist = [
          'Yield Spread Maintenance',
          '10 Year Prepayment Penalty: 5, 5, 4, 4, 3, 3, 2, 2, 1, 1',
          '7 Year Prepayment Penalty: 5, 5, 4, 4, 3, 2, 1',
        ]
        // this.prePaymentPenaltySelected = 'Yield Spread Maintenance'
        break
      default:
      // Do nothing
    }
  }

  /**
   * method updateCheckbox()
   *
   * alters the value of the checkbox object based on id
   *
   * @param event object
   */
  updateCheckbox(event: any): void {
    let checked = 'No'
    if (event.checked) {
      checked = 'Yes'
    }

    switch (event.id) {
      case 'refinanceL1CFixNFlip':
        this.refinanceL1CFixNFlip = checked
        break
      case 'propertyLessThanTenThousand':
        this.propertyLessThanTenThousand = checked
        break
      case 'greaterThanTwentyFiveCollateralCondosLessThanSeventyFiveThousand':
        this.greaterThanTwentyFiveCollateralCondosLessThanSeventyFiveThousand = checked
        break
      case 'propertiesNY':
        this.propertiesNY = checked
        break
      case 'referral':
        this.referral = checked
        break
      case 'condo':
        this.condo = checked
        break
      case 'greaterThanTwentyFiveCollateralCondos':
        this.greaterThanTwentyFiveCollateralCondos = checked
        break
      case 'fix2Rent':
        this.fix2Rent = checked
        break
      default:
      // Do nothing
    }

    this.unsavable()
  }

  /**
   * method unsavable()
   *
   * sets the value of savable to false
   */
  unsavable(): void {
    this.savable = false
  }

  /**
   * method triggerPrimaryOverride()
   *
   * toggles the primary override value
   */
  triggerPrimaryOverride(): void {
    this.overrideMaster = !this.overrideMaster
  }

  /**
   * method checkPricing()
   *
   * updates the view with the appropriate product picklist
   * for premium vs. rental quotes
   *
   * @param event object
   */
  checkPricing(event: any): void {
    this.premierPricing = event.checked
    this.updatePricing()
    if (this.premierPricing) {
      this.productTypeSelected = 'Premier - 5 Year, 30 Am'
    }
    this.updatePrepay()
  }

  /**
   * method updatePricing()
   *
   * Updates the pricing dropdown fields to match premier or rental
   */
  updatePricing(): void {
    this.setBase()
    if (this.premierPricing) {
      this.setPremier()
    }
  }

  /**
   * method setBase()
   *
   * Updates the product picklist on load
   */
  setBase(): void {
    this.productTypePicklist = ['5/1 ARM', '10/1 ARM', 'Fixed']
    this.productTypeSelected = '5/1 ARM'
  }

  /**
   * method setPremier()
   *
   * sets product type picklist on load
   */
  setPremier(): void {
    this.productTypePicklist = [
      'Premier - 5 Year, 30 Am',
      'Premier - 10 Year, 30 Am',
      'Premier - 5 Year Balloon',
      'Premier - 10 Year Balloon',
      'Premier - 5/1 ARM',
      'Premier - 30 Year Fixed',
    ]
  }

  /**
   * method formatPrepayText()
   *
   * splits the prepayment penalty string by bullet points
   * for display as an unordered list
   */
  formatPrepayText(): void {
    this.prepaymentPenaltyArray = this.prepaymentPenaltyText.split('\u2022')
  }

  /**
   * method selectInclude()
   *
   * stores the value of the selected scenario option into the
   * object attribute
   *
   * @param scenario string
   * @param event object
   */
  selectInclude(scenario: any, event: any): void {
    switch (scenario) {
      case 1:
        this.include1 = event.target.value
        break
      case 2:
        this.include2 = event.target.value
        break
      case 3:
        this.include3 = event.target.value
        break
      default:
      // Do nothing
    }
    this.savable = false
    this.printable = false
  }

  /**
   * method setValue()
   *
   * sets the value of the given attribute based on the dropdown option
   * passed in the event object
   *
   * @param node string
   * @param event object
   */
  setValue(node: any, event: any): void {
    switch (node) {
      case 'index':
        this.indexSelected = event.target.value
        break
      case 'product':
        this.productTypeSelected = event.target.value
        break
      case 'prepay':
        this.openPrepayOptionSelected = event.target.value
        break
      case 'submission':
        this.submissionTypeSelected = event.target.value
        break
      case 'buyUpDown':
        this.buyUpDownSelected = event.target.value
        break
      case 'yield':
        this.yieldSpreadPremium = event.target.value
        break
      case 'prepayPenalty':
        this.prePaymentPenaltySelected = event.target.value
        break
      default:
      // Do Nothing
    }
    this.savable = false
    this.printable = false
    this.updatePrepay()
  }

  /**
   * method setCalcModel()
   *
   * sets the values of the calcModel attribute in preparation for
   * the calculation payload
   */
  setCalcModel(): void {
    this.calcModel = JSON.parse(JSON.stringify(this.calcModelTemplate))

    switch (this.premierPricing) {
      case true:
        delete this.calcModel[0].base
        delete this.calcModel[0].openPrepayOption
        this.calcModel[0].premier.propertyManagementFee = Number(
          this.propertyManagementFee,
        )
        this.calcModel[0].premier.collateralCondos = this.greaterThanTwentyFiveCollateralCondos
        this.calcModel[0].premier.collateral = this.greaterThanTwentyFiveCollateralCondosLessThanSeventyFiveThousand
        this.calcModel[0].premier.index = this.indexSelected
        this.calcModel[0].quoteType = 'Premier'
        this.calcModel[0].premier.openPrepayOption = this.openPrepayOptionSelected
        break
      default:
        delete this.calcModel[0].premier
        this.calcModel[0].base.condo = this.condo
        this.calcModel[0].base.fixToRent = this.fix2Rent
        this.calcModel[0].base.propertyVal = this.propertyLessThanTenThousand
        this.calcModel[0].quoteType = 'Rental'
    }

    // First Level Values
    this.calcModel[0].username = this.username
    this.calcModel[0].quoteName = this.quoteName
    this.calcModel[0].annualFloodInsurance = Number(this.annualFloodInsurance)
    this.calcModel[0].annualHOA = Number(this.annualHOA)
    this.calcModel[0].annualInsurance = Number(this.annualInsurance)
    this.calcModel[0].annualTaxes = Number(this.annualTaxes)
    this.calcModel[0].borrowerIsReferral = this.referral
    this.calcModel[0].borrowerNetWorth = Number(this.borrowerNetWorth)
    this.calcModel[0].borrowingEntity = this.borrowingEntity
    this.calcModel[0].brokerFee = Number(this.brokerFee)
    this.calcModel[0].buyUpDownOption = this.buyUpDownSelected
    this.calcModel[0].buyUpDownPercentage = Number(this.buyUpDownPercentage)
    this.calcModel[0].creditScore = Number(this.fico)
    this.calcModel[0].monthlyRent = Number(this.monthlyRent)
    this.calcModel[0].mortagePayoffs = Number(this.mortgagePayoffs)
    this.calcModel[0].numOfProperties = Number(this.numberOfProperties)
    this.calcModel[0].totalCosts1 = Number(this.totalCosts1)
    this.calcModel[0].totalCosts2 = Number(this.totalCosts2)
    this.calcModel[0].appraisedVal1 = Number(this.appraisedVal1)
    this.calcModel[0].appraisedVal2 = Number(this.appraisedVal2)
    this.calcModel[0].appraisedVal3 = Number(this.appraisedVal3)
    this.calcModel[0].purchasePrice = Number(this.purchasePrice)
    this.calcModel[0].prepaymentPenalty = this.prePaymentPenaltySelected
    this.calcModel[0].productType = this.productTypeSelected
    this.calcModel[0].refiL1cFixNFlip = this.refinanceL1CFixNFlip
    this.calcModel[0].submissionType = this.submissionTypeSelected
    this.calcModel[0].propertiesNY = this.propertiesNY
    this.calcModel[0].yieldSpendPremium = Number(this.yieldSpreadPremium)
    this.calcModel[0].sreoPropertyNumber = Number(this.sreoPropertyNumber)
    this.calcModel[0].brokerProcessingFee = Number(this.brokerProcessingFee)
    this.calcModel[0].appraisalFee = Number(this.appraisalFee)

    // Scenarios
    this.calcModel[0].scenario[0].ltvPercentage = Number(this.ltvPercentage1)
    this.calcModel[0].scenario[0].included = this.include1
    this.calcModel[0].scenario[1].ltvPercentage = Number(this.ltvPercentage2)
    this.calcModel[0].scenario[1].included = this.include2
    this.calcModel[0].scenario[2].ltvPercentage = Number(this.ltvPercentage3)
    this.calcModel[0].scenario[2].included = this.include3
  }

  /**
   * method setQuoteModel()
   *
   * Sets the values of the quoteModel attributes in preparation for the
   * save/update quote methods
   */
  setQuoteModel(): void {
    this.quoteModel = JSON.parse(JSON.stringify(this.quoteModelTemplate))

    this.quoteModel[0].in = this.calcModel[0]

    this.quoteModel[0].out.workbookId = this.outputs.out.workbookId
    this.quoteModel[0].out.validQuote = this.outputs.out.validQuote
    this.quoteModel[0].out.lastUpdated = this.outputs.out.lastUpdated
    this.quoteModel[0].out.termSheetTitle = this.outputs.out.termSheetTitle

    // Scenarios
    this.quoteModel[0].out.scenarios[0].ltvEligibility = this.outputs.out.scenarios[0].ltvEligibility
    this.quoteModel[0].out.scenarios[0].eocTitle = this.outputs.out.scenarios[0].eocTitle
    this.quoteModel[0].out.scenarios[0].borrower = this.outputs.out.scenarios[0].borrower
    this.quoteModel[0].out.scenarios[0].estimatedValOfProperty = this.outputs.out.scenarios[0].estimatedValOfProperty
    this.quoteModel[0].out.scenarios[0].numOfProperties = this.outputs.out.scenarios[0].numOfProperties
    this.quoteModel[0].out.scenarios[0].estimatedLoanToVal = this.outputs.out.scenarios[0].estimatedLoanToVal
    this.quoteModel[0].out.scenarios[0].borrowerCreditScore = this.outputs.out.scenarios[0].borrowerCreditScore
    this.quoteModel[0].out.scenarios[0].loanAmt = this.outputs.out.scenarios[0].loanAmt
    this.quoteModel[0].out.scenarios[0].payoffMortageProperties = this.outputs.out.scenarios[0].payoffMortageProperties
    this.quoteModel[0].out.scenarios[0].purchasePriceProperties = this.outputs.out.scenarios[0].purchasePriceProperties
    this.quoteModel[0].out.scenarios[0].estimatedClosingCost = this.outputs.out.scenarios[0].estimatedClosingCost
    this.quoteModel[0].out.scenarios[0].originationFee = this.outputs.out.scenarios[0].originationFee
    this.quoteModel[0].out.scenarios[0].buyUpDownNotOpted = this.outputs.out.scenarios[0].buyUpDownNotOpted
    this.quoteModel[0].out.scenarios[0].processingFee = this.outputs.out.scenarios[0].processingFee
    this.quoteModel[0].out.scenarios[0].legalFee = this.outputs.out.scenarios[0].legalFee
    this.quoteModel[0].out.scenarios[0].brokerFee1 = this.outputs.out.scenarios[0].brokerFee
    this.quoteModel[0].out.scenarios[0].insuranceCertifications = this.outputs.out.scenarios[0].insuranceCertifications
    this.quoteModel[0].out.scenarios[0].taxImpound = this.outputs.out.scenarios[0].taxImpound
    this.quoteModel[0].out.scenarios[0].insuranceImpound = this.outputs.out.scenarios[0].insuranceImpound
    this.quoteModel[0].out.scenarios[0].interestReserveEscrow = this.outputs.out.scenarios[0].interestReserveEscrow
    this.quoteModel[0].out.scenarios[0].closingCashToBorrower = this.outputs.out.scenarios[0].closingCashToBorrower
    this.quoteModel[0].out.scenarios[0].qualifyingInterestRate = this.outputs.out.scenarios[0].qualifyingInterestRate
    this.quoteModel[0].out.scenarios[0].rateBuyUpDown = this.outputs.out.scenarios[0].rateBuyUpDown
    this.quoteModel[0].out.scenarios[0].prepaymentPenalty = this.outputs.out.scenarios[0].prepaymentPenalty
    this.quoteModel[0].out.scenarios[0].initFixedInterestRate = this.outputs.out.scenarios[0].initFixedInterestRate
    this.quoteModel[0].out.scenarios[0].amortizationPeriod = this.outputs.out.scenarios[0].amortizationPeriod
    this.quoteModel[0].out.scenarios[0].monthlyPrincipalInterest = this.outputs.out.scenarios[0].monthlyPrincipalInterest
    this.quoteModel[0].out.scenarios[0].monthlyTaxesInsurance = this.outputs.out.scenarios[0].monthlyTaxesInsurance
    this.quoteModel[0].out.scenarios[0].monthlyHOADues = this.outputs.out.scenarios[0].monthlyHOADues
    this.quoteModel[0].out.scenarios[0].monthlyTotalPayment = this.outputs.out.scenarios[0].monthlyTotalPayment
    this.quoteModel[0].out.scenarios[0].monthlyEstimatedRentalIncome = this.outputs.out.scenarios[0].monthlyEstimatedRentalIncome
    this.quoteModel[0].out.scenarios[0].rentDebtRatio = this.outputs.out.scenarios[0].rentDebtRatio
    this.quoteModel[0].out.scenarios[0].capStructure = this.outputs.out.scenarios[0].capStructure
    this.quoteModel[0].out.scenarios[0].lifetimeCap = this.outputs.out.scenarios[0].lifetimeCap
    this.quoteModel[0].out.scenarios[0].floor = this.outputs.out.scenarios[0].floor
    this.quoteModel[0].out.scenarios[0].lookbackPeriod = this.outputs.out.scenarios[0].lookbackPeriod
    this.quoteModel[0].out.scenarios[0].prepaymentPenaltyText = this.outputs.out.scenarios[0].prepaymentPenaltyText
    this.quoteModel[0].out.scenarios[0].openPrepayOption = this.outputs.out.scenarios[0].openPrepayOption
    this.quoteModel[0].out.scenarios[0].guarantyType = this.outputs.out.scenarios[0].guarantyType
    this.quoteModel[0].out.scenarios[0].liquidReserves = this.outputs.out.scenarios[0].liquidReserves
    this.quoteModel[0].out.scenarios[0].buyUpDownFee = this.outputs.out.scenarios[0].buyUpDownFee

    this.quoteModel[0].out.scenarios[1].ltvEligibility = this.outputs.out.scenarios[1].ltvEligibility
    this.quoteModel[0].out.scenarios[1].eocTitle = this.outputs.out.scenarios[1].eocTitle
    this.quoteModel[0].out.scenarios[1].borrower = this.outputs.out.scenarios[1].borrower
    this.quoteModel[0].out.scenarios[1].estimatedValOfProperty = this.outputs.out.scenarios[1].estimatedValOfProperty
    this.quoteModel[0].out.scenarios[1].numOfProperties = this.outputs.out.scenarios[1].numOfProperties
    this.quoteModel[0].out.scenarios[1].estimatedLoanToVal = this.outputs.out.scenarios[1].estimatedLoanToVal
    this.quoteModel[0].out.scenarios[1].borrowerCreditScore = this.outputs.out.scenarios[1].borrowerCreditScore
    this.quoteModel[0].out.scenarios[1].loanAmt = this.outputs.out.scenarios[1].loanAmt
    this.quoteModel[0].out.scenarios[1].payoffMortageProperties = this.outputs.out.scenarios[1].payoffMortageProperties
    this.quoteModel[0].out.scenarios[1].purchasePriceProperties = this.outputs.out.scenarios[1].purchasePriceProperties
    this.quoteModel[0].out.scenarios[1].estimatedClosingCost = this.outputs.out.scenarios[1].estimatedClosingCost
    this.quoteModel[0].out.scenarios[1].originationFee = this.outputs.out.scenarios[1].originationFee
    this.quoteModel[0].out.scenarios[1].buyUpDownNotOpted = this.outputs.out.scenarios[1].buyUpDownNotOpted
    this.quoteModel[0].out.scenarios[1].processingFee = this.outputs.out.scenarios[1].processingFee
    this.quoteModel[0].out.scenarios[1].legalFee = this.outputs.out.scenarios[1].legalFee
    this.quoteModel[0].out.scenarios[1].brokerFee2 = this.outputs.out.scenarios[1].brokerFee
    this.quoteModel[0].out.scenarios[1].insuranceCertifications = this.outputs.out.scenarios[1].insuranceCertifications
    this.quoteModel[0].out.scenarios[1].taxImpound = this.outputs.out.scenarios[1].taxImpound
    this.quoteModel[0].out.scenarios[1].insuranceImpound = this.outputs.out.scenarios[1].insuranceImpound
    this.quoteModel[0].out.scenarios[1].interestReserveEscrow = this.outputs.out.scenarios[1].interestReserveEscrow
    this.quoteModel[0].out.scenarios[1].closingCashToBorrower = this.outputs.out.scenarios[1].closingCashToBorrower
    this.quoteModel[0].out.scenarios[1].qualifyingInterestRate = this.outputs.out.scenarios[1].qualifyingInterestRate
    this.quoteModel[0].out.scenarios[1].rateBuyUpDown = this.outputs.out.scenarios[1].rateBuyUpDown
    this.quoteModel[0].out.scenarios[1].prepaymentPenalty = this.outputs.out.scenarios[1].prepaymentPenalty
    this.quoteModel[0].out.scenarios[1].initFixedInterestRate = this.outputs.out.scenarios[1].initFixedInterestRate
    this.quoteModel[0].out.scenarios[1].amortizationPeriod = this.outputs.out.scenarios[1].amortizationPeriod
    this.quoteModel[0].out.scenarios[1].monthlyPrincipalInterest = this.outputs.out.scenarios[1].monthlyPrincipalInterest
    this.quoteModel[0].out.scenarios[1].monthlyTaxesInsurance = this.outputs.out.scenarios[1].monthlyTaxesInsurance
    this.quoteModel[0].out.scenarios[1].monthlyHOADues = this.outputs.out.scenarios[1].monthlyHOADues
    this.quoteModel[0].out.scenarios[1].monthlyTotalPayment = this.outputs.out.scenarios[1].monthlyTotalPayment
    this.quoteModel[0].out.scenarios[1].monthlyEstimatedRentalIncome = this.outputs.out.scenarios[1].monthlyEstimatedRentalIncome
    this.quoteModel[0].out.scenarios[1].rentDebtRatio = this.outputs.out.scenarios[1].rentDebtRatio
    this.quoteModel[0].out.scenarios[1].capStructure = this.outputs.out.scenarios[1].capStructure
    this.quoteModel[0].out.scenarios[1].lifetimeCap = this.outputs.out.scenarios[1].lifetimeCap
    this.quoteModel[0].out.scenarios[1].floor = this.outputs.out.scenarios[1].floor
    this.quoteModel[0].out.scenarios[1].lookbackPeriod = this.outputs.out.scenarios[1].lookbackPeriod
    this.quoteModel[0].out.scenarios[1].prepaymentPenaltyText = this.outputs.out.scenarios[1].prepaymentPenaltyText
    this.quoteModel[0].out.scenarios[1].openPrepayOption = this.outputs.out.scenarios[1].openPrepayOption
    this.quoteModel[0].out.scenarios[1].guarantyType = this.outputs.out.scenarios[1].guarantyType
    this.quoteModel[0].out.scenarios[1].liquidReserves = this.outputs.out.scenarios[1].liquidReserves
    this.quoteModel[0].out.scenarios[1].buyUpDownFee = this.outputs.out.scenarios[1].buyUpDownFee

    this.quoteModel[0].out.scenarios[2].ltvEligibility = this.outputs.out.scenarios[2].ltvEligibility
    this.quoteModel[0].out.scenarios[2].eocTitle = this.outputs.out.scenarios[2].eocTitle
    this.quoteModel[0].out.scenarios[2].borrower = this.outputs.out.scenarios[2].borrower
    this.quoteModel[0].out.scenarios[2].estimatedValOfProperty = this.outputs.out.scenarios[2].estimatedValOfProperty
    this.quoteModel[0].out.scenarios[2].numOfProperties = this.outputs.out.scenarios[2].numOfProperties
    this.quoteModel[0].out.scenarios[2].estimatedLoanToVal = this.outputs.out.scenarios[2].estimatedLoanToVal
    this.quoteModel[0].out.scenarios[2].borrowerCreditScore = this.outputs.out.scenarios[2].borrowerCreditScore
    this.quoteModel[0].out.scenarios[2].loanAmt = this.outputs.out.scenarios[2].loanAmt
    this.quoteModel[0].out.scenarios[2].payoffMortageProperties = this.outputs.out.scenarios[2].payoffMortageProperties
    this.quoteModel[0].out.scenarios[2].purchasePriceProperties = this.outputs.out.scenarios[2].purchasePriceProperties
    this.quoteModel[0].out.scenarios[2].estimatedClosingCost = this.outputs.out.scenarios[2].estimatedClosingCost
    this.quoteModel[0].out.scenarios[2].originationFee = this.outputs.out.scenarios[2].originationFee
    this.quoteModel[0].out.scenarios[2].buyUpDownNotOpted = this.outputs.out.scenarios[2].buyUpDownNotOpted
    this.quoteModel[0].out.scenarios[2].processingFee = this.outputs.out.scenarios[2].processingFee
    this.quoteModel[0].out.scenarios[2].legalFee = this.outputs.out.scenarios[2].legalFee
    this.quoteModel[0].out.scenarios[2].brokerFee3 = this.outputs.out.scenarios[2].brokerFee
    this.quoteModel[0].out.scenarios[2].insuranceCertifications = this.outputs.out.scenarios[2].insuranceCertifications
    this.quoteModel[0].out.scenarios[2].taxImpound = this.outputs.out.scenarios[2].taxImpound
    this.quoteModel[0].out.scenarios[2].insuranceImpound = this.outputs.out.scenarios[2].insuranceImpound
    this.quoteModel[0].out.scenarios[2].interestReserveEscrow = this.outputs.out.scenarios[2].interestReserveEscrow
    this.quoteModel[0].out.scenarios[2].closingCashToBorrower = this.outputs.out.scenarios[2].closingCashToBorrower
    this.quoteModel[0].out.scenarios[2].qualifyingInterestRate = this.outputs.out.scenarios[2].qualifyingInterestRate
    this.quoteModel[0].out.scenarios[2].rateBuyUpDown = this.outputs.out.scenarios[2].rateBuyUpDown
    this.quoteModel[0].out.scenarios[2].prepaymentPenalty = this.outputs.out.scenarios[2].prepaymentPenalty
    this.quoteModel[0].out.scenarios[2].initFixedInterestRate = this.outputs.out.scenarios[2].initFixedInterestRate
    this.quoteModel[0].out.scenarios[2].amortizationPeriod = this.outputs.out.scenarios[2].amortizationPeriod
    this.quoteModel[0].out.scenarios[2].monthlyPrincipalInterest = this.outputs.out.scenarios[2].monthlyPrincipalInterest
    this.quoteModel[0].out.scenarios[2].monthlyTaxesInsurance = this.outputs.out.scenarios[2].monthlyTaxesInsurance
    this.quoteModel[0].out.scenarios[2].monthlyHOADues = this.outputs.out.scenarios[2].monthlyHOADues
    this.quoteModel[0].out.scenarios[2].monthlyTotalPayment = this.outputs.out.scenarios[2].monthlyTotalPayment
    this.quoteModel[0].out.scenarios[2].monthlyEstimatedRentalIncome = this.outputs.out.scenarios[2].monthlyEstimatedRentalIncome
    this.quoteModel[0].out.scenarios[2].rentDebtRatio = this.outputs.out.scenarios[2].rentDebtRatio
    this.quoteModel[0].out.scenarios[2].capStructure = this.outputs.out.scenarios[2].capStructure
    this.quoteModel[0].out.scenarios[2].lifetimeCap = this.outputs.out.scenarios[2].lifetimeCap
    this.quoteModel[0].out.scenarios[2].floor = this.outputs.out.scenarios[2].floor
    this.quoteModel[0].out.scenarios[2].lookbackPeriod = this.outputs.out.scenarios[2].lookbackPeriod
    this.quoteModel[0].out.scenarios[2].prepaymentPenaltyText = this.outputs.out.scenarios[2].prepaymentPenaltyText
    this.quoteModel[0].out.scenarios[2].openPrepayOption = this.outputs.out.scenarios[2].openPrepayOption
    this.quoteModel[0].out.scenarios[2].guarantyType = this.outputs.out.scenarios[2].guarantyType
    this.quoteModel[0].out.scenarios[2].liquidReserves = this.outputs.out.scenarios[2].liquidReserves
    this.quoteModel[0].out.scenarios[2].buyUpDownFee = this.outputs.out.scenarios[2].buyUpDownFee

    // Set Premier Pricing Fields
    switch (this.premierPricing) {
      case true:
        this.quoteModel[0].out.scenarios[0].premier.interestRateValue = this.outputs.out.scenarios[0].premier.interestRateValue
        this.quoteModel[0].out.scenarios[1].premier.interestRateValue = this.outputs.out.scenarios[1].premier.interestRateValue
        this.quoteModel[0].out.scenarios[2].premier.interestRateValue = this.outputs.out.scenarios[2].premier.interestRateValue

        this.quoteModel[0].out.scenarios[0].premier.interestRateName = this.outputs.out.scenarios[0].premier.interestRateName
        this.quoteModel[0].out.scenarios[1].premier.interestRateName = this.outputs.out.scenarios[1].premier.interestRateName
        this.quoteModel[0].out.scenarios[2].premier.interestRateName = this.outputs.out.scenarios[2].premier.interestRateName

        this.quoteModel[0].out.scenarios[0].premier.openPrepay = this.outputs.out.scenarios[0].premier.openPrepay
        this.quoteModel[0].out.scenarios[1].premier.openPrepay = this.outputs.out.scenarios[1].premier.openPrepay
        this.quoteModel[0].out.scenarios[2].premier.openPrepay = this.outputs.out.scenarios[2].premier.openPrepay

        this.quoteModel[0].out.scenarios[0].premier.indexRate = this.outputs.out.scenarios[0].premier.indexRate
        this.quoteModel[0].out.scenarios[1].premier.indexRate = this.outputs.out.scenarios[1].premier.indexRate
        this.quoteModel[0].out.scenarios[2].premier.indexRate = this.outputs.out.scenarios[2].premier.indexRate

        this.quoteModel[0].out.scenarios[0].premier.dateRate = this.outputs.out.scenarios[0].premier.dateRate
        this.quoteModel[0].out.scenarios[1].premier.dateRate = this.outputs.out.scenarios[1].premier.dateRate
        this.quoteModel[0].out.scenarios[2].premier.dateRate = this.outputs.out.scenarios[2].premier.dateRate

        this.quoteModel[0].out.scenarios[0].premier.spread = this.outputs.out.scenarios[0].premier.spread
        this.quoteModel[0].out.scenarios[1].premier.spread = this.outputs.out.scenarios[1].premier.spread
        this.quoteModel[0].out.scenarios[2].premier.spread = this.outputs.out.scenarios[2].premier.spread

        this.quoteModel[0].out.scenarios[0].premier.index = this.outputs.out.scenarios[0].premier.index
        this.quoteModel[0].out.scenarios[1].premier.index = this.outputs.out.scenarios[1].premier.index
        this.quoteModel[0].out.scenarios[2].premier.index = this.outputs.out.scenarios[2].premier.index

        break
      default:
        delete this.quoteModel[0].out.scenarios[0].premier
        delete this.quoteModel[0].out.scenarios[1].premier
        delete this.quoteModel[0].out.scenarios[2].premier
    }
  }

  /**
   * method validate()
   *
   * validates the input fields before sending the payload to the API
   *
   * @returns array
   */
  validate(): any {
    const fields = []

    if (this.borrowingEntity === '') {
      fields.push('Borrower Entity')
    }

    if (this.borrowerNetWorth === '') {
      fields.push('Net Worth')
    }

    if (this.numberOfProperties === '') {
      fields.push('Number Of Properties')
    }

    if (this.fico === '') {
      fields.push('Credit Score')
    }

    if (this.sreoPropertyNumber === '') {
      fields.push('SREO Property Number')
    }

    if (this.mortgagePayoffs === '') {
      fields.push('Mortgage Payoffs')
    }

    if (this.annualInsurance === '') {
      fields.push('Annual Insurance')
    }

    if (this.annualTaxes === '') {
      fields.push('Annual Taxes')
    }

    if (this.monthlyRent === '') {
      fields.push('Monthly Rent')
    }

    if (this.annualFloodInsurance === '') {
      fields.push('Annual Flood Insurance')
    }

    if (this.annualHOA === '') {
      fields.push('Annual HOA')
    }

    if (this.yieldSpreadPremium === '') {
      fields.push('Yield Spread Premium')
    }

    if (this.buyUpbuyDownSelected === '') {
      fields.push('Buy Up/Buy Down Option')
    }

    if (this.prePaymentPenaltySelected === '') {
      fields.push('Pre Payment Penalty')
    }

    if (this.brokerFee === '') {
      fields.push('Broker Fee')
    }

    if (this.appraisalFee === '') {
      fields.push('Appraisal Fee')
    }

    if (this.premierPricing && this.propertyManagementFee === '') {
      fields.push('Property Management Fee')
    }

    return fields
  }

  /**
   * method isSaveAble()
   *
   * tests the validation conditions to see if the quote is savable and
   * sets the value of the attribute accordingly
   */
  isSaveAble(): void {
    let saveAble = true
    const fields = this.validate()

    if (fields.length > 1) {
      saveAble = false
    }
    if (this.validationErrors.length > 1) {
      saveAble = false
    }

    this.savable = saveAble
  }

  /**
   * method clearInputs()
   *
   * sets the value of all the inputs to ''
   */
  clearInputs(): void {
    const inputs = document.querySelectorAll('input')

    inputs.forEach((key: any, val: any) => {
      key.value = ''
    })
  }

  /**
   * method validateFront()
   * tests whether or not the required fields are set
   *
   * @returns boolean
   */
  validateFront(): boolean {
    this.frontEndValidation = this.validate()
    if (this.frontEndValidation.length < 1) {
      return true
    }
    return false
  }

  /**
   * method saveQuote()
   *
   * method saves the current quote
   */
  saveQuote(): void {
    this.setQuoteModel()
    switch (this.id) {
      case '':
        this.backEndSaveCall()
        break
      default:
        this.backEndPutCall()
        break
    }
  }

  /**
   * method calculateResults()
   *
   * calculates the current quote
   */
  calculateResults(): void {
    this.setCalcModel()
    const valid = this.validateFront()
    if (valid) {
      this.runCalcBackEnd()
    }
  }

  /**
   * method generatePreview()
   *
   * Displays the appropriate template preview
   *
   * @param whiteLabel boolean
   */
  generatePreview(whiteLabel: any = false): void {
    this.whiteLabel = whiteLabel
    this.closePreview()
    switch (this.premierPricing) {
      case true:
        this.previewPremier = true
        break
      default:
        this.previewRental = true
    }
  }

  /**
   * method closePreview()
   *
   * closes the preview window
   */
  closePreview(): void {
    this.previewRental = false
    this.previewPremier = false
  }

  /**
   * method generatePDF()
   *
   * Builds an in page PDF based on the loaded quote
   */
  generatePDF(): void {
    this.loading = true
    let data = document.getElementById('rental')!
    if (this.premierPricing) {
      data = document.getElementById('premier')!
    }
    html2canvas(data, { allowTaint: true }).then((canvas: any) => {
      const currentDate = new Date()
      const imgData = canvas.toDataURL('image/png')
      const pdf = new jspdf('p', 'pt', [canvas.width, canvas.height])
      const pdfWidth = pdf.internal.pageSize.getWidth()
      const pdfHeight = pdf.internal.pageSize.getHeight()
      pdf.addImage(imgData, 'PNG', 0, 0, pdfWidth, pdfHeight)
      pdf.save(
        `${this.quoteName}-${currentDate.toLocaleDateString('en-US')}.pdf`,
      )
    })
    this.closePreview()
    this.loading = false
  }

  /**
   * method runCalcBackEnd()
   *
   * sends the calculation api request
   *
   * @returns error
   */
  runCalcBackEnd = async () => {
    this.loading = true
    const calcResponse = await fetch(`${localURL}calculations`, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(this.calcModel),
    })
      .then((response: any) => {
        if (response.status === 200) {
          return response.json()
        }
        return Promise.reject(response)
      })
      .catch((err: any) => {
        console.warn(`Error: ${err}`)
      })

    this.loading = false
    this.output = true
    if (typeof calcResponse.res === 'undefined') {
      this.validationErrors = 'There was an error processing your request'
      return
    }
    this.outputs = calcResponse.res
    this.savable = true

    this.setOutputs()
  }

  /**
   * method backEndSaveCall()
   *
   * sends the quote save API request
   *
   * @returns error
   */
  backEndSaveCall = async () => {
    this.loading = true
    this.validationErrors = ''
    const resultJsonData = await fetch(`${localURL}quotes`, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(this.quoteModel),
    })
      .then((response: any) => {
        if (response.status === 200) {
          return response.json()
        }
        return Promise.reject(response)
      })
      .catch((err: any) => {
        console.warn(`Error: ${err}`)
        const error = [
          {
            error: 'There was an error processing your request',
          },
        ]
        return error
      })
    this.isPrintable()
    this.loading = false
    if (typeof resultJsonData.res === 'undefined') {
      this.validationErrors = 'There was an error processing your request'
      return
    }
    this.id = resultJsonData.res[0].out.id
  }

  /**
   * method backEndPutCall()
   *
   * sends the update API call for quote/id
   *
   * @returns error
   */
  backEndPutCall = async () => {
    this.loading = true
    const putResponse = await fetch(`${localURL}quotes/${this.id}`, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(this.quoteModel),
    })
      .then((response: any) => {
        if (response.status === 200) {
          return response.json()
        }
        return Promise.reject(response)
      })
      .catch((err: any) => {
        console.warn(`Error: ${err}`)
        const error = [
          {
            error: 'There was an error processing your request',
          },
        ]
        return error
      })
    this.isPrintable()
    this.loading = false
    if (typeof putResponse.res === 'undefined') {
      this.validationErrors = 'There was an error processing your request'
      return
    }
  }

  /**
   * method getQuoteBackEnd()
   *
   * loads the existing quote into the html from the Mulesoft API
   */
  getQuoteBackEnd = async () => {
    this.loading = true
    const quote = fetch(`${localURL}quote/${this.id}`, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    })
    const response = await quote
    const responseJsonData = await response.json()
    this.currentQuote = responseJsonData.res
    this.setInputs()
    this.setDropDowns()
    this.updatePricing()
    this.isPrintable()
    this.loading = false
  }

  /**
   * method isPrintable()
   *
   * controls whether or not PDF buttons are active
   */
  isPrintable(): void {
    this.printable = false
    if (
      this.validationErrors.length === 0 &&
      this.frontEndValidation.length === 0
    ) {
      this.printable = true
      this.checkColumns()
      this.formatPrepayText()
    }
  }

  /**
   * method checkColumns()
   *
   * determines the correct bootstrap format for the PDF template
   */
  checkColumns(): void {
    let count = 0
    if (this.include1 === 'Yes') {
      count = count + 1
    }
    if (this.include2 === 'Yes') {
      count = count + 1
    }
    if (this.include3 === 'Yes') {
      count = count + 1
    }
    this.mainColumn = 'col-6'
    switch (count) {
      case 1:
        this.columnWidth = 'col-3'
        this.mainColumn = 'col-9'
        break
      case 2:
        this.columnWidth = 'col-3'
        break
      case 3:
        this.columnWidth = 'col-2'
        break
      default:
        this.columnWidth = 'col-2'
    }
    this.setArmClass()
  }

  /**
   * method setArmClass()
   *
   * sets the spacing in the pdf template to execute appropriate
   * paging
   */
  setArmClass(): void {
    switch (this.productTypeSelected) {
      case '5/1 ARM':
      case '10/1 ARM':
      case 'Premier - 5/1 ARM':
        this.arm = 'fixed'
        break
      default:
        this.arm = ''
    }
  }

  /**
   * method setDropDowns()
   *
   * sets the picklist options to the appropriate values based on current
   * attribute values
   */
  setDropDowns(): void {
    this.premierNode = document.getElementById('premierCheck')
    this.premierNode.checked = this.premierPricing

    this.productNode = document.getElementById(this.productTypeSelected)
    this.productNode.selected = true

    if (this.submissionTypeSelected !== 'Standard Retail') {
      this.submissionNode = document.getElementById(this.submissionTypeSelected)
      this.submissionNode.selected = true
    }

    this.rateBuyUpDownNode = document.getElementById(this.buyUpDownSelected)
    this.rateBuyUpDownNode.selected = true
    this.prepaymentPenaltyNode = document.getElementById(
      this.prePaymentPenaltySelected,
    )
    this.prepaymentPenaltyNode.selected = true

    this.includeNode1 = document.getElementById(`include1_${this.include1}`)
    this.includeNode2 = document.getElementById(`include2_${this.include2}`)
    this.includeNode3 = document.getElementById(`include3_${this.include3}`)

    this.includeNode1.selected = true
    this.includeNode2.selected = true
    this.includeNode3.selected = true

    this.yieldSpreadPremiumNode = document.getElementById(
      this.yieldSpreadPremium,
    )
    this.yieldSpreadPremiumNode.selected = true

    this.condoNode = document.getElementById('condo')
    this.condoNode.checked = true
    if (this.condo === 'No') {
      this.condoNode.checked = false
    }

    this.refinanceL1CFixNFlipNode = document.getElementById(
      'refinanceL1CFixNFlip',
    )
    this.refinanceL1CFixNFlipNode.checked = true
    if (this.refinanceL1CFixNFlip === 'No') {
      this.refinanceL1CFixNFlipNode.checked = false
    }

    this.propertyLessThanTenThousandNode = document.getElementById(
      'propertyLessThanTenThousand',
    )
    this.propertyLessThanTenThousandNode.checked = true
    if (this.propertyLessThanTenThousand === 'No') {
      this.propertyLessThanTenThousandNode.checked = false
    }

    this.fix2RentNode = document.getElementById('fix2Rent')
    this.fix2RentNode.checked = true
    if (this.fix2Rent === 'No') {
      this.fix2RentNode.checked = false
    }

    // this.greaterThanTwentyFiveCollateralCondosLessThanSeventyFiveThousandNode =
    // document.getElementById('greaterThanTwentyFiveCollateralCondosLessThanSeventyFiveThousand')
    // this.greaterThanTwentyFiveCollateralCondosLessThanSeventyFiveThousandNode.checked = true
    // if(this.greaterThanTwentyFiveCollateralCondosLessThanSeventyFiveThousand === 'No') {
    //   this.greaterThanTwentyFiveCollateralCondosLessThanSeventyFiveThousandNode.checked = false
    // }

    this.propertiesNYNode = document.getElementById('propertiesNY')
    this.propertiesNYNode.checked = true
    if (this.propertiesNY === 'No') {
      this.propertiesNYNode.checked = false
    }

    this.referralNode = document.getElementById('referral')
    this.referralNode.checked = true
    if (this.referral === 'No') {
      this.referralNode.checked = false
    }
  }

  /**
   * method setInputs()
   *
   * sets the attribute values from the currentQuote model
   */
  setInputs(): void {
    // In
    this.quoteType = this.currentQuote[0].in.quoteType
    this.productTypeSelected = this.currentQuote[0].in.productType
    this.quoteName = this.currentQuote[0].in.quoteName
    this.overrideMaster = this.currentQuote[0].in.overrideMaster
    this.submissionTypeSelected = this.currentQuote[0].in.submissionType
    this.borrowingEntity = this.currentQuote[0].in.borrowingEntity
    this.borrowerNetWorth = this.currentQuote[0].in.borrowerNetWorth
    this.fico = this.currentQuote[0].in.creditScore
    this.sreoPropertyNumber = this.currentQuote[0].in.sreoPropertyNumber
    this.numOfProperties = this.currentQuote[0].in.numOfProperties
    this.totalCosts1 = this.currentQuote[0].in.totalCosts1
    this.totalCosts2 = this.currentQuote[0].in.totalCosts2
    this.appraisedVal1 = this.currentQuote[0].in.appraisedVal1
    this.appraisedVal2 = this.currentQuote[0].in.appraisedVal2
    this.appraisedVal3 = this.currentQuote[0].in.appraisedVal3
    this.purchasePrice = this.currentQuote[0].in.purchasePrice
    this.mortgagePayoffs = this.currentQuote[0].in.mortagePayoffs
    this.numberOfProperties = this.currentQuote[0].in.numOfProperties
    this.monthlyRent = this.currentQuote[0].in.monthlyRent
    this.annualTaxes = this.currentQuote[0].in.annualTaxes
    this.annualInsurance = this.currentQuote[0].in.annualInsurance
    this.annualFloodInsurance = this.currentQuote[0].in.annualFloodInsurance
    this.annualHOA = this.currentQuote[0].in.annualHOA
    this.propertiesNY = this.currentQuote[0].in.propertiesNY
    this.referral = this.currentQuote[0].in.borrowerIsReferral
    this.yieldSpreadPremium = this.currentQuote[0].in.yieldSpendPremium
      .toFixed(3)
      .toString()
    this.refinanceL1CFixNFlip = this.currentQuote[0].in.refiL1cFixNFlip
    this.buyUpDownSelected = this.currentQuote[0].in.buyUpDownOption
    this.buyUpDownPercentage = this.currentQuote[0].in.buyUpDownPercentage
    this.prePaymentPenaltySelected = this.currentQuote[0].in.prepaymentPenalty
    this.brokerFee = this.currentQuote[0].in.brokerFee
    this.ltvPercentage1 = this.currentQuote[0].in.scenario[0].ltvPercentage
    this.ltvPercentage2 = this.currentQuote[0].in.scenario[1].ltvPercentage
    this.ltvPercentage3 = this.currentQuote[0].in.scenario[2].ltvPercentage

    this.propertyLessThanTenThousand = 'No'
    this.condo = 'No'
    this.fix2Rent = 'No'
    if (this.currentQuote[0].in.base) {
      this.propertyLessThanTenThousand = this.currentQuote[0].in.base.propertyVal
      this.condo = this.currentQuote[0].in.base.condo
      this.fix2Rent = this.currentQuote[0].in.base.fixToRent
    }

    this.include1 = this.currentQuote[0].in.scenario[0].included
    this.include2 = this.currentQuote[0].in.scenario[1].included
    this.include3 = this.currentQuote[0].in.scenario[2].included

    this.brokerProcessingFee = this.currentQuote[0].in.brokerProcessingFee
    this.appraisalFee = this.currentQuote[0].in.appraisalFee

    // Out
    // this.id = this.currentQuote[0].out.id
    this.workbookId = this.currentQuote[0].out.workbookId
    this.validQuote = this.currentQuote[0].out.validQuote
    this.lastUpdated = this.currentQuote[0].out.lastUpdated
    this.termSheetTitle = this.currentQuote[0].out.termSheetTitle

    // Scenario 1
    this.ltvEligibility = this.currentQuote[0].out.scenarios[0].ltvEligibility
    this.eocTitle = this.currentQuote[0].out.scenarios[0].eocTitle
    this.borrower = this.currentQuote[0].out.scenarios[0].borrower
    this.estimatedValOfProperty = this.currentQuote[0].out.scenarios[0].estimatedValOfProperty
    this.numOfProperties = this.currentQuote[0].out.scenarios[0].numOfProperties
    this.estimatedLoanToVal = this.currentQuote[0].out.scenarios[0].estimatedLoanToVal
    this.borrowerCreditScore = this.currentQuote[0].out.scenarios[0].borrowerCreditScore
    this.loanAmt = this.currentQuote[0].out.scenarios[0].loanAmt
    this.payoffMortageProperties = this.currentQuote[0].out.scenarios[0].payoffMortageProperties
    this.purchasePriceProperties = this.currentQuote[0].out.scenarios[0].purchasePriceProperties
    this.estimatedClosingCost = this.currentQuote[0].out.scenarios[0].estimatedClosingCost
    this.originationFee = this.currentQuote[0].out.scenarios[0].originationFee
    this.buyUpDownNotOpted = this.currentQuote[0].out.scenarios[0].buyUpDownNotOpted
    this.processingFee = this.currentQuote[0].out.scenarios[0].processingFee
    this.legalFee = this.currentQuote[0].out.scenarios[0].legalFee
    this.brokerFee1 = this.currentQuote[0].out.scenarios[0].brokerFee
    this.insuranceCertifications = this.currentQuote[0].out.scenarios[0].insuranceCertifications
    this.taxImpound = this.currentQuote[0].out.scenarios[0].taxImpound
    this.insuranceImpound = this.currentQuote[0].out.scenarios[0].insuranceImpound
    this.interestReserveEscrow = this.currentQuote[0].out.scenarios[0].interestReserveEscrow
    this.closingCashToBorrower = this.currentQuote[0].out.scenarios[0].closingCashToBorrower
    this.qualifyingInterestRate = this.currentQuote[0].out.scenarios[0].qualifyingInterestRate
    this.rateBuyUpDown = this.currentQuote[0].out.scenarios[0].rateBuyUpDown
    this.prepaymentPenalty = this.currentQuote[0].out.scenarios[0].prepaymentPenalty
    this.initFixedInterestRate = this.currentQuote[0].out.scenarios[0].initFixedInterestRate
    this.amortizationPeriod = this.currentQuote[0].out.scenarios[0].amortizationPeriod
    this.monthlyPrincipalInterest = this.currentQuote[0].out.scenarios[0].monthlyPrincipalInterest
    this.monthlyTaxesInsurance = this.currentQuote[0].out.scenarios[0].monthlyTaxesInsurance
    this.monthlyHOADues = this.currentQuote[0].out.scenarios[0].monthlyHOADues
    this.monthlyTotalPayment = this.currentQuote[0].out.scenarios[0].monthlyTotalPayment
    this.monthlyEstimatedRentalIncome = this.currentQuote[0].out.scenarios[0].monthlyEstimatedRentalIncome
    this.rentDebtRatio = this.currentQuote[0].out.scenarios[0].rentDebtRatio
    this.capStructure = this.currentQuote[0].out.scenarios[0].capStructure
    this.lifetimeCap = this.currentQuote[0].out.scenarios[0].lifetimeCap
    this.floor = this.currentQuote[0].out.scenarios[0].floor
    this.lookbackPeriod = this.currentQuote[0].out.scenarios[0].lookbackPeriod
    this.prepaymentPenaltyText = this.currentQuote[0].out.scenarios[0].prepaymentPenaltyText
    this.openPrepayOptionSelected1 = this.currentQuote[0].out.scenarios[0].openPrepayOption
    this.guarantyType = this.currentQuote[0].out.scenarios[0].guarantyType
    this.liquidReserves = this.currentQuote[0].out.scenarios[0].liquidReserves
    this.buyUpDownFee = this.currentQuote[0].out.scenarios[0].buyUpDownFee
    this.margin = this.currentQuote[0].out.scenarios[0].margin

    // Scenario 2
    this.ltvEligibility2 = this.currentQuote[0].out.scenarios[1].ltvEligibility
    this.eocTitle2 = this.currentQuote[0].out.scenarios[1].eocTitle
    this.borrower2 = this.currentQuote[0].out.scenarios[1].borrower
    this.estimatedValOfProperty2 = this.currentQuote[0].out.scenarios[1].estimatedValOfProperty
    this.numOfProperties2 = this.currentQuote[0].out.scenarios[1].numOfProperties
    this.estimatedLoanToVal2 = this.currentQuote[0].out.scenarios[1].estimatedLoanToVal
    this.borrowerCreditScore2 = this.currentQuote[0].out.scenarios[1].borrowerCreditScore
    this.loanAmt2 = this.currentQuote[0].out.scenarios[1].loanAmt
    this.payoffMortageProperties2 = this.currentQuote[0].out.scenarios[1].payoffMortageProperties
    this.purchasePriceProperties2 = this.currentQuote[0].out.scenarios[1].purchasePriceProperties
    this.estimatedClosingCost2 = this.currentQuote[0].out.scenarios[1].estimatedClosingCost
    this.originationFee2 = this.currentQuote[0].out.scenarios[1].originationFee
    this.buyUpDownNotOpted2 = this.currentQuote[0].out.scenarios[1].buyUpDownNotOpted
    this.processingFee2 = this.currentQuote[0].out.scenarios[1].processingFee
    this.legalFee2 = this.currentQuote[0].out.scenarios[1].legalFee
    this.brokerFee2 = this.currentQuote[0].out.scenarios[1].brokerFee
    this.insuranceCertifications2 = this.currentQuote[0].out.scenarios[1].insuranceCertifications
    this.taxImpound2 = this.currentQuote[0].out.scenarios[1].taxImpound
    this.insuranceImpound2 = this.currentQuote[0].out.scenarios[1].insuranceImpound
    this.interestReserveEscrow2 = this.currentQuote[0].out.scenarios[1].interestReserveEscrow
    this.closingCashToBorrower2 = this.currentQuote[0].out.scenarios[1].closingCashToBorrower
    this.qualifyingInterestRate2 = this.currentQuote[0].out.scenarios[1].qualifyingInterestRate
    this.rateBuyUpDown2 = this.currentQuote[0].out.scenarios[1].rateBuyUpDown
    this.prepaymentPenalty2 = this.currentQuote[0].out.scenarios[1].prepaymentPenalty
    this.initFixedInterestRate2 = this.currentQuote[0].out.scenarios[1].initFixedInterestRate
    this.amortizationPeriod2 = this.currentQuote[0].out.scenarios[1].amortizationPeriod
    this.monthlyPrincipalInterest2 = this.currentQuote[0].out.scenarios[1].monthlyPrincipalInterest
    this.monthlyTaxesInsurance2 = this.currentQuote[0].out.scenarios[1].monthlyTaxesInsurance
    this.monthlyHOADues2 = this.currentQuote[0].out.scenarios[1].monthlyHOADues
    this.monthlyTotalPayment2 = this.currentQuote[0].out.scenarios[1].monthlyTotalPayment
    this.monthlyEstimatedRentalIncome2 = this.currentQuote[0].out.scenarios[1].monthlyEstimatedRentalIncome
    this.rentDebtRatio2 = this.currentQuote[0].out.scenarios[1].rentDebtRatio
    this.capStructure2 = this.currentQuote[0].out.scenarios[1].capStructure
    this.lifetimeCap2 = this.currentQuote[0].out.scenarios[1].lifetimeCap
    this.floor2 = this.currentQuote[0].out.scenarios[1].floor
    this.lookbackPeriod2 = this.currentQuote[0].out.scenarios[1].lookbackPeriod
    this.prepaymentPenaltyText2 = this.currentQuote[0].out.scenarios[1].prepaymentPenaltyText
    this.openPrepayOption2 = this.currentQuote[0].out.scenarios[1].openPrepayOption
    this.guarantyType2 = this.currentQuote[0].out.scenarios[1].guarantyType
    this.liquidReserves2 = this.currentQuote[0].out.scenarios[1].liquidReserves
    this.buyUpDownFee2 = this.currentQuote[0].out.scenarios[1].buyUpDownFee
    this.margin2 = this.currentQuote[0].out.scenarios[1].margin

    // Scenario 3
    this.ltvEligibility3 = this.currentQuote[0].out.scenarios[2].ltvEligibility
    this.eocTitle3 = this.currentQuote[0].out.scenarios[2].eocTitle
    this.borrower3 = this.currentQuote[0].out.scenarios[2].borrower
    this.estimatedValOfProperty3 = this.currentQuote[0].out.scenarios[2].estimatedValOfProperty
    this.numOfProperties3 = this.currentQuote[0].out.scenarios[2].numOfProperties
    this.estimatedLoanToVal3 = this.currentQuote[0].out.scenarios[2].estimatedLoanToVal
    this.borrowerCreditScore3 = this.currentQuote[0].out.scenarios[2].borrowerCreditScore
    this.loanAmt3 = this.currentQuote[0].out.scenarios[2].loanAmt
    this.payoffMortageProperties3 = this.currentQuote[0].out.scenarios[2].payoffMortageProperties
    this.purchasePriceProperties3 = this.currentQuote[0].out.scenarios[2].purchasePriceProperties
    this.estimatedClosingCost3 = this.currentQuote[0].out.scenarios[2].estimatedClosingCost
    this.originationFee3 = this.currentQuote[0].out.scenarios[2].originationFee
    this.buyUpDownNotOpted3 = this.currentQuote[0].out.scenarios[2].buyUpDownNotOpted
    this.processingFee3 = this.currentQuote[0].out.scenarios[2].processingFee
    this.legalFee3 = this.currentQuote[0].out.scenarios[2].legalFee
    this.brokerFee3 = this.currentQuote[0].out.scenarios[2].brokerFee
    this.insuranceCertifications3 = this.currentQuote[0].out.scenarios[2].insuranceCertifications
    this.taxImpound3 = this.currentQuote[0].out.scenarios[2].taxImpound
    this.insuranceImpound3 = this.currentQuote[0].out.scenarios[2].insuranceImpound
    this.interestReserveEscrow3 = this.currentQuote[0].out.scenarios[2].interestReserveEscrow
    this.closingCashToBorrower3 = this.currentQuote[0].out.scenarios[2].closingCashToBorrower
    this.qualifyingInterestRate3 = this.currentQuote[0].out.scenarios[2].qualifyingInterestRate
    this.rateBuyUpDown3 = this.currentQuote[0].out.scenarios[2].rateBuyUpDown
    this.prepaymentPenalty3 = this.currentQuote[0].out.scenarios[2].prepaymentPenalty
    this.initFixedInterestRate3 = this.currentQuote[0].out.scenarios[2].initFixedInterestRate
    this.amortizationPeriod3 = this.currentQuote[0].out.scenarios[2].amortizationPeriod
    this.monthlyPrincipalInterest3 = this.currentQuote[0].out.scenarios[2].monthlyPrincipalInterest
    this.monthlyTaxesInsurance3 = this.currentQuote[0].out.scenarios[2].monthlyTaxesInsurance
    this.monthlyHOADues3 = this.currentQuote[0].out.scenarios[2].monthlyHOADues
    this.monthlyTotalPayment3 = this.currentQuote[0].out.scenarios[2].monthlyTotalPayment
    this.monthlyEstimatedRentalIncome3 = this.currentQuote[0].out.scenarios[2].monthlyEstimatedRentalIncome
    this.rentDebtRatio3 = this.currentQuote[0].out.scenarios[2].rentDebtRatio
    this.capStructure3 = this.currentQuote[0].out.scenarios[2].capStructure
    this.lifetimeCap3 = this.currentQuote[0].out.scenarios[2].lifetimeCap
    this.floor3 = this.currentQuote[0].out.scenarios[2].floor
    this.lookbackPeriod3 = this.currentQuote[0].out.scenarios[2].lookbackPeriod
    this.prepaymentPenaltyText3 = this.currentQuote[0].out.scenarios[2].prepaymentPenaltyText
    this.openPrepayOption3 = this.currentQuote[0].out.scenarios[2].openPrepayOption
    this.guarantyType3 = this.currentQuote[0].out.scenarios[2].guarantyType
    this.liquidReserves3 = this.currentQuote[0].out.scenarios[2].liquidReserves
    this.buyUpDownFee3 = this.currentQuote[0].out.scenarios[2].buyUpDownFee
    this.margin3 = this.currentQuote[0].out.scenarios[2].margin

    // Set Premier Pricing
    switch (this.quoteType) {
      case 'Premier':
        this.premierPricing = true
        this.propertyManagementFee = this.currentQuote[0].in.premier.propertyManagementFee
        this.indexSelected = this.currentQuote[0].out.scenarios[0].premier.index
        this.interestRateName1 = this.currentQuote[0].out.scenarios[0].premier.interestRateName
        this.interestRateName2 = this.currentQuote[0].out.scenarios[0].premier.interestRateName
        this.interestRateName3 = this.currentQuote[0].out.scenarios[0].premier.interestRateName
        this.interestRateValue1 = this.currentQuote[0].out.scenarios[0].premier.interestRateValue
        this.interestRateValue2 = this.currentQuote[0].out.scenarios[1].premier.interestRateValue
        this.interestRateValue3 = this.currentQuote[0].out.scenarios[2].premier.interestRateValue
        this.indexRate = this.currentQuote[0].out.scenarios[0].premier.indexRate
        this.indexRate1 = this.currentQuote[0].out.scenarios[0].premier.indexRate
        this.indexRate2 = this.currentQuote[0].out.scenarios[1].premier.indexRate
        this.indexRate3 = this.currentQuote[0].out.scenarios[2].premier.indexRate
        this.dateRate1 = this.currentQuote[0].out.scenarios[0].premier.dateRate
        this.dateRate2 = this.currentQuote[0].out.scenarios[1].premier.dateRate
        this.dateRate3 = this.currentQuote[0].out.scenarios[2].premier.dateRate
        this.spread1 = this.currentQuote[0].out.scenarios[0].premier.spread
        this.spread2 = this.currentQuote[0].out.scenarios[1].premier.spread
        this.spread3 = this.currentQuote[0].out.scenarios[2].premier.spread
        this.setPremier()
        break
      default:
        this.premierPricing = false
    }
    this.updatePrepay()
  }

  /**
   * method setOutputs()
   *
   * sets the output values returned from the Mulesoft API
   */
  setOutputs(): void {
    // Outputs
    this.workbookId = this.outputs.out.workbookId
    this.validQuote = this.outputs.out.validQuote
    this.termSheetTitle = this.outputs.out.termSheetTitle

    // Scenario 1
    this.ltvEligibility = this.outputs.out.scenarios[0].ltvEligibility
    this.eocTitle = this.outputs.out.scenarios[0].eocTitle
    this.borrower = this.outputs.out.scenarios[0].borrower
    this.estimatedValOfProperty = this.outputs.out.scenarios[0].estimatedValOfProperty
    this.numOfProperties = this.outputs.out.scenarios[0].numOfProperties
    this.estimatedLoanToVal = this.outputs.out.scenarios[0].estimatedLoanToVal
    this.borrowerCreditScore = this.outputs.out.scenarios[0].borrowerCreditScore
    this.loanAmt = this.outputs.out.scenarios[0].loanAmt
    this.payoffMortageProperties = this.outputs.out.scenarios[0].payoffMortageProperties
    this.purchasePriceProperties = this.outputs.out.scenarios[0].purchasePriceProperties
    this.estimatedClosingCost = this.outputs.out.scenarios[0].estimatedClosingCost
    this.originationFee = this.outputs.out.scenarios[0].originationFee
    this.buyUpDownNotOpted = this.outputs.out.scenarios[0].buyUpDownNotOpted
    this.processingFee = this.outputs.out.scenarios[0].processingFee
    this.legalFee = this.outputs.out.scenarios[0].legalFee
    this.brokerFee1 = this.outputs.out.scenarios[0].brokerFee
    this.insuranceCertifications = this.outputs.out.scenarios[0].insuranceCertifications
    this.taxImpound = this.outputs.out.scenarios[0].taxImpound
    this.insuranceImpound = this.outputs.out.scenarios[0].insuranceImpound
    this.interestReserveEscrow = this.outputs.out.scenarios[0].interestReserveEscrow
    this.closingCashToBorrower = this.outputs.out.scenarios[0].closingCashToBorrower
    this.qualifyingInterestRate = this.outputs.out.scenarios[0].qualifyingInterestRate
    this.rateBuyUpDown = this.outputs.out.scenarios[0].rateBuyUpDown
    this.prepaymentPenalty = this.outputs.out.scenarios[0].prepaymentPenalty
    this.initFixedInterestRate = this.outputs.out.scenarios[0].initFixedInterestRate
    this.amortizationPeriod = this.outputs.out.scenarios[0].amortizationPeriod
    this.monthlyPrincipalInterest = this.outputs.out.scenarios[0].monthlyPrincipalInterest
    this.monthlyTaxesInsurance = this.outputs.out.scenarios[0].monthlyTaxesInsurance
    this.monthlyHOADues = this.outputs.out.scenarios[0].monthlyHOADues
    this.monthlyTotalPayment = this.outputs.out.scenarios[0].monthlyTotalPayment
    this.monthlyEstimatedRentalIncome = this.outputs.out.scenarios[0].monthlyEstimatedRentalIncome
    this.rentDebtRatio = this.outputs.out.scenarios[0].rentDebtRatio
    this.capStructure = this.outputs.out.scenarios[0].capStructure
    this.lifetimeCap = this.outputs.out.scenarios[0].lifetimeCap
    this.floor = this.outputs.out.scenarios[0].floor
    this.lookbackPeriod = this.outputs.out.scenarios[0].lookbackPeriod
    this.prepaymentPenaltyText = this.outputs.out.scenarios[0].prepaymentPenaltyText
    this.openPrepayOptionSelected1 = this.outputs.out.scenarios[0].openPrepayOption
    this.guarantyType = this.outputs.out.scenarios[0].guarantyType
    this.liquidReserves = this.outputs.out.scenarios[0].liquidReserves
    this.buyUpDownFee = this.outputs.out.scenarios[0].buyUpDownFee
    this.margin = this.outputs.out.scenarios[0].margin

    if (this.outputs.out.scenarios.length > 1) {
      this.ltvEligibility2 = this.outputs.out.scenarios[1].ltvEligibility
      this.eocTitle2 = this.outputs.out.scenarios[1].eocTitle
      this.borrower2 = this.outputs.out.scenarios[1].borrower
      this.estimatedValOfProperty2 = this.outputs.out.scenarios[1].estimatedValOfProperty
      this.numOfProperties2 = this.outputs.out.scenarios[1].numOfProperties
      this.estimatedLoanToVal2 = this.outputs.out.scenarios[1].estimatedLoanToVal
      this.borrowerCreditScore2 = this.outputs.out.scenarios[1].borrowerCreditScore
      this.loanAmt2 = this.outputs.out.scenarios[1].loanAmt
      this.payoffMortageProperties2 = this.outputs.out.scenarios[1].payoffMortageProperties
      this.purchasePriceProperties2 = this.outputs.out.scenarios[1].purchasePriceProperties
      this.estimatedClosingCost2 = this.outputs.out.scenarios[1].estimatedClosingCost
      this.originationFee2 = this.outputs.out.scenarios[1].originationFee
      this.buyUpDownNotOpted2 = this.outputs.out.scenarios[1].buyUpDownNotOpted
      this.processingFee2 = this.outputs.out.scenarios[1].processingFee
      this.legalFee2 = this.outputs.out.scenarios[1].legalFee
      this.brokerFee2 = this.outputs.out.scenarios[1].brokerFee
      this.insuranceCertifications2 = this.outputs.out.scenarios[1].insuranceCertifications
      this.taxImpound2 = this.outputs.out.scenarios[1].taxImpound
      this.insuranceImpound2 = this.outputs.out.scenarios[1].insuranceImpound
      this.interestReserveEscrow2 = this.outputs.out.scenarios[1].interestReserveEscrow
      this.closingCashToBorrower2 = this.outputs.out.scenarios[1].closingCashToBorrower
      this.qualifyingInterestRate2 = this.outputs.out.scenarios[1].qualifyingInterestRate
      this.rateBuyUpDown2 = this.outputs.out.scenarios[1].rateBuyUpDown
      this.prepaymentPenalty2 = this.outputs.out.scenarios[1].prepaymentPenalty
      this.initFixedInterestRate2 = this.outputs.out.scenarios[1].initFixedInterestRate
      this.amortizationPeriod2 = this.outputs.out.scenarios[1].amortizationPeriod
      this.monthlyPrincipalInterest2 = this.outputs.out.scenarios[1].monthlyPrincipalInterest
      this.monthlyTaxesInsurance2 = this.outputs.out.scenarios[1].monthlyTaxesInsurance
      this.monthlyHOADues2 = this.outputs.out.scenarios[1].monthlyHOADues
      this.monthlyTotalPayment2 = this.outputs.out.scenarios[1].monthlyTotalPayment
      this.monthlyEstimatedRentalIncome2 = this.outputs.out.scenarios[1].monthlyEstimatedRentalIncome
      this.rentDebtRatio2 = this.outputs.out.scenarios[1].rentDebtRatio
      this.capStructure2 = this.outputs.out.scenarios[1].capStructure
      this.lifetimeCap2 = this.outputs.out.scenarios[1].lifetimeCap
      this.floor2 = this.outputs.out.scenarios[1].floor
      this.lookbackPeriod2 = this.outputs.out.scenarios[1].lookbackPeriod
      this.prepaymentPenaltyText2 = this.outputs.out.scenarios[1].prepaymentPenaltyText
      this.openPrepayOption2 = this.outputs.out.scenarios[1].openPrepayOption
      this.guarantyType2 = this.outputs.out.scenarios[1].guarantyType
      this.liquidReserves2 = this.outputs.out.scenarios[1].liquidReserves
      this.buyUpDownFee2 = this.outputs.out.scenarios[1].buyUpDownFee
      this.margin2 = this.outputs.out.scenarios[1].margin
    }

    if (this.outputs.out.scenarios.length > 2) {
      this.ltvEligibility3 = this.outputs.out.scenarios[2].ltvEligibility
      this.eocTitle3 = this.outputs.out.scenarios[2].eocTitle
      this.borrower3 = this.outputs.out.scenarios[2].borrower
      this.estimatedValOfProperty3 = this.outputs.out.scenarios[2].estimatedValOfProperty
      this.numOfProperties3 = this.outputs.out.scenarios[2].numOfProperties
      this.estimatedLoanToVal3 = this.outputs.out.scenarios[2].estimatedLoanToVal
      this.borrowerCreditScore3 = this.outputs.out.scenarios[2].borrowerCreditScore
      this.loanAmt3 = this.outputs.out.scenarios[2].loanAmt
      this.payoffMortageProperties3 = this.outputs.out.scenarios[2].payoffMortageProperties
      this.purchasePriceProperties3 = this.outputs.out.scenarios[2].purchasePriceProperties
      this.estimatedClosingCost3 = this.outputs.out.scenarios[2].estimatedClosingCost
      this.originationFee3 = this.outputs.out.scenarios[2].originationFee
      this.buyUpDownNotOpted3 = this.outputs.out.scenarios[2].buyUpDownNotOpted
      this.processingFee3 = this.outputs.out.scenarios[2].processingFee
      this.legalFee3 = this.outputs.out.scenarios[2].legalFee
      this.brokerFee3 = this.outputs.out.scenarios[2].brokerFee
      this.insuranceCertifications3 = this.outputs.out.scenarios[2].insuranceCertifications
      this.taxImpound3 = this.outputs.out.scenarios[2].taxImpound
      this.insuranceImpound3 = this.outputs.out.scenarios[2].insuranceImpound
      this.interestReserveEscrow3 = this.outputs.out.scenarios[2].interestReserveEscrow
      this.closingCashToBorrower3 = this.outputs.out.scenarios[2].closingCashToBorrower
      this.qualifyingInterestRate3 = this.outputs.out.scenarios[2].qualifyingInterestRate
      this.rateBuyUpDown3 = this.outputs.out.scenarios[2].rateBuyUpDown
      this.prepaymentPenalty3 = this.outputs.out.scenarios[2].prepaymentPenalty
      this.initFixedInterestRate3 = this.outputs.out.scenarios[2].initFixedInterestRate
      this.amortizationPeriod3 = this.outputs.out.scenarios[2].amortizationPeriod
      this.monthlyPrincipalInterest3 = this.outputs.out.scenarios[2].monthlyPrincipalInterest
      this.monthlyTaxesInsurance3 = this.outputs.out.scenarios[2].monthlyTaxesInsurance
      this.monthlyHOADues3 = this.outputs.out.scenarios[2].monthlyHOADues
      this.monthlyTotalPayment3 = this.outputs.out.scenarios[2].monthlyTotalPayment
      this.monthlyEstimatedRentalIncome3 = this.outputs.out.scenarios[2].monthlyEstimatedRentalIncome
      this.rentDebtRatio3 = this.outputs.out.scenarios[2].rentDebtRatio
      this.capStructure3 = this.outputs.out.scenarios[2].capStructure
      this.lifetimeCap3 = this.outputs.out.scenarios[2].lifetimeCap
      this.floor3 = this.outputs.out.scenarios[2].floor
      this.lookbackPeriod3 = this.outputs.out.scenarios[2].lookbackPeriod
      this.prepaymentPenaltyText3 = this.outputs.out.scenarios[2].prepaymentPenaltyText
      this.openPrepayOption3 = this.outputs.out.scenarios[2].openPrepayOption
      this.guarantyType3 = this.outputs.out.scenarios[2].guarantyType
      this.liquidReserves3 = this.outputs.out.scenarios[2].liquidReserves
      this.buyUpDownFee3 = this.outputs.out.scenarios[2].buyUpDownFee
      this.margin3 = this.outputs.out.scenarios[2].margin
    }

    // Premier Fields
    if (this.premierPricing) {
      // Scenario 1
      this.interestRateValue1 = this.outputs.out.scenarios[0].premier.interestRateValue
      this.interestRateName1 = this.outputs.out.scenarios[0].premier.interestRateName
      this.openPrepay1 = this.outputs.out.scenarios[0].premier.openPrepay
      this.indexRate = this.outputs.out.scenarios[0].premier.indexRate
      this.indexRate1 = this.outputs.out.scenarios[0].premier.indexRate
      this.dateRate1 = this.outputs.out.scenarios[0].premier.dateRate
      this.spread1 = this.outputs.out.scenarios[0].premier.spread
      this.index1 = this.outputs.out.scenarios[0].premier.index
      this.indexSelected = this.outputs.out.scenarios[0].premier.index

      // Scenario 2
      this.interestRateValue2 = this.outputs.out.scenarios[1].premier.interestRateValue
      this.interestRateName2 = this.outputs.out.scenarios[1].premier.interestRateName
      this.openPrepay2 = this.outputs.out.scenarios[1].premier.openPrepay
      this.indexRate2 = this.outputs.out.scenarios[1].premier.indexRate
      this.dateRate2 = this.outputs.out.scenarios[1].premier.dateRate
      this.spread2 = this.outputs.out.scenarios[1].premier.spread
      this.index2 = this.outputs.out.scenarios[1].premier.index

      // Scenario 3
      this.interestRateValue3 = this.outputs.out.scenarios[2].premier.interestRateValue
      this.interestRateName3 = this.outputs.out.scenarios[2].premier.interestRateName
      this.openPrepay3 = this.outputs.out.scenarios[2].premier.openPrepay
      this.indexRate3 = this.outputs.out.scenarios[2].premier.indexRate
      this.dateRate3 = this.outputs.out.scenarios[2].premier.dateRate
      this.spread3 = this.outputs.out.scenarios[2].premier.spread
      this.index3 = this.outputs.out.scenarios[0].premier.index
    }
  }

  /**
   * method validateField()
   *
   * validates specific fields based on their content
   *
   * @param field string
   */
  validateField(field: any): void {
    switch (field) {
      case 'fico':
        const ficoPattern = new RegExp('^[0-9]+$')
        const ficoResult = ficoPattern.test(this.fico)
        this.ficoWarning = false
        if (!ficoResult || this.fico > 850 || this.fico < 300) {
          this.ficoWarning = true
        }
        break
      default:
      // Do Nothing
    }
    this.unsavable()
  }

  constructor(private route: ActivatedRoute) {
    this.id = ''
    this.premierPricing = false
    this.ficoWarning = false
    this.borrowingEntity = ''
    this.quoteName = ''
    this.fico = ''
    this.sreoPropertyNumber = ''
    this.numberOfProperties = ''
    this.totalCosts = ''
    this.propertyLessThanTenThousand = 'No'
    this.purchasePrice = ''
    this.condo = 'No'
    this.mortgagePayoffs = ''
    this.monthlyRent = ''
    this.annualTaxes = ''
    this.annualInsurance = ''
    this.annualFloodInsurance = ''
    this.annualHOA = ''
    this.propertiesNY = 'No'
    this.fix2Rent = 'No'
    this.referral = 'No'
    this.yieldSpreadPremium = '0.000'
    this.refinanceL1CFixNFlip = 'No'
    this.buyUpDownSelected = 'Rate Buy Up'
    this.buyUpDownPercentage = ''
    this.brokerFee = ''
    this.brokerProcessingFee = ''
    this.appraisalFee = ''
    this.include1 = 'Yes'
    this.include2 = 'Yes'
    this.include3 = 'Yes'
    this.appraisedVal1 = ''
    this.appraisedVal2 = ''
    this.appraisedVal3 = ''
    this.totalCosts1 = ''
    this.totalCosts2 = ''
    this.greaterThanTwentyFiveCollateralCondosLessThanSeventyFiveThousand = 'No'
    this.greaterThanTwentyFiveCollateralCondos = 'No'
    this.validationErrors = ''
    this.frontEndValidation = ''
    this.prepaymentPenaltyText = ''
    this.prepaymentPenaltyArray = []
    this.propertyManagementFee = ''
    this.indexSelected = '5yr I/R Swap'
    this.openPrepayOptionSelected = 'Open Prepayment Allowance: 0% UPB/Year'
    this.arm = ''
    this.whiteLabel = false
    this.interestRateValue1 = 0
    this.interestRateValue2 = 0
    this.interestRateValue3 = 0

    this.quoteModelTemplate = [
      {
        in: {
          quoteName: 'Test Renta Quote',
          username: 'virgil.anderson@datameaning.com',
          quoteType: 'Rental',
          overrideMaster: false,
          productType: '10/1 ARM',
          submissionType: 'Standard Retail',
          borrowingEntity: 'TBD',
          borrowerNetWorth: 500000,
          creditScore: 760,
          sreoPropertyNumber: 9,
          numOfProperties: 9,
          totalCosts1: 50000,
          totalCosts2: 50000,
          appraisedVal1: 150000,
          appraisedVal2: 150000,
          appraisedVal3: 150000,
          purchasePrice: 0,
          mortagePayoffs: 1000,
          monthlyRent: 10000,
          annualTaxes: 100,
          annualInsurance: 100,
          annualFloodInsurance: 1,
          annualHOA: 1,
          propertiesNY: 'No',
          borrowerIsReferral: 'No',
          yieldSpendPremium: 0.125,
          refiL1cFixNFlip: 'No',
          buyUpDownOption: 'No Buy Up/Buy Down',
          buyUpDownPercentage: 1,
          prepaymentPenalty: '7 Year Prepayment Penalty: 5, 5, 4, 4, 3, 2, 1',
          brokerFee: 1,
          base: { condo: 'Yes', fixToRent: 'Yes', propertyVal: 'Yes' },
          scenario: [
            { scenarioId: 1, ltvPercentage: 0.65, included: 'Yes' },
            { scenarioId: 2, ltvPercentage: 0.75, included: 'Yes' },
            { scenarioId: 3, ltvPercentage: 0.6, included: 'Yes' },
          ],
        },
        out: {
          'variantId?': '',
          workbookId: 'workbkidajsdflj',
          validQuote: true,
          lastUpdated: '2016-02-28T16:41:41.090Z',
          termSheetTitle: 'title',
          scenarios: [
            {
              ltvEligibility: 'malone',
              eocTitle: 'SingleFamilyRental101ARM',
              borrower: 'malone',
              estimatedValOfProperty: 367,
              numOfProperties: 99,
              estimatedLoanToVal: 99,
              borrowerCreditScore: 99,
              loanAmt: 367,
              payoffMortageProperties: 367,
              purchasePriceProperties: 367,
              estimatedClosingCost: 367,
              originationFee: 367,
              buyUpDownNotOpted: 'tsfest',
              processingFee: 367,
              legalFee: '367',
              brokerFee: 367,
              insuranceCertifications: 367,
              taxImpound: 367,
              insuranceImpound: 367,
              interestReserveEscrow: 367,
              closingCashToBorrower: 367,
              qualifyingInterestRate: 99,
              rateBuyUpDown: 99,
              prepaymentPenalty: 99,
              initFixedInterestRate: 99,
              amortizationPeriod: 'timeperiod',
              monthlyPrincipalInterest: 367,
              monthlyTaxesInsurance: 367,
              monthlyHOADues: 367,
              monthlyTotalPayment: 367,
              monthlyEstimatedRentalIncome: 367,
              rentDebtRatio: 99,
              capStructure: 'malone',
              lifetimeCap: 99,
              floor: 99,
              lookbackPeriod: '99Daya',
              prepaymentPenaltyText: 'malone',
              openPrepayOption: 'malone',
              guarantyType: 'malone',
              liquidReserves: 367,
              buyUpDownFee: 367,
              premier: {
                estManagementReserve: 0,
                indexRate: 0,
                openPrepay: 0,
                interestRateName: 'Spread',
                interestRateValue: 0,
                index: '',
              },
            },
            {
              ltvEligibility: 'malone',
              eocTitle: 'SingleFamilyRental101ARM',
              borrower: 'malone',
              estimatedValOfProperty: 367,
              numOfProperties: 99,
              estimatedLoanToVal: 99,
              borrowerCreditScore: 99,
              loanAmt: 367,
              payoffMortageProperties: 367,
              purchasePriceProperties: 367,
              estimatedClosingCost: 367,
              originationFee: 367,
              buyUpDownNotOpted: 'jhgjhg',
              processingFee: 367,
              legalFee: '367',
              brokerFee: 367,
              insuranceCertifications: 367,
              taxImpound: 367,
              insuranceImpound: 367,
              interestReserveEscrow: 367,
              closingCashToBorrower: 367,
              qualifyingInterestRate: 99,
              rateBuyUpDown: 72,
              prepaymentPenalty: 99,
              initFixedInterestRate: 99,
              amortizationPeriod: '99',
              monthlyPrincipalInterest: 367,
              monthlyTaxesInsurance: 367,
              monthlyHOADues: 367,
              monthlyTotalPayment: 367,
              monthlyEstimatedRentalIncome: 367,
              rentDebtRatio: 99,
              capStructure: 'malone',
              lifetimeCap: 99,
              floor: 99,
              lookbackPeriod: '99Day',
              prepaymentPenaltyText: 'malone',
              openPrepayOption: 'malone',
              guarantyType: 'malone',
              liquidReserves: 367,
              buyUpDownFee: 367,
              premier: {
                estManagementReserve: 0,
                indexRate: 0,
                openPrepay: 0,
                interestRateName: 'Spread',
                interestRateValue: 0,
                index: '',
              },
            },
            {
              ltvEligibility: 'malone',
              eocTitle: 'SingleFamilyRental101ARM',
              borrower: 'malone',
              estimatedValOfProperty: 367,
              numOfProperties: 99,
              estimatedLoanToVal: 99,
              borrowerCreditScore: 99,
              loanAmt: 367,
              payoffMortageProperties: 367,
              purchasePriceProperties: 367,
              estimatedClosingCost: 367,
              originationFee: 367,
              buyUpDownNotOpted: 'b,mnb,mnb',
              processingFee: 367,
              legalFee: '367',
              brokerFee: 367,
              insuranceCertifications: 367,
              taxImpound: 367,
              insuranceImpound: 367,
              interestReserveEscrow: 367,
              closingCashToBorrower: 367,
              qualifyingInterestRate: 99,
              rateBuyUpDown: 99,
              prepaymentPenalty: 99,
              initFixedInterestRate: 99,
              amortizationPeriod: '99',
              monthlyPrincipalInterest: 367,
              monthlyTaxesInsurance: 367,
              monthlyHOADues: 367,
              monthlyTotalPayment: 367,
              monthlyEstimatedRentalIncome: 367,
              rentDebtRatio: 99,
              capStructure: 'malone',
              lifetimeCap: 99,
              floor: 99,
              lookbackPeriod: '45Days',
              prepaymentPenaltyText: 'malone',
              openPrepayOption: 'malone',
              guarantyType: 'malone',
              liquidReserves: 367,
              buyUpDownFee: 367,
              premier: {
                estManagementReserve: 0,
                indexRate: 0,
                openPrepay: 0,
                interestRateName: 'Spread',
                interestRateValue: 0,
                dateRate: '2021-06-21',
                index: '',
              },
            },
          ],
        },
      },
    ]

    this.calcModelTemplate = [
      {
        quoteName: '',
        brokerFee: 0,
        borrowerIsReferral: '',
        appraisedVal1: -0.1,
        creditScore: 0,
        submissionType: '',
        totalCosts2: -0.1,
        annualTaxes: -0.1,
        propertiesNY: '',
        annualInsurance: -0.1,
        refiL1cFixNFlip: '',
        buyUpDownOption: '',
        username: '',
        numOfProperties: 0,
        overrideMaster: false,
        premier: {
          propertyManagementFee: 0,
          collateralCondos: 'No',
          collateral: 'No',
          index: '5yr I/R Swap',
          openPrepayOption: 'Open Prepayment Allowance: 0% UPB/Year',
        },
        annualFloodInsurance: -0.1,
        borrowingEntity: '',
        scenario: [
          {
            scenarioId: 1,
            ltvPercentage: 0,
            included: '',
          },
          {
            scenarioId: 2,
            ltvPercentage: 0,
            included: '',
          },
          {
            scenarioId: 3,
            ltvPercentage: 0,
            included: '',
          },
        ],
        mortagePayoffs: -0.1,
        quoteType: '',
        appraisedVal3: -0.1,
        totalCosts1: -0.1,
        monthlyRent: -0.1,
        sreoPropertyNumber: 0,
        appraisedVal2: -0.1,
        productType: '',
        purchasePrice: -0.1,
        prepaymentPenalty: '',
        borrowerNetWorth: -0.1,
        yieldSpendPremium: 0,
        annualHOA: -0.1,
        base: {
          condo: '',
          fixToRent: '',
          propertyVal: '',
        },
        buyUpDownPercentage: 0,
      },
    ]

    this.currentQuote = [
      {
        in: {
          username: '',
          quoteType: 'Rental',
          overrideMaster: false,
          productType: '5/1 ARM',
          submissionType: 'Inside Sales',
          borrowingEntity: '',
          borrowerNetWorth: 225,
          creditScore: 600,
          sreoPropertyNumber: 22,
          numOfProperties: 3,
          totalCosts1: 335,
          totalCosts2: 335,
          appraisedVal1: 42,
          appraisedVal2: 42,
          appraisedVal3: 42,
          purchasePrice: 82,
          condo: true,
          mortagePayoffs: 85,
          monthlyRent: 85,
          annualTaxes: 85,
          annualInsurance: 85,
          annualFloodInsurance: 85,
          annualHOA: 85,
          propertiesNY: true,
          fixToRent: true,
          borrowerIsReferral: true,
          yieldSpendPremium: 99,
          refiL1cFixNFlip: true,
          buyUpDownOption: 'Rate Buy Up',
          buyUpDownPercentage: 99,
          prepaymentPenalty: '3 Year Prepayment Penalty: 3,2,1',
          brokerFee: 99,
          'premier?': {
            propertyManagementFee: 99,
            collateralCondos: true,
            collateral: true,
            index: '5yr I/R Swap',
          },
          scenario: [
            {
              scenarioId: 1,
              ltvPercentage: 99,
              included: true,
            },
            {
              scenarioId: 2,
              ltvPercentage: 99,
              included: true,
            },
            {
              scenarioId: 3,
              ltvPercentage: 99,
              included: true,
            },
          ],
        },
        out: {
          id: 'bc97h2wb3du3nnp984hnt',
          workbookId: 'workbkidajsdflj',
          validQuote: true,
          lastUpdated: '2016-02-28T16:41:41.090Z',
          termSheetTitle: 'title',
          quoteName: 'guarantor_product_street',
          scenarios: [
            {
              ltvEligibility: 'malone',
              eocTitle: 'SingleFamilyRental101ARM',
              borrower: 'malone',
              estimatedValOfProperty: 367,
              numOfProperties: 99,
              estimatedLoanToVal: 99,
              borrowerCreditScore: '99+',
              loanAmt: 367,
              payoffMortageProperties: 367,
              purchasePriceProperties: 367,
              estimatedClosingCost: 367,
              originationFee: 367,
              buyUpDownNotOpted: 'tsfest',
              processingFee: 367,
              legalFee: 367,
              brokerFee: 367,
              insuranceCertifications: 367,
              taxImpound: 367,
              insuranceImpound: 367,
              interestReserveEscrow: 367,
              closingCashToBorrower: 367,
              qualifyingInterestRate: 99,
              rateBuyUpDown: 99,
              prepaymentPenalty: 99,
              initFixedInterestRate: 99,
              amortizationPeriod: 99,
              monthlyPrincipalInterest: 367,
              monthlyTaxesInsurance: 367,
              monthlyHOADues: 367,
              monthlyTotalPayment: 367,
              monthlyEstimatedRentalIncome: 367,
              rentDebtRatio: 99,
              capStructure: 'malone',
              lifetimeCap: 99,
              floor: 99,
              lookbackPeriod: '99Daya',
              prepaymentPenaltyText: 'malone',
              openPrepayOption: 'malone',
              guarantyType: 'malone',
              liquidReserves: 367,
              premier: {
                estManagementReserve: 367,
                legalFee: 367,
                spread: 99,
                indexRate: 99,
                interestRateValue: 0,
                index: '5yr I/R Swap',
              },
              buyUpDownFee: 367,
            },
            {
              ltvEligibility: 'malone',
              eocTitle: 'SingleFamilyRental101ARM',
              borrower: 'malone',
              estimatedValOfProperty: 367,
              numOfProperties: 99,
              estimatedLoanToVal: 99,
              borrowerCreditScore: '99+',
              loanAmt: 367,
              payoffMortageProperties: 367,
              purchasePriceProperties: 367,
              estimatedClosingCost: 367,
              originationFee: 367,
              buyUpDownNotOpted: 'jhgjhg',
              processingFee: 367,
              legalFee: 367,
              brokerFee: 367,
              insuranceCertifications: 367,
              taxImpound: 367,
              insuranceImpound: 367,
              interestReserveEscrow: 367,
              closingCashToBorrower: 367,
              qualifyingInterestRate: 99,
              rateBuyUpDown: 72,
              prepaymentPenalty: 99,
              initFixedInterestRate: 99,
              amortizationPeriod: 99,
              monthlyPrincipalInterest: 367,
              monthlyTaxesInsurance: 367,
              monthlyHOADues: 367,
              monthlyTotalPayment: 367,
              monthlyEstimatedRentalIncome: 367,
              rentDebtRatio: 99,
              capStructure: 'malone',
              lifetimeCap: 99,
              floor: 99,
              lookbackPeriod: '99Day',
              prepaymentPenaltyText: 'malone',
              openPrepayOption: 'malone',
              guarantyType: 'malone',
              liquidReserves: 367,
              premier: {
                estManagementReserve: 367,
                legalFee: 367,
                spread: 99,
                indexRate: 99,
                interestRateValue: 0,
                index: '5yr I/R Swap',
              },
              buyUpDownFee: 367,
            },
            {
              ltvEligibility: 'malone',
              eocTitle: 'SingleFamilyRental101ARM',
              borrower: 'malone',
              estimatedValOfProperty: 367,
              numOfProperties: 99,
              estimatedLoanToVal: 99,
              borrowerCreditScore: '99+',
              loanAmt: 367,
              payoffMortageProperties: 367,
              purchasePriceProperties: 367,
              estimatedClosingCost: 367,
              originationFee: 367,
              buyUpDownNotOpted: 'b,mnb,mnb',
              processingFee: 367,
              legalFee: 367,
              brokerFee: 367,
              insuranceCertifications: 367,
              taxImpound: 367,
              insuranceImpound: 367,
              interestReserveEscrow: 367,
              closingCashToBorrower: 367,
              qualifyingInterestRate: 99,
              rateBuyUpDown: 99,
              prepaymentPenalty: 99,
              initFixedInterestRate: 99,
              amortizationPeriod: 99,
              monthlyPrincipalInterest: 367,
              monthlyTaxesInsurance: 367,
              monthlyHOADues: 367,
              monthlyTotalPayment: 367,
              monthlyEstimatedRentalIncome: 367,
              rentDebtRatio: 99,
              capStructure: 'malone',
              lifetimeCap: 99,
              floor: 99,
              lookbackPeriod: '45Days',
              prepaymentPenaltyText: 'malone',
              openPrepayOption: 'malone',
              guarantyType: 'malone',
              liquidReserves: 367,
              premier: {
                estManagementReserve: 367,
                legalFee: 367,
                spread: 99,
                indexRate: 99,
                interestRateValue: 0,
                index: '5yr I/R Swap',
              },
              buyUpDownFee: 367,
            },
          ],
        },
      },
    ]

    this.ltvOptions = ['50', '55', '60', '65', '70', '75']
    this.indexPicklist = ['5yr I/R Swap', '10yr I/R Swap']

    this.productTypePicklist = [
      '5/1 ARM',
      '10/1 ARM',
      'Fixed',
      'Premier - 5 Year, 30 Am',
      'Premier - 10 Year, 30 Am',
      'Premier - 5 Year Balloon',
      'Premier - 10 Year Balloon',
      'Premier - 5/1 ARM',
      'Premier - 30 Year Fixed',
    ]

    this.productTypeSelected = '5/1 ARM'

    this.submissionTypePicklist = [
      'Inside Sales',
      'Business Development',
      'Broker',
      'Commercial',
    ]

    this.yieldSpreadPicklist = [
      '0.000',
      '0.125',
      '0.250',
      '0.375',
      '0.500',
      '0.625',
      '0.750',
      '0.875',
      '1.000',
    ]

    this.openPrepayOptionPicklist = [
      'Open Prepayment Allowance: 0% UPB/Year',
      'Open Prepayment Allowance: 5% UPB/Year',
    ]

    this.submissionTypeSelected = 'Inside Sales'

    this.timeOwnedPicklist = [
      'Less than Six Months',
      '6-12 Months',
      'Greater than 12 Months',
    ]

    this.timeOwnedSelected = 'Less than Six Months'

    this.includeOptions = ['Yes', 'No']

    this.buyUpDownPicklist = [
      'Rate Buy Up',
      'Rate Buy Down',
      'No Buy Up/Buy Down',
    ]

    // Update this picklist to be dynamic based on product type
    this.prepaymentPenaltyPicklist = [
      '3 Year Prepayment Penalty: 3, 2, 1',
      '5 Year Prepayment Penalty: 3, 3, 3, 2, 1',
      '7 Year Prepayment Penalty: 5, 5, 4, 4, 3, 2, 1',
      'Yield Spread Maintenance',
      '5 Year Prepayment Penalty: 5, 5, 4, 4, 3',
      '5 Year Prepayment Penalty: 5, 4, 3, 2, 1',
      '10 Yr. Balloon, 30 Am',
      '3 Year Prepayment Penalty: 5, 4, 3',
      '10 Year Prepayment Penalty: 5, 5, 4, 4, 3, 3, 2, 2, 1, 1',
    ]

    this.prePaymentPenaltySelected = '3 Year Prepayment Penalty: 3, 2, 1'
    this.output = false
    this.outputs = []

    this.username = this.route.snapshot.params.username
    this.CurrentDate = new Date()
    this.loading = false
    this.savable = false
    this.printable = false
    this.frontEndValidation = []
    this.quoteType = 'Rental'

    if (this.route.snapshot.params.id) {
      this.id = this.route.snapshot.params.id
      this.getQuoteBackEnd()
      this.output = true
      this.printable = true
      this.savable = true
      return
    }
    this.setBase()
    this.updatePrepay()
  }
}
