import { Routes } from '@angular/router'
import { Rental30Component } from './rental30.component'

export const routes: Routes = [
  {
    path: 'new/:username',
    component: Rental30Component,
  },
  {
    path: 'new/:id/:username',
    component: Rental30Component,
  },
]
