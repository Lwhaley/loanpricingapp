import { ComponentFixture, TestBed } from '@angular/core/testing'

import { Rental30Component } from './rental30.component'

describe('Rental30Component', () => {
  let component: Rental30Component
  let fixture: ComponentFixture<Rental30Component>

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [Rental30Component],
    }).compileComponents()
  })

  beforeEach(() => {
    fixture = TestBed.createComponent(Rental30Component)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
