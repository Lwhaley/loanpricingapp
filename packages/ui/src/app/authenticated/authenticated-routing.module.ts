import { NgModule } from '@angular/core'
import { RouterModule, Routes } from '@angular/router'
import { AuthenticatedComponent } from './authenticated.component'
import { routes as dashboardRoutes } from './dashboard/dashboard.routes'
import { routes as fixandflipRoutes } from './fixandflip/fixandflip.routes'
import { routes as rental30Routes } from './rental30/rental30.routes'

const routes: Routes = [
  {
    path: '',
    component: AuthenticatedComponent,
    children: [
      {
        path: 'fixandflip',
        children: fixandflipRoutes,
      },
      {
        path: 'rental30',
        children: rental30Routes,
      },
      {
        path: 'dashboard',
        children: dashboardRoutes,
      },
      {
        path: '**',
        redirectTo: 'dashboard',
        pathMatch: 'full',
      },
    ],
  },
  {
    path: '**',
    redirectTo: '',
    pathMatch: 'full',
  },
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AuthenticatedRoutingModule {}
