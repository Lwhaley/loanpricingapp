import { HttpClient } from '@angular/common/http'
import { Component, OnInit } from '@angular/core'
import html2canvas from 'html2canvas'
import jspdf from 'jspdf'

// Quality Assurance
let localURL = 'https://pricingqa.azurewebsites.net/api/v1/'

// UAT
if (window.location.href.startsWith('https://pricinguat')) {
  localURL = 'https://pricinguat.azurewebsites.net/api/v1/'
}

if (window.location.href.startsWith('http://pricinguat')) {
  localURL = 'http://pricinguat.azurewebsites.net/api/v1/'
}

// Production
if (window.location.href.startsWith('https://pricingprod')) {
  localURL = 'https://pricingprod.azurewebsites.net/api/v1/'
}

// Local
if (window.location.href.startsWith('http://localhost')) {
  localURL = 'http://localhost:3000/api/v1/'
}

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
})
export class DashboardComponent implements OnInit {
  quotes: any
  quoteFeed: any
  request: any
  userModel: any
  loading: any
  id: any
  username: any
  CurrentDate: any
  overlay: any
  previewSTF: any
  previewRental: any
  previewPremier: any

  currentQuote: any

  title: any
  address: any
  zipCode: any
  city: any
  product: any
  loanPurpose: any
  propertyType: any
  state: any

  // Borrower Info
  borrowingEntity: any
  buildsCompletedInThreeYears: any
  combinedLiquidity: any
  fnfCompletedInThreeYears: any
  primaryGuarantor: any
  qualifyingFICO: any
  rentalUnitsOwned: any
  secondaryGuarantor: any
  citizen: any

  // Deal Terms
  afterCompletedVal: any
  lesserOfLotOrPurchase: any
  payoff: any
  rehabConBudget: any
  targetCloseDate: any
  timeCloseToPayoff: any
  cashout: any
  existingDebtStatus: any
  originalPurchaseDate: any
  refinance: any

  // Loan Term
  brokerFee: any
  guaranty: any
  channel: any
  loanTerm: any
  referralFee: any

  // OUTPUTS
  validationErrors: any
  validQuote: any

  // BlendedLTCCalc
  blendedLTCMaxLoan: any
  maxBlendedLTCRatio: any
  totalCost: any

  // Borrower Info
  borrowerTier: any

  // Cash to Close
  conBudget: any
  estimatedCashAtClose: any
  estimatedLoanAmt: any
  existingPayoff: any
  purchase: any
  totalLoanFeesCost: any

  // LTCCalcOnPurchaseRehab
  LTCConBudget: any
  effectiveBlendedLTC: any
  initFundedLoanAmt: any
  loanAmt: any
  maxLTCOnPurchase: any
  // RehabConBudget: any

  // LTCARVCalc
  LTVMaxLoan: any
  afterCompletedValLTV: any
  maxLTVRatio: any
  totalLoanAmt: any

  // Loan Costs
  brokerFeeCosts: any
  interestReserve: any
  originationFee: any
  processingFee: any
  projectInspectionFee: any
  totalLoanCostFees: any

  // Payments
  initMonthlyPayment: any
  paymentFullyDrawn: any

  // Rate Fee LLPA
  baseOrigFee: any
  baseRate: any
  finalOrigFee: any
  finalRate: any
  LLPAreferallFee: any
  totalOrigFeeLLPA: any
  totalRateLLPA: any

  // Rental Only Fields
  quoteName: any
  overrideMaster: any
  productTypeSelected: any
  submissionTypeSelected: any
  borrowerNetWorth: any
  fico: any
  sreoPropertyNumber: any
  numOfProperties: any
  totalCosts1: any
  totalCosts2: any
  appraisedVal1: any
  appraisedVal2: any
  appraisedVal3: any
  purchasePrice: any
  mortgagePayoffs: any
  numberOfProperties: any
  monthlyRent: any
  annualTaxes: any
  annualInsurance: any
  annualFloodInsurance: any
  annualHOA: any
  propertiesNY: any
  referral: any
  yieldSpreadPremium: any
  refinanceL1CFixNFlip: any
  buyUpDownSelected: any
  buyUpDownPercentage: any
  prepaymentPenalty: any
  ltvPercentage1: any
  ltvPercentage2: any
  ltvPercentage3: any
  propertyLessThanTenThousand: any
  condo: any
  fix2Rent: any
  workbookId: any
  ltvEligibility: any
  eocTitle: any
  borrower: any
  estimatedValOfProperty: any
  estimatedLoanToVal: any
  borrowerCreditScore: any
  payoffMortageProperties: any
  purchasePriceProperties: any
  estimatedClosingCost: any
  buyUpDownNotOpted: any
  legalFee: any
  brokerFee1: any
  insuranceCertifications: any
  taxImpound: any
  insuranceImpound: any
  interestReserveEscrow: any
  closingCashToBorrower: any
  qualifyingInterestRate: any
  rateBuyUpDown: any
  initFixedInterestRate: any
  amortizationPeriod: any
  monthlyPrincipalInterest: any
  monthlyTaxesInsurance: any
  monthlyHOADues: any
  monthlyTotalPayment: any
  monthlyEstimatedRentalIncome: any
  rentDebtRatio: any
  capStructure: any
  lifetimeCap: any
  floor: any
  lookbackPeriod: any
  prepaymentPenaltyText: any
  openPrepayOptionSelected1: any
  guarantyType: any
  liquidReserves: any
  buyUpDownFee: any
  margin: any
  ltvEligibility2: any
  eocTitle2: any
  borrower2: any
  estimatedValOfProperty2: any
  numOfProperties2: any
  estimatedLoanToVal2: any
  borrowerCreditScore2: any
  loanAmt2: any
  payoffMortageProperties2: any
  purchasePriceProperties2: any
  estimatedClosingCost2: any
  originationFee2: any
  buyUpDownNotOpted2: any
  processingFee2: any
  legalFee2: any
  brokerFee2: any
  insuranceCertifications2: any
  taxImpound2: any
  insuranceImpound2: any
  interestReserveEscrow2: any
  closingCashToBorrower2: any
  qualifyingInterestRate2: any
  rateBuyUpDown2: any
  prepaymentPenalty2: any
  initFixedInterestRate2: any
  amortizationPeriod2: any
  monthlyPrincipalInterest2: any
  monthlyTaxesInsurance2: any
  monthlyHOADues2: any
  monthlyTotalPayment2: any
  monthlyEstimatedRentalIncome2: any
  rentDebtRatio2: any
  capStructure2: any
  lifetimeCap2: any
  floor2: any
  lookbackPeriod2: any
  prepaymentPenaltyText2: any
  openPrepayOption2: any
  guarantyType2: any
  liquidReserves2: any
  buyUpDownFee2: any
  margin2: any
  ltvEligibility3: any
  eocTitle3: any
  borrower3: any
  estimatedValOfProperty3: any
  numOfProperties3: any
  estimatedLoanToVal3: any
  borrowerCreditScore3: any
  loanAmt3: any
  payoffMortageProperties3: any
  purchasePriceProperties3: any
  estimatedClosingCost3: any
  originationFee3: any
  buyUpDownNotOpted3: any
  processingFee3: any
  legalFee3: any
  brokerFee3: any
  insuranceCertifications3: any
  taxImpound3: any
  insuranceImpound3: any
  interestReserveEscrow3: any
  closingCashToBorrower3: any
  qualifyingInterestRate3: any
  rateBuyUpDown3: any
  prepaymentPenalty3: any
  initFixedInterestRate3: any
  amortizationPeriod3: any
  monthlyPrincipalInterest3: any
  monthlyTaxesInsurance3: any
  monthlyHOADues3: any
  monthlyTotalPayment3: any
  monthlyEstimatedRentalIncome3: any
  rentDebtRatio3: any
  capStructure3: any
  lifetimeCap3: any
  floor3: any
  lookbackPeriod3: any
  prepaymentPenaltyText3: any
  openPrepayOption3: any
  guarantyType3: any
  liquidReserves3: any
  buyUpDownFee3: any
  margin3: any
  include1: any
  include2: any
  include3: any
  mainColumn: any
  columnWidth: any
  arm: any
  whiteLabel: any
  brokerProcessingFee: any
  appraisalFee: any
  prePaymentPenaltySelected: any
  prepaymentPenaltyArray: any
  indexSelected: any
  indexRate: any
  spread: any
  quoteType: any
  premierPricing: any
  propertyManagementFee: any
  interestRateValue1: any
  interestRateValue2: any
  interestRateValue3: any
  indexRate1: any
  indexRate2: any
  indexRate3: any
  interestRateName1: any
  interestRateName2: any
  interestRateName3: any
  dateRate1: any
  dateRate2: any
  dateRate3: any
  spread1: any
  spread2: any
  spread3: any

  lastUpdated: any
  termSheetTitle: any

  constructor() {
    this.CurrentDate = new Date()
    this.userModel = {
      username: 'string',
    }
    this.loading = false
    this.overlay = false
    this.previewSTF = false
    this.previewRental = false
    this.previewPremier = false
  }

  ngOnInit(): void {
    this.quoteSummary()
  }

  openOverlay(): void {
    this.overlay = true
  }

  /**
   * method formatPrepayText()
   *
   * splits the prepayment penalty string by bullet points
   * for display as an unordered list
   */
  formatPrepayText(): void {
    this.prepaymentPenaltyArray = this.prepaymentPenaltyText.split('\u2022')
  }

  /**
   * method abs()
   *
   * returns the absolute value of an integer
   *
   * @param value integer
   * @returns float
   */
  abs(value: any): any {
    const absValue = Math.abs(value)
    return absValue
  }

  sort(column: any): void {
    switch (column) {
      case 'date':
        this.quotes.sort((a: any, b: any) =>
          a.lastUpdated > b.lastUpdated ? 1 : -1,
        )
        break
      default:
        this.quotes.sort((a: any, b: any) => (a.name > b.name ? 1 : -1))
    }
  }

  closePreview(): void {
    this.previewSTF = false
    this.previewRental = false
    this.previewPremier = false
  }

  pdfPreview(product: any): void {
    this.closePreview()
    this.getQuoteBackEnd(product)

    switch (product.quoteType) {
      case 'STF':
        this.previewSTF = true
        break
      case 'Rental':
        this.previewRental = true
        break
      case 'Premier':
        this.previewPremier = true
        break
      default:
      // Do nothing
    }
  }

  getQuoteBackEnd = async (product: any) => {
    this.loading = true
    const quote = fetch(`${localURL}quote/${product.id}`, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    })
    const response = await quote
    const responseJsonData = await response.json()
    this.currentQuote = responseJsonData.res
    switch (product.quoteType) {
      case 'STF':
        this.setSTF()
        break
      case 'Premier':
      case 'Rental':
        this.setRental()
        this.checkColumns()
        this.formatPrepayText()
        break
      default:
      // Do nothing
    }

    this.loading = false
  }

  buildPDF(whiteLabelPDF: any): void {
    let data = document.getElementById('contentToConvert')!
    if (whiteLabelPDF) {
      data = document.getElementById('whiteLabel')!
    }

    data.style.display = 'block'
    html2canvas(data, { useCORS: true }).then((canvas: any) => {
      const imgWidth = 200
      const imgHeight = (canvas.height * imgWidth) / canvas.width
      const srcpath = canvas.toDataURL('image/png')
      const imageData: string | HTMLCanvasElement | HTMLImageElement = srcpath
      const pdf = new jspdf('p', 'mm', 'a4')
      const position = 0
      pdf.addImage(imageData, 'PNG', 0, position, imgWidth, imgHeight)

      const currentDate = new Date()

      pdf.save(`${this.title}-${currentDate.toLocaleDateString('en-US')}.pdf`)
    })
    data.style.display = 'none'
  }

  /**
   * method generatePDF()
   *
   * Builds an in page PDF based on the loaded quote
   */
  generatePDF(id: string): void {
    this.loading = true
    const data = document.getElementById(id)!

    html2canvas(data, { allowTaint: true }).then((canvas: any) => {
      const currentDate = new Date()
      const imgData = canvas.toDataURL('image/png')
      const pdf = new jspdf('p', 'pt', [canvas.width, canvas.height])
      const pdfWidth = pdf.internal.pageSize.getWidth()
      const pdfHeight = pdf.internal.pageSize.getHeight()
      pdf.addImage(imgData, 'PNG', 0, 0, pdfWidth, pdfHeight)
      pdf.save(
        `${this.quoteName}-${currentDate.toLocaleDateString('en-US')}.pdf`,
      )
    })
    this.closePreview()
    this.loading = false
  }

  /**
   * method checkColumns()
   *
   * determines the correct bootstrap format for the PDF template
   */
  checkColumns(): void {
    let count = 0
    if (this.include1 === 'Yes') {
      count = count + 1
    }
    if (this.include2 === 'Yes') {
      count = count + 1
    }
    if (this.include3 === 'Yes') {
      count = count + 1
    }
    this.mainColumn = 'col-6'
    switch (count) {
      case 1:
        this.columnWidth = 'col-3'
        this.mainColumn = 'col-9'
        break
      case 2:
        this.columnWidth = 'col-3'
        break
      case 3:
        this.columnWidth = 'col-2'
        break
      default:
        this.columnWidth = 'col-2'
    }
    this.setArmClass()
  }

  /**
   * method setArmClass()
   *
   * sets the spacing in the pdf template to execute appropriate
   * paging
   */
  setArmClass(): void {
    switch (this.productTypeSelected) {
      case '5/1 ARM':
      case '10/1 ARM':
      case 'Premier - 5/1 ARM':
        this.arm = 'fixed'
        break
      default:
        this.arm = ''
    }
  }

  quoteSummary = async () => {
    this.id = 'string'
    this.loading = true
    const quotes = fetch(`${localURL}quoteSummaries`, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(this.userModel),
    })
    const response = await quotes
    const quoteJsonData = await response.json()
    this.username = quoteJsonData.res[0].username
    this.quotes = quoteJsonData.res[0].quotes
    this.loading = false
  }

  /**
   * method setRental()
   *
   * sets the attribute values from the currentQuote model
   * for the rental and Rental Premier quote type
   */
  setRental(): void {
    // In
    // In
    this.quoteType = this.currentQuote[0].in.quoteType
    this.productTypeSelected = this.currentQuote[0].in.productType
    this.quoteName = this.currentQuote[0].in.quoteName
    this.overrideMaster = this.currentQuote[0].in.overrideMaster
    this.submissionTypeSelected = this.currentQuote[0].in.submissionType
    this.borrowingEntity = this.currentQuote[0].in.borrowingEntity
    this.borrowerNetWorth = this.currentQuote[0].in.borrowerNetWorth
    this.fico = this.currentQuote[0].in.creditScore
    this.sreoPropertyNumber = this.currentQuote[0].in.sreoPropertyNumber
    this.numOfProperties = this.currentQuote[0].in.numOfProperties
    this.totalCosts1 = this.currentQuote[0].in.totalCosts1
    this.totalCosts2 = this.currentQuote[0].in.totalCosts2
    this.appraisedVal1 = this.currentQuote[0].in.appraisedVal1
    this.appraisedVal2 = this.currentQuote[0].in.appraisedVal2
    this.appraisedVal3 = this.currentQuote[0].in.appraisedVal3
    this.purchasePrice = this.currentQuote[0].in.purchasePrice
    this.mortgagePayoffs = this.currentQuote[0].in.mortagePayoffs
    this.numberOfProperties = this.currentQuote[0].in.numOfProperties
    this.monthlyRent = this.currentQuote[0].in.monthlyRent
    this.annualTaxes = this.currentQuote[0].in.annualTaxes
    this.annualInsurance = this.currentQuote[0].in.annualInsurance
    this.annualFloodInsurance = this.currentQuote[0].in.annualFloodInsurance
    this.annualHOA = this.currentQuote[0].in.annualHOA
    this.propertiesNY = this.currentQuote[0].in.propertiesNY
    this.referral = this.currentQuote[0].in.borrowerIsReferral
    this.yieldSpreadPremium = this.currentQuote[0].in.yieldSpendPremium
      .toFixed(3)
      .toString()
    this.refinanceL1CFixNFlip = this.currentQuote[0].in.refiL1cFixNFlip
    this.buyUpDownSelected = this.currentQuote[0].in.buyUpDownOption
    this.buyUpDownPercentage = this.currentQuote[0].in.buyUpDownPercentage
    this.prePaymentPenaltySelected = this.currentQuote[0].in.prepaymentPenalty
    this.brokerFee = this.currentQuote[0].in.brokerFee
    this.ltvPercentage1 = this.currentQuote[0].in.scenario[0].ltvPercentage
    this.ltvPercentage2 = this.currentQuote[0].in.scenario[1].ltvPercentage
    this.ltvPercentage3 = this.currentQuote[0].in.scenario[2].ltvPercentage

    this.propertyLessThanTenThousand = 'No'
    this.condo = 'No'
    this.fix2Rent = 'No'
    if (this.currentQuote[0].in.base) {
      this.propertyLessThanTenThousand = this.currentQuote[0].in.base.propertyVal
      this.condo = this.currentQuote[0].in.base.condo
      this.fix2Rent = this.currentQuote[0].in.base.fixToRent
    }

    this.include1 = this.currentQuote[0].in.scenario[0].included
    this.include2 = this.currentQuote[0].in.scenario[1].included
    this.include3 = this.currentQuote[0].in.scenario[2].included

    this.brokerProcessingFee = this.currentQuote[0].in.brokerProcessingFee
    this.appraisalFee = this.currentQuote[0].in.appraisalFee

    // Out
    // this.id = this.currentQuote[0].out.id
    this.workbookId = this.currentQuote[0].out.workbookId
    this.validQuote = this.currentQuote[0].out.validQuote
    this.lastUpdated = this.currentQuote[0].out.lastUpdated
    this.termSheetTitle = this.currentQuote[0].out.termSheetTitle

    // Scenario 1
    this.ltvEligibility = this.currentQuote[0].out.scenarios[0].ltvEligibility
    this.eocTitle = this.currentQuote[0].out.scenarios[0].eocTitle
    this.borrower = this.currentQuote[0].out.scenarios[0].borrower
    this.estimatedValOfProperty = this.currentQuote[0].out.scenarios[0].estimatedValOfProperty
    this.numOfProperties = this.currentQuote[0].out.scenarios[0].numOfProperties
    this.estimatedLoanToVal = this.currentQuote[0].out.scenarios[0].estimatedLoanToVal
    this.borrowerCreditScore = this.currentQuote[0].out.scenarios[0].borrowerCreditScore
    this.loanAmt = this.currentQuote[0].out.scenarios[0].loanAmt
    this.payoffMortageProperties = this.currentQuote[0].out.scenarios[0].payoffMortageProperties
    this.purchasePriceProperties = this.currentQuote[0].out.scenarios[0].purchasePriceProperties
    this.estimatedClosingCost = this.currentQuote[0].out.scenarios[0].estimatedClosingCost
    this.originationFee = this.currentQuote[0].out.scenarios[0].originationFee
    this.buyUpDownNotOpted = this.currentQuote[0].out.scenarios[0].buyUpDownNotOpted
    this.processingFee = this.currentQuote[0].out.scenarios[0].processingFee
    this.legalFee = this.currentQuote[0].out.scenarios[0].legalFee
    this.brokerFee1 = this.currentQuote[0].out.scenarios[0].brokerFee
    this.insuranceCertifications = this.currentQuote[0].out.scenarios[0].insuranceCertifications
    this.taxImpound = this.currentQuote[0].out.scenarios[0].taxImpound
    this.insuranceImpound = this.currentQuote[0].out.scenarios[0].insuranceImpound
    this.interestReserveEscrow = this.currentQuote[0].out.scenarios[0].interestReserveEscrow
    this.closingCashToBorrower = this.currentQuote[0].out.scenarios[0].closingCashToBorrower
    this.qualifyingInterestRate = this.currentQuote[0].out.scenarios[0].qualifyingInterestRate
    this.rateBuyUpDown = this.currentQuote[0].out.scenarios[0].rateBuyUpDown
    this.prepaymentPenalty = this.currentQuote[0].out.scenarios[0].prepaymentPenalty
    this.initFixedInterestRate = this.currentQuote[0].out.scenarios[0].initFixedInterestRate
    this.amortizationPeriod = this.currentQuote[0].out.scenarios[0].amortizationPeriod
    this.monthlyPrincipalInterest = this.currentQuote[0].out.scenarios[0].monthlyPrincipalInterest
    this.monthlyTaxesInsurance = this.currentQuote[0].out.scenarios[0].monthlyTaxesInsurance
    this.monthlyHOADues = this.currentQuote[0].out.scenarios[0].monthlyHOADues
    this.monthlyTotalPayment = this.currentQuote[0].out.scenarios[0].monthlyTotalPayment
    this.monthlyEstimatedRentalIncome = this.currentQuote[0].out.scenarios[0].monthlyEstimatedRentalIncome
    this.rentDebtRatio = this.currentQuote[0].out.scenarios[0].rentDebtRatio
    this.capStructure = this.currentQuote[0].out.scenarios[0].capStructure
    this.lifetimeCap = this.currentQuote[0].out.scenarios[0].lifetimeCap
    this.floor = this.currentQuote[0].out.scenarios[0].floor
    this.lookbackPeriod = this.currentQuote[0].out.scenarios[0].lookbackPeriod
    this.prepaymentPenaltyText = this.currentQuote[0].out.scenarios[0].prepaymentPenaltyText
    this.openPrepayOptionSelected1 = this.currentQuote[0].out.scenarios[0].openPrepayOption
    this.guarantyType = this.currentQuote[0].out.scenarios[0].guarantyType
    this.liquidReserves = this.currentQuote[0].out.scenarios[0].liquidReserves
    this.buyUpDownFee = this.currentQuote[0].out.scenarios[0].buyUpDownFee
    this.margin = this.currentQuote[0].out.scenarios[0].margin

    // Scenario 2
    this.ltvEligibility2 = this.currentQuote[0].out.scenarios[1].ltvEligibility
    this.eocTitle2 = this.currentQuote[0].out.scenarios[1].eocTitle
    this.borrower2 = this.currentQuote[0].out.scenarios[1].borrower
    this.estimatedValOfProperty2 = this.currentQuote[0].out.scenarios[1].estimatedValOfProperty
    this.numOfProperties2 = this.currentQuote[0].out.scenarios[1].numOfProperties
    this.estimatedLoanToVal2 = this.currentQuote[0].out.scenarios[1].estimatedLoanToVal
    this.borrowerCreditScore2 = this.currentQuote[0].out.scenarios[1].borrowerCreditScore
    this.loanAmt2 = this.currentQuote[0].out.scenarios[1].loanAmt
    this.payoffMortageProperties2 = this.currentQuote[0].out.scenarios[1].payoffMortageProperties
    this.purchasePriceProperties2 = this.currentQuote[0].out.scenarios[1].purchasePriceProperties
    this.estimatedClosingCost2 = this.currentQuote[0].out.scenarios[1].estimatedClosingCost
    this.originationFee2 = this.currentQuote[0].out.scenarios[1].originationFee
    this.buyUpDownNotOpted2 = this.currentQuote[0].out.scenarios[1].buyUpDownNotOpted
    this.processingFee2 = this.currentQuote[0].out.scenarios[1].processingFee
    this.legalFee2 = this.currentQuote[0].out.scenarios[1].legalFee
    this.brokerFee2 = this.currentQuote[0].out.scenarios[1].brokerFee
    this.insuranceCertifications2 = this.currentQuote[0].out.scenarios[1].insuranceCertifications
    this.taxImpound2 = this.currentQuote[0].out.scenarios[1].taxImpound
    this.insuranceImpound2 = this.currentQuote[0].out.scenarios[1].insuranceImpound
    this.interestReserveEscrow2 = this.currentQuote[0].out.scenarios[1].interestReserveEscrow
    this.closingCashToBorrower2 = this.currentQuote[0].out.scenarios[1].closingCashToBorrower
    this.qualifyingInterestRate2 = this.currentQuote[0].out.scenarios[1].qualifyingInterestRate
    this.rateBuyUpDown2 = this.currentQuote[0].out.scenarios[1].rateBuyUpDown
    this.prepaymentPenalty2 = this.currentQuote[0].out.scenarios[1].prepaymentPenalty
    this.initFixedInterestRate2 = this.currentQuote[0].out.scenarios[1].initFixedInterestRate
    this.amortizationPeriod2 = this.currentQuote[0].out.scenarios[1].amortizationPeriod
    this.monthlyPrincipalInterest2 = this.currentQuote[0].out.scenarios[1].monthlyPrincipalInterest
    this.monthlyTaxesInsurance2 = this.currentQuote[0].out.scenarios[1].monthlyTaxesInsurance
    this.monthlyHOADues2 = this.currentQuote[0].out.scenarios[1].monthlyHOADues
    this.monthlyTotalPayment2 = this.currentQuote[0].out.scenarios[1].monthlyTotalPayment
    this.monthlyEstimatedRentalIncome2 = this.currentQuote[0].out.scenarios[1].monthlyEstimatedRentalIncome
    this.rentDebtRatio2 = this.currentQuote[0].out.scenarios[1].rentDebtRatio
    this.capStructure2 = this.currentQuote[0].out.scenarios[1].capStructure
    this.lifetimeCap2 = this.currentQuote[0].out.scenarios[1].lifetimeCap
    this.floor2 = this.currentQuote[0].out.scenarios[1].floor
    this.lookbackPeriod2 = this.currentQuote[0].out.scenarios[1].lookbackPeriod
    this.prepaymentPenaltyText2 = this.currentQuote[0].out.scenarios[1].prepaymentPenaltyText
    this.openPrepayOption2 = this.currentQuote[0].out.scenarios[1].openPrepayOption
    this.guarantyType2 = this.currentQuote[0].out.scenarios[1].guarantyType
    this.liquidReserves2 = this.currentQuote[0].out.scenarios[1].liquidReserves
    this.buyUpDownFee2 = this.currentQuote[0].out.scenarios[1].buyUpDownFee
    this.margin2 = this.currentQuote[0].out.scenarios[1].margin

    // Scenario 3
    this.ltvEligibility3 = this.currentQuote[0].out.scenarios[2].ltvEligibility
    this.eocTitle3 = this.currentQuote[0].out.scenarios[2].eocTitle
    this.borrower3 = this.currentQuote[0].out.scenarios[2].borrower
    this.estimatedValOfProperty3 = this.currentQuote[0].out.scenarios[2].estimatedValOfProperty
    this.numOfProperties3 = this.currentQuote[0].out.scenarios[2].numOfProperties
    this.estimatedLoanToVal3 = this.currentQuote[0].out.scenarios[2].estimatedLoanToVal
    this.borrowerCreditScore3 = this.currentQuote[0].out.scenarios[2].borrowerCreditScore
    this.loanAmt3 = this.currentQuote[0].out.scenarios[2].loanAmt
    this.payoffMortageProperties3 = this.currentQuote[0].out.scenarios[2].payoffMortageProperties
    this.purchasePriceProperties3 = this.currentQuote[0].out.scenarios[2].purchasePriceProperties
    this.estimatedClosingCost3 = this.currentQuote[0].out.scenarios[2].estimatedClosingCost
    this.originationFee3 = this.currentQuote[0].out.scenarios[2].originationFee
    this.buyUpDownNotOpted3 = this.currentQuote[0].out.scenarios[2].buyUpDownNotOpted
    this.processingFee3 = this.currentQuote[0].out.scenarios[2].processingFee
    this.legalFee3 = this.currentQuote[0].out.scenarios[2].legalFee
    this.brokerFee3 = this.currentQuote[0].out.scenarios[2].brokerFee
    this.insuranceCertifications3 = this.currentQuote[0].out.scenarios[2].insuranceCertifications
    this.taxImpound3 = this.currentQuote[0].out.scenarios[2].taxImpound
    this.insuranceImpound3 = this.currentQuote[0].out.scenarios[2].insuranceImpound
    this.interestReserveEscrow3 = this.currentQuote[0].out.scenarios[2].interestReserveEscrow
    this.closingCashToBorrower3 = this.currentQuote[0].out.scenarios[2].closingCashToBorrower
    this.qualifyingInterestRate3 = this.currentQuote[0].out.scenarios[2].qualifyingInterestRate
    this.rateBuyUpDown3 = this.currentQuote[0].out.scenarios[2].rateBuyUpDown
    this.prepaymentPenalty3 = this.currentQuote[0].out.scenarios[2].prepaymentPenalty
    this.initFixedInterestRate3 = this.currentQuote[0].out.scenarios[2].initFixedInterestRate
    this.amortizationPeriod3 = this.currentQuote[0].out.scenarios[2].amortizationPeriod
    this.monthlyPrincipalInterest3 = this.currentQuote[0].out.scenarios[2].monthlyPrincipalInterest
    this.monthlyTaxesInsurance3 = this.currentQuote[0].out.scenarios[2].monthlyTaxesInsurance
    this.monthlyHOADues3 = this.currentQuote[0].out.scenarios[2].monthlyHOADues
    this.monthlyTotalPayment3 = this.currentQuote[0].out.scenarios[2].monthlyTotalPayment
    this.monthlyEstimatedRentalIncome3 = this.currentQuote[0].out.scenarios[2].monthlyEstimatedRentalIncome
    this.rentDebtRatio3 = this.currentQuote[0].out.scenarios[2].rentDebtRatio
    this.capStructure3 = this.currentQuote[0].out.scenarios[2].capStructure
    this.lifetimeCap3 = this.currentQuote[0].out.scenarios[2].lifetimeCap
    this.floor3 = this.currentQuote[0].out.scenarios[2].floor
    this.lookbackPeriod3 = this.currentQuote[0].out.scenarios[2].lookbackPeriod
    this.prepaymentPenaltyText3 = this.currentQuote[0].out.scenarios[2].prepaymentPenaltyText
    this.openPrepayOption3 = this.currentQuote[0].out.scenarios[2].openPrepayOption
    this.guarantyType3 = this.currentQuote[0].out.scenarios[2].guarantyType
    this.liquidReserves3 = this.currentQuote[0].out.scenarios[2].liquidReserves
    this.buyUpDownFee3 = this.currentQuote[0].out.scenarios[2].buyUpDownFee
    this.margin3 = this.currentQuote[0].out.scenarios[2].margin

    // Set Premier Pricing
    switch (this.quoteType) {
      case 'Premier':
        this.premierPricing = true
        this.propertyManagementFee = this.currentQuote[0].in.premier.propertyManagementFee
        this.indexSelected = this.currentQuote[0].out.scenarios[0].premier.index
        this.interestRateName1 = this.currentQuote[0].out.scenarios[0].premier.interestRateName
        this.interestRateName2 = this.currentQuote[0].out.scenarios[0].premier.interestRateName
        this.interestRateName3 = this.currentQuote[0].out.scenarios[0].premier.interestRateName
        this.interestRateValue1 = this.currentQuote[0].out.scenarios[0].premier.interestRateValue
        this.interestRateValue2 = this.currentQuote[0].out.scenarios[1].premier.interestRateValue
        this.interestRateValue3 = this.currentQuote[0].out.scenarios[2].premier.interestRateValue
        this.indexRate = this.currentQuote[0].out.scenarios[0].premier.indexRate
        this.indexRate1 = this.currentQuote[0].out.scenarios[0].premier.indexRate
        this.indexRate2 = this.currentQuote[0].out.scenarios[1].premier.indexRate
        this.indexRate3 = this.currentQuote[0].out.scenarios[2].premier.indexRate
        this.dateRate1 = this.currentQuote[0].out.scenarios[0].premier.dateRate
        this.dateRate2 = this.currentQuote[0].out.scenarios[1].premier.dateRate
        this.dateRate3 = this.currentQuote[0].out.scenarios[2].premier.dateRate
        this.spread1 = this.currentQuote[0].out.scenarios[0].premier.spread
        this.spread2 = this.currentQuote[0].out.scenarios[1].premier.spread
        this.spread3 = this.currentQuote[0].out.scenarios[2].premier.spread
        break
      default:
        this.premierPricing = false
    }
  }

  setSTF(): void {
    // Inputs
    // Loan Summary
    this.quoteName = this.currentQuote[0].in.quoteName
    this.address = this.currentQuote[0].in.loanSummary.propertyAddress
    this.zipCode = this.currentQuote[0].in.loanSummary.zip
    this.city = this.currentQuote[0].in.loanSummary.city
    this.product = this.currentQuote[0].in.loanSummary.loanProduct
    this.loanPurpose = this.currentQuote[0].in.loanSummary.loanPurpose
    this.propertyType = this.currentQuote[0].in.loanSummary.propertyType
    this.state = this.currentQuote[0].in.loanSummary.state

    // Borrower Info
    this.borrowingEntity = this.currentQuote[0].in.borrowerInfo.borrowingEntity
    this.buildsCompletedInThreeYears = this.currentQuote[0].in.borrowerInfo.buildsCompletedInThreeYears
    this.combinedLiquidity = Number(
      this.currentQuote[0].in.borrowerInfo.combinedLiquidity,
    )
    this.fnfCompletedInThreeYears = this.currentQuote[0].in.borrowerInfo.fnfCompletedInThreeYears
    this.primaryGuarantor = this.currentQuote[0].in.borrowerInfo.primaryGuarantor
    this.qualifyingFICO = this.currentQuote[0].in.borrowerInfo.qualifyingFICO
    this.rentalUnitsOwned = this.currentQuote[0].in.borrowerInfo.rentalUnitsOwned
    this.secondaryGuarantor = this.currentQuote[0].in.borrowerInfo.secondaryGuarantor
    this.citizen = this.currentQuote[0].in.borrowerInfo.usCitizen

    // Deal Terms
    this.afterCompletedVal = Number(
      this.currentQuote[0].in.dealTerms.afterCompletedVal,
    )
    this.lesserOfLotOrPurchase = Number(
      this.currentQuote[0].in.dealTerms.lesserOfLotOrPurchase,
    )
    this.payoff = Number(this.currentQuote[0].in.dealTerms.payoff)
    this.rehabConBudget = Number(
      this.currentQuote[0].in.dealTerms.rehabConBudget,
    )
    this.targetCloseDate = this.currentQuote[0].in.dealTerms.targetCloseDate
    this.timeCloseToPayoff = Number(
      this.currentQuote[0].in.dealTerms.timeCloseToPayoff,
    )
    this.cashout = this.currentQuote[0].in.dealTerms.cashOut
    this.existingDebtStatus = this.currentQuote[0].in.dealTerms.existingDebtStatus
    this.originalPurchaseDate = this.currentQuote[0].in.dealTerms.originalPurchaseDate
    this.refinance = this.currentQuote[0].in.dealTerms.refinance

    // Loan Term
    this.brokerFee = Number(this.currentQuote[0].in.loanTerm.brokerFee)
    this.guaranty = this.currentQuote[0].in.loanTerm.guaranty
    this.channel = this.currentQuote[0].in.loanTerm.channel
    this.loanTerm = this.currentQuote[0].in.loanTerm.loanTerm
    this.referralFee = this.currentQuote[0].in.loanTerm.referralFee

    // OUTPUTS
    this.validationErrors = this.currentQuote[0].out.validationErrors
    this.validQuote = this.currentQuote[0].out.validQuote

    // BlendedLTCCalc
    this.blendedLTCMaxLoan = Number(
      this.currentQuote[0].out.BlendedLTCCalc.blendedLTCMaxLoan,
    )
    this.maxBlendedLTCRatio = Number(
      this.currentQuote[0].out.BlendedLTCCalc.maxBlendedLTCRatio,
    )
    this.totalCost = Number(this.currentQuote[0].out.BlendedLTCCalc.totalCost)

    // Borrower Info
    this.borrowerTier = this.currentQuote[0].out.BorrowerInfo.borrowerTier

    // Cash to Close
    this.conBudget = Number(this.currentQuote[0].out.CashToClose.conBudget)
    this.estimatedCashAtClose = Number(
      this.currentQuote[0].out.CashToClose.estimatedCashAtClose,
    )
    this.estimatedLoanAmt = Number(
      this.currentQuote[0].out.CashToClose.estimatedLoanAmt,
    )
    this.existingPayoff = Number(
      this.currentQuote[0].out.CashToClose.existingPayoff,
    )
    this.purchase = Number(this.currentQuote[0].out.CashToClose.purchase)
    this.totalLoanFeesCost = Number(
      this.currentQuote[0].out.CashToClose.totalLoanFeesCost,
    )

    // LTCCalcOnPurchaseRehab
    this.LTCConBudget = Number(
      this.currentQuote[0].out.LTCCalcOnPurchaseRehab.LTCConBudget,
    )
    this.effectiveBlendedLTC = Number(
      this.currentQuote[0].out.LTCCalcOnPurchaseRehab.effectiveBlendedLTC,
    )
    this.initFundedLoanAmt = Number(
      this.currentQuote[0].out.LTCCalcOnPurchaseRehab.initFundedLoanAmt,
    )
    this.loanAmt = Number(
      this.currentQuote[0].out.LTCCalcOnPurchaseRehab.loanAmt,
    )
    this.maxLTCOnPurchase = Number(
      this.currentQuote[0].out.LTCCalcOnPurchaseRehab.maxLTCOnPurchase,
    )
    // RehabConBudget: any

    // LTCARVCalc
    this.LTVMaxLoan = Number(this.currentQuote[0].out.LTVARVCalc.LTVMaxLoan)
    this.afterCompletedValLTV = Number(
      this.currentQuote[0].out.LTVARVCalc.afterCompletedVal,
    )
    this.maxLTVRatio = Number(this.currentQuote[0].out.LTVARVCalc.maxLTVRatio)
    this.totalLoanAmt = Number(this.currentQuote[0].out.LTVARVCalc.totalLoanAmt)

    // Loan Costs
    this.brokerFeeCosts = Number(this.currentQuote[0].out.LoanCosts.brokerFee)
    this.interestReserve = Number(
      this.currentQuote[0].out.LoanCosts.interestReserve,
    )
    this.originationFee = Number(
      this.currentQuote[0].out.LoanCosts.originationFee,
    )
    this.processingFee = Number(
      this.currentQuote[0].out.LoanCosts.processingFee,
    )
    this.projectInspectionFee = Number(
      this.currentQuote[0].out.LoanCosts.projectInspectionFee,
    )
    this.totalLoanCostFees = Number(
      this.currentQuote[0].out.LoanCosts.totalLoanCostFees,
    )

    // Payments
    this.initMonthlyPayment = Number(
      this.currentQuote[0].out.Payments.initMonthlyPayment,
    )
    this.paymentFullyDrawn = Number(
      this.currentQuote[0].out.Payments.paymentFullyDrawn,
    )

    // Rate Fee LLPA
    this.baseOrigFee = Number(this.currentQuote[0].out.RateFeeLLPA.baseOrigFee)
    this.baseRate = Number(this.currentQuote[0].out.RateFeeLLPA.baseRate)
    this.finalOrigFee = Number(
      this.currentQuote[0].out.RateFeeLLPA.finalOrigFee,
    )
    this.finalRate = Number(this.currentQuote[0].out.RateFeeLLPA.finalRate)
    this.LLPAreferallFee = Number(
      this.currentQuote[0].out.RateFeeLLPA.referralFee,
    )
    this.totalOrigFeeLLPA = Number(
      this.currentQuote[0].out.RateFeeLLPA.totalOrigFeeLLPA,
    )
    this.totalRateLLPA = Number(
      this.currentQuote[0].out.RateFeeLLPA.totalRateLLPA,
    )

    // TopTier Fields
    // id
    // lastUpdated
    // validQuote
    // variantId
  }
}
