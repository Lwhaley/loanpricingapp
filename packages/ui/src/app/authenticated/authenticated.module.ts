import { CommonModule } from '@angular/common'
import { NgModule } from '@angular/core'
import { SharedModule } from '../shared/shared.module'
import { AuthenticatedRoutingModule } from './authenticated-routing.module'
import { AuthenticatedComponent } from './authenticated.component'
import { HeaderComponent } from './components/header/header.component'
import { DashboardComponent } from './dashboard/dashboard.component'
import { FixandflipComponent } from './fixandflip/fixandflip.component'
import { Rental30Component } from './rental30/rental30.component'

@NgModule({
  declarations: [
    AuthenticatedComponent,
    FixandflipComponent,
    Rental30Component,
    HeaderComponent,
    DashboardComponent,
  ],
  imports: [CommonModule, AuthenticatedRoutingModule, SharedModule],
})
export class AuthenticatedModule {}
