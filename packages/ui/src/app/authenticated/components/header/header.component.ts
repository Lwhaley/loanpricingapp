import { Component } from '@angular/core'

const localURL = 'https://limapricingserver.azurewebsites.net/'
// const localURL = 'http://localhost:3000/'
// if (process.env.NODE_ENV === 'development') {
//   localURL = 'http://localhost:3000/'
// }

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent {
  logout = async () => {
    const putQuote = fetch(`${localURL}logout`, {
      method: 'GET',
    })
  }

  constructor() {
    /** empty */
  }
}
