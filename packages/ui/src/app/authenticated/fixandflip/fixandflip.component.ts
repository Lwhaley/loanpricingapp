import { Component } from '@angular/core'
import { async } from '@angular/core/testing'
import { ActivatedRoute, Router } from '@angular/router'
import html2canvas from 'html2canvas'
import jspdf from 'jspdf'

// Quality Assurance
let localURL = 'https://pricingqa.azurewebsites.net/api/v1/'

// UAT
if (window.location.href.startsWith('https://pricinguat')) {
  localURL = 'https://pricinguat.azurewebsites.net/api/v1/'
}

// Production
if (window.location.href.startsWith('https://pricingprod')) {
  localURL = 'https://pricingprod.azurewebsites.net/api/v1/'
}

// Local
if (window.location.href.startsWith('http://localhost')) {
  localURL = 'http://localhost:3000/api/v1/'
}

@Component({
  selector: 'app-fixandflip',
  templateUrl: './fixandflip.component.html',
  styleUrls: ['./fixandflip.component.scss'],
})
export class FixandflipComponent {
  CurrentDate: any
  output: boolean
  tierOverride: boolean
  originationOverride: boolean
  rateOverride: boolean
  masterOverride: boolean
  currentQuote: any
  outputModel: any
  calcModel: any
  savable: any
  printable: any
  id: any
  username: any
  outputs: any
  quoteModel: any
  loading: any
  whiteLabel: any
  whiteLabelButton: any
  validQuote: any
  ficoWarning: any
  zipCodeWarning: any
  preview: any
  rehab: any
  rehabNode: any

  // INPUTS
  // Loan Summary
  title: any
  address: any
  zipCode: any
  city: any
  state: any
  stateInputs: any // picklist
  stateNode: any
  loanPurpose: any
  loanPurposeInput: any // picklist
  selectedLoanPurpose: any
  product: any
  loanProductOptions: any // picklist
  selectedLoanProduct: any
  propertyType: any
  selectedPropertyType: any
  propertyTypeOptions: any // picklist

  // Loan term
  channel: any
  selectedChannel: any
  channelInputs: any // picklist
  brokerFee: any
  referralFee: any
  referralFeeInputs: any // picklist
  selectedFeeReferral: any
  loanTerm: any
  loanTermInputs: any // picklist
  selectedLoanTerm: any
  guaranty: any
  guarantyOptions: any
  selectedGuarantyOption: any
  brokerProcessingFee: any
  appraisalFee: any

  // Deal Term
  targetCloseDate: any
  // Lesser of As-is Lot Value or Purchase Price
  timeCloseToPayoff: any
  lesserOfLotOrPurchase: any
  rehabConBudget: any
  afterCompletedVal: any
  refinance: any
  refinanceOptions: any
  refinanceSelected: any
  originalPurchaseDate: any
  existingDebtStatus: any
  existingDebtSelected: any
  existingDebtInputs: any // picklist
  cashout: any
  cashoutInputs: any // picklist
  cashoutSelected: any
  payoff: any

  // Borrower Information
  primaryGuarantor: any
  secondaryGuarantor: any
  borrowingEntity: any
  fnfCompletedInThreeYears: any
  buildsCompletedInThreeYears: any
  rentalUnitsOwned: any
  qualifyingFICO: any
  combinedLiquidity: any
  citizen: any
  citizenInputs: any // Picklist
  citizenSelected: any

  // OUTPUTS
  // errors
  validationErrors: any
  frontEndValidation: any

  // borrower info
  borrowerTier: any

  // blendedLtcCalc
  // lesser of as is value
  totalCost: any
  blendedLTCMaxLoan: any
  maxBlendedLTCRatio: any

  // LTCCalcOnPurchaseRehab
  initFundedLoanAmt: any
  LTCConBudget: any
  loanAmt: any
  effectiveBlendedLTC: any
  maxLTCOnPurchase: any

  // LTVARVCalc
  LTVMaxLoan: any
  afterCompletedValLTV: any
  maxLTVRatio: any
  totalLoanAmt: any

  // CashToClose
  conBudget: any
  estimatedCashAtClose: any
  estimatedLoanAmt: any
  existingPayoff: any
  purchase: any
  totalLoanFeesCost: any

  // LoanCosts
  brokerFeeCosts: any
  interestReserve: any
  originationFee: any
  processingFee: any
  projectInspectionFee: any
  totalLoanCostFees: any

  // RateFeeLLPA
  baseOrigFee: any
  baseRate: any
  finalOrigFee: any
  finalRate: any
  totalOrigFeeLLPA: any
  totalRateLLPA: any
  LLPAreferallFee: any

  // Payments
  initMonthlyPayment: any
  paymentFullyDrawn: any

  /**
   * method abs()
   *
   * returns the absolute value of an integer
   *
   * @param value integer
   * @returns float
   */
  abs(value: any): any {
    const absValue = Math.abs(value)
    return absValue
  }

  /**
   * method validateField()
   *
   * validates the format of the given field
   *
   * @param field string
   */
  validateField(field: any): void {
    switch (field) {
      case 'fico':
        const ficoPattern = new RegExp('^[0-9]+$')
        const ficoResult = ficoPattern.test(this.qualifyingFICO)
        this.ficoWarning = false
        if (
          !ficoResult ||
          this.qualifyingFICO > 850 ||
          this.qualifyingFICO < 300
        ) {
          this.ficoWarning = true
        }
        break
      case 'zipCode':
        const zipPattern = new RegExp('^[0-9]{5}(?:-[0-9]{4})?$')
        const zipResult = zipPattern.test(this.zipCode)
        this.zipCodeWarning = false
        if (!zipResult) {
          this.zipCodeWarning = true
        }
        break
      default:
      // Do Nothing
    }
    this.unsavable()
  }

  /**
   * method generatePreview()
   *
   * Displays the appropriate template preview
   *
   * @param whiteLabel boolean
   */
  generatePreview(whiteLabel: any = false): void {
    this.whiteLabel = whiteLabel
    this.preview = true
  }

  /**
   * method closePreview()
   *
   * closes the preview overlay window
   */
  closePreview(): void {
    this.preview = false
  }

  /**
   * method generatePDF()
   *
   * generates a PDF from the loaded quote
   *
   * @param whiteLabelPDF boolean
   */
  generatePDF(): void {
    this.loading = true
    const data = document.getElementById('STF')!

    html2canvas(data, { allowTaint: true }).then((canvas: any) => {
      const currentDate = new Date()
      const imgData = canvas.toDataURL('image/png')
      const pdf = new jspdf('p', 'pt', [canvas.width, canvas.height])
      const pdfWidth = pdf.internal.pageSize.getWidth()
      const pdfHeight = pdf.internal.pageSize.getHeight()
      pdf.addImage(imgData, 'PNG', 0, 0, pdfWidth, pdfHeight)
      pdf.save(`${this.title}-${currentDate.toLocaleDateString('en-US')}.pdf`)
    })
    this.closePreview()
    this.loading = false
  }

  /**
   * method unsaveable()
   *
   * sets the value of the unsavable attribute to false
   */
  unsavable(): void {
    this.savable = false
    this.unprintable()
  }

  /**
   * method unprintable()
   *
   * sets the value of printable to false
   */
  unprintable(): void {
    this.printable = false
  }

  /**
   * method clearInputs()
   *
   * sets the value of all the page's inputs to ''
   */
  clearInputs(): void {
    const inputs = document.querySelectorAll('input')

    inputs.forEach((key: any, val: any) => {
      key.value = ''
    })
  }

  /**
   * method valid()
   *
   * returns an array of required fields which are not filled out
   *
   * @returns array
   */
  validate(): any {
    const fields = []

    if (this.title === '') {
      fields.push('Quote Name')
    }
    if (this.loanPurpose === '') {
      fields.push('Loan Purpose')
    }
    if (this.product === '') {
      fields.push('Loan Product')
    }
    if (this.address === '') {
      fields.push('Property Address')
    }
    if (this.city === '') {
      fields.push('City')
    }
    if (this.state === '') {
      fields.push('State')
    }
    if (this.zipCode === '') {
      fields.push('Zip Code')
    }
    if (this.propertyType === '') {
      fields.push('Property Type')
    }
    if (this.channel === '') {
      fields.push('Channel')
    }
    if (this.loanTerm === '') {
      fields.push('Loan Term')
    }
    if (this.guaranty === '') {
      fields.push('Guaranty')
    }
    if (this.targetCloseDate === '') {
      fields.push('Target Close Date')
    }
    if (this.timeCloseToPayoff === '') {
      fields.push('Time Close To Payoff')
    }
    if (this.lesserOfLotOrPurchase === '') {
      fields.push('Lesser of lot or purchase')
    }
    if (this.afterCompletedVal === '') {
      fields.push('After Completed Value')
    }
    if (this.refinance === '') {
      fields.push('Refinance')
    }
    if (this.existingDebtStatus === '') {
      fields.push('Existing Debt Status')
    }
    if (this.cashout === '') {
      fields.push('Cashout')
    }
    if (this.primaryGuarantor === '') {
      fields.push('Primary Guarantor')
    }
    if (this.borrowingEntity === '') {
      fields.push('Borrowing Entity')
    }
    if (this.qualifyingFICO === '') {
      fields.push('Qualifying FICO')
    }
    if (this.combinedLiquidity === '') {
      fields.push('Combined Liquidity')
    }
    if (this.citizen === '') {
      fields.push('Citizen')
    }

    return fields
  }

  /**
   * method toggleOverride()
   *
   * toggles the value of the override field passed in
   *
   * @param node string
   */
  toggleOverride(node: any): void {
    switch (node) {
      case 'tier':
        this.tierOverride = !this.tierOverride
        break
      case 'origination':
        this.originationOverride = !this.originationOverride
        break
      case 'rate':
        this.rateOverride = !this.rateOverride
        break
      case 'master':
        this.masterOverride = !this.masterOverride
        break
      default:
      // Do Nothing
    }
  }

  /**
   * method setDropDowns()
   *
   * sets the selected value of the dropdowns based on the loaded attributes
   */
  setDropDowns(): void {
    // Rehab
    this.rehabNode = document.getElementById('rehab')
    this.rehabNode.selected = this.rehab === 'Yes' ? true : false

    // Set state
    this.stateNode = document.getElementById(this.state)
    this.stateNode.selected = true

    // Set loan purpose
    this.selectedLoanPurpose = document.getElementById(this.loanPurpose)
    this.selectedLoanPurpose.selected = true

    // Set loan product
    this.selectedLoanProduct = document.getElementById(this.product)
    this.selectedLoanProduct.selected = true

    // Set select channel
    this.selectedChannel = document.getElementById(this.channel)
    this.selectedChannel.selected = true
    this.whiteLabelButton = false
    if (this.channel === 'Broker') {
      this.whiteLabelButton = true
    }

    // Set selected fee referral
    this.selectedFeeReferral = document.getElementById(this.referralFee)
    this.selectedFeeReferral.selected = true

    // Set selected property type
    this.selectedPropertyType = document.getElementById(this.propertyType)
    this.selectedPropertyType.selected = true

    // Set Loan Term
    this.selectedLoanTerm = document.getElementById(this.loanTerm)
    this.selectedLoanTerm.selected = true

    // Set Guaranty
    this.selectedGuarantyOption = document.getElementById(
      `guaranty_${this.guaranty}`,
    )
    this.selectedGuarantyOption.selected = true

    // Set Refinance
    this.refinanceSelected = document.getElementById(`refi_${this.refinance}`)
    this.refinanceSelected.selected = true

    // Set existing debt status
    this.existingDebtSelected = document.getElementById(
      `existing_${this.existingDebtStatus}`,
    )
    this.existingDebtSelected.selected = true

    // Set Citizen Status
    this.citizenSelected = document.getElementById(`citizen_${this.citizen}`)
    this.citizenSelected.selected = true

    // Set Cashout Status
    this.cashoutSelected = document.getElementById(`cashout_${this.cashout}`)
    this.cashoutSelected.selected = true
  }

  /**
   * method setValue()
   *
   * sets the value of the node attribute based on the value
   * passed in via the event object
   *
   * @param node string
   * @param event object
   */
  setValue(node: any, event: any): void {
    switch (node) {
      case 'rehab':
        this.rehab = event.target.checked ? 'Yes' : 'No'
        break
      case 'refinance':
        this.refinance = event.target.value
        break
      case 'propertyType':
        this.propertyType = event.target.value
        break
      case 'state':
        this.state = event.target.value
        break
      case 'channel':
        this.channel = event.target.value
        this.whiteLabelButton = false
        if (this.channel === 'Broker') {
          this.whiteLabelButton = true
        }
        break
      case 'purpose':
        this.loanPurpose = event.target.value
        break
      case 'referral':
        this.referralFee = event.target.value
        break
      case 'product':
        if (event.target.value !== null) {
          this.product = event.target.value
        }
        break
      case 'term':
        this.loanTerm = event.target.value
        break
      case 'guaranty':
        this.guaranty = event.target.value
        break
      case 'debt':
        this.existingDebtStatus = event.target.value
        break
      case 'citizen':
        this.citizen = event.target.value
        break
      case 'cashout':
        this.cashout = event.target.value
        break
      default:
      // Do Nothing
    }
    this.savable = false
    this.unprintable()
  }

  /**
   * method isSaveAble()
   *
   * sets the saveble attribute
   */
  isSaveAble(): void {
    let saveAble = true
    const fields = this.validate()

    if (fields.length > 1) {
      saveAble = false
    }
    if (this.validationErrors.length > 1) {
      saveAble = false
    }

    this.savable = saveAble
  }

  // Methods to control API Calls

  /**
   * method calculateResults()
   *
   * calculates the current quote
   *
   * @returns null
   */
  calculateResults(): void {
    this.frontEndValidation = []
    this.output = true
    this.savable = false
    this.setCalcModel()
    const fields = this.validate()

    // console.log(JSON.stringify(this.calcModel))

    if (fields.length < 1) {
      this.runCalcBackEnd()
      return
    }
    this.frontEndValidation = fields
  }

  /**
   * method saveQuote()
   *
   * saves the current quote
   */
  saveQuote(): void {
    this.setQuoteModel()
    switch (this.id) {
      case '':
        this.backEndSaveCall()
        break
      default:
        this.backEndPutCall()
        break
    }
  }

  /**
   * method isPrintable()
   *
   * sets the value of the printable attribute
   */
  isPrintable(): void {
    this.printable = false
    if (
      (this.validationErrors.length === 0 &&
        this.frontEndValidation.length === 0) ||
      this.rateOverride ||
      this.tierOverride ||
      this.masterOverride ||
      this.originationOverride
    ) {
      this.printable = true
    }
  }

  /**
   * method backEndPutCall()
   *
   * updates the loaded quote via the mulesoft API
   *
   * @returns error
   */
  backEndPutCall = async () => {
    this.loading = true
    const putResponse = await fetch(`${localURL}quotes/${this.id}`, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(this.quoteModel),
    })
      .then((response: any) => {
        if (response.status === 200) {
          return response.json()
        }
        return Promise.reject(response)
      })
      .catch((err: any) => {
        console.warn(`Error: ${err}`)
        const error = [
          {
            error: 'There was an error processing your request',
          },
        ]
        return error
      })
    this.isPrintable()
    this.loading = false
    if (typeof putResponse.res === 'undefined') {
      this.validationErrors = 'There was an error processing your request'
      return
    }
  }

  /**
   * method backEndSaveCall()
   *
   * saves a new quote via the Mulesoft API
   *
   * @returns error
   */
  backEndSaveCall = async () => {
    this.loading = true
    this.validationErrors = ''
    const resultJsonData = await fetch(`${localURL}quotes`, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(this.quoteModel),
    })
      .then((response: any) => {
        if (response.status === 200) {
          return response.json()
        }
        return Promise.reject(response)
      })
      .catch((err: any) => {
        console.warn(`Error: ${err}`)
        const error = [
          {
            error: 'There was an error processing your request',
          },
        ]
        return error
      })
    this.isPrintable()
    this.loading = false
    if (typeof resultJsonData.res === 'undefined') {
      this.validationErrors = 'There was an error processing your request'
      return
    }
    this.id = resultJsonData.res[0].out.id
  }

  /**
   * method getQuoteBackEnd()
   *
   * loads the existing quote from the Mulesoft API
   */
  getQuoteBackEnd = async () => {
    this.loading = true
    const quote = fetch(`${localURL}quote/${this.id}`, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    })
    const response = await quote
    const responseJsonData = await response.json()
    this.currentQuote = responseJsonData.res
    this.setInputs()
    this.setDropDowns()
    this.isPrintable()
    this.loading = false
  }

  /**
   * method runCalcBackEnd()
   *
   * sends the calculate call to the backend server to calculate a quote
   *
   * @returns error
   */
  runCalcBackEnd = async () => {
    this.loading = true
    const calcResponse = await fetch(`${localURL}calculations`, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(this.calcModel),
    })
      .then((response: any) => {
        if (response.status === 200) {
          return response.json()
        }
        return Promise.reject(response)
      })
      .catch((err: any) => {
        console.warn(`Error: ${err}`)
      })

    this.loading = false
    if (typeof calcResponse.res === 'undefined') {
      this.validationErrors = 'There was an error processing your request'
      return
    }

    this.outputs = calcResponse.res
    this.setOutputs()
    this.isSaveAble()
    this.printable = false
  }

  /**
   * method normalizePercentages()
   *
   * sets the rate to the appropriate float value for percentages
   */
  normalizePercentages(): void {
    if (this.finalRate > 1) {
      this.finalRate = this.finalRate * 0.01
    }
    if (this.finalOrigFee > 1) {
      this.finalOrigFee = this.finalOrigFee * 0.01
    }
  }

  /**
   * method setQuoteModel()
   *
   * sets the values of the quote attributes for a save quote call
   */
  setQuoteModel(): void {
    // Set Inputs

    // Top Tier Inputs
    this.quoteModel[0].in.username = this.username
    this.quoteModel[0].in.overrideMaster = this.masterOverride
    this.quoteModel[0].in.quoteName = this.title

    this.quoteModel[0].in.exceptionPricingOverride.enabled = false
    if (this.tierOverride || this.originationOverride || this.rateOverride) {
      this.quoteModel[0].in.exceptionPricingOverride.rate = this.finalRate
      this.quoteModel[0].in.exceptionPricingOverride.tier = this.borrowerTier.toString()
      this.quoteModel[0].in.exceptionPricingOverride.fee = this.finalOrigFee
      this.quoteModel[0].in.exceptionPricingOverride.enabled = true
    }

    // loanSummary
    this.quoteModel[0].in.loanSummary.loanPurpose = this.loanPurpose
    this.quoteModel[0].in.loanSummary.loanProduct = this.product
    this.quoteModel[0].in.loanSummary.propertyAddress = this.address
    this.quoteModel[0].in.loanSummary.city = this.city
    this.quoteModel[0].in.loanSummary.state = this.state
    this.quoteModel[0].in.loanSummary.zip = this.zipCode
    this.quoteModel[0].in.loanSummary.propertyType = this.propertyType

    // loanTerm
    this.quoteModel[0].in.loanTerm.channel = this.channel
    this.quoteModel[0].in.loanTerm.referralFee = this.referralFee
    this.quoteModel[0].in.loanTerm.brokerFee = Number(this.brokerFee)
    this.quoteModel[0].in.loanTerm.loanTerm = this.loanTerm
    this.quoteModel[0].in.loanTerm.guaranty = this.guaranty
    this.quoteModel[0].in.loanTerm.appraisalFee = Number(this.appraisalFee)
    this.quoteModel[0].in.loanTerm.brokerProcessingFee = Number(
      this.brokerProcessingFee,
    )

    // dealTerms
    this.quoteModel[0].in.dealTerms.targetCloseDate = this.targetCloseDate
    this.quoteModel[0].in.dealTerms.timeCloseToPayoff = Number(
      this.timeCloseToPayoff,
    )
    this.quoteModel[0].in.dealTerms.lesserOfLotOrPurchase = Number(
      this.lesserOfLotOrPurchase,
    )
    this.quoteModel[0].in.dealTerms.rehabConBudget = Number(this.rehabConBudget)
    this.quoteModel[0].in.dealTerms.afterCompletedVal = Number(
      this.afterCompletedVal,
    )
    this.quoteModel[0].in.dealTerms.refinance = this.refinance
    this.quoteModel[0].in.dealTerms.originalPurchaseDate = this.originalPurchaseDate
    this.quoteModel[0].in.dealTerms.existingDebtStatus = this.existingDebtStatus
    this.quoteModel[0].in.dealTerms.payoff = Number(this.payoff)
    this.quoteModel[0].in.dealTerms.cashOut = this.cashout

    // borrowerInfo
    this.quoteModel[0].in.borrowerInfo.primaryGuarantor = this.primaryGuarantor
    this.quoteModel[0].in.borrowerInfo.secondaryGuarantor = this.secondaryGuarantor
    this.quoteModel[0].in.borrowerInfo.borrowingEntity = this.borrowingEntity
    this.quoteModel[0].in.borrowerInfo.fnfCompletedInThreeYears = Number(
      this.fnfCompletedInThreeYears,
    )
    this.quoteModel[0].in.borrowerInfo.buildsCompletedInThreeYears = Number(
      this.buildsCompletedInThreeYears,
    )
    this.quoteModel[0].in.borrowerInfo.rentalUnitsOwned = Number(
      this.rentalUnitsOwned,
    )
    this.quoteModel[0].in.borrowerInfo.qualifyingFICO = Number(
      this.qualifyingFICO,
    )
    this.quoteModel[0].in.borrowerInfo.combinedLiquidity = Number(
      this.combinedLiquidity,
    )
    this.quoteModel[0].in.borrowerInfo.usCitizen = this.citizen

    // outputs
    // top tier
    this.quoteModel[0].out.validQuote = this.validQuote
    this.quoteModel[0].out.validationErrors = this.validationErrors

    // borrowerInfo
    this.quoteModel[0].out.BorrowerInfo.borrowerTier = this.borrowerTier.toString()

    // BlendedLTCCalc
    this.quoteModel[0].out.BlendedLTCCalc.maxBlendedLTCRatio = this.maxBlendedLTCRatio
    this.quoteModel[0].out.BlendedLTCCalc.totalCost = this.totalCost
    this.quoteModel[0].out.BlendedLTCCalc.blendedLTCMaxLoan = this.blendedLTCMaxLoan

    // LTCCalcOnPurchaseRehab
    this.quoteModel[0].out.LTCCalcOnPurchaseRehab.maxLTCOnPurchase = this.maxLTCOnPurchase
    this.quoteModel[0].out.LTCCalcOnPurchaseRehab.initFundedLoanAmt = this.initFundedLoanAmt
    this.quoteModel[0].out.LTCCalcOnPurchaseRehab.LTCConBudget = this.LTCConBudget
    this.quoteModel[0].out.LTCCalcOnPurchaseRehab.loanAmt = this.loanAmt
    this.quoteModel[0].out.LTCCalcOnPurchaseRehab.effectiveBlendedLTC = this.effectiveBlendedLTC

    // LTVARVCalc
    this.quoteModel[0].out.LTVARVCalc.afterCompletedVal = Number(
      this.afterCompletedVal,
    )
    this.quoteModel[0].out.LTVARVCalc.maxLTVRatio = this.maxLTVRatio
    this.quoteModel[0].out.LTVARVCalc.LTVMaxLoan = this.LTVMaxLoan
    this.quoteModel[0].out.LTVARVCalc.totalLoanAmt = this.totalLoanAmt

    // CashToClose
    this.quoteModel[0].out.CashToClose.purchase = this.purchase
    this.quoteModel[0].out.CashToClose.existingPayoff = this.existingPayoff
    this.quoteModel[0].out.CashToClose.conBudget = this.conBudget
    this.quoteModel[0].out.CashToClose.totalLoanFeesCost = Number(
      this.totalLoanFeesCost.toFixed(2),
    )
    this.quoteModel[0].out.CashToClose.estimatedLoanAmt = this.estimatedLoanAmt
    this.quoteModel[0].out.CashToClose.estimatedCashAtClose = Number(
      this.estimatedCashAtClose.toFixed(2),
    )

    // LoanCosts
    this.quoteModel[0].out.LoanCosts.brokerFee = Number(this.brokerFeeCosts)
    this.quoteModel[0].out.LoanCosts.originationFee = Number(
      this.originationFee.toFixed(2),
    )
    this.quoteModel[0].out.LoanCosts.processingFee = this.processingFee
    this.quoteModel[0].out.LoanCosts.projectInspectionFee = this.projectInspectionFee
    this.quoteModel[0].out.LoanCosts.totalLoanCostFees = Number(
      this.totalLoanCostFees.toFixed(2),
    )
    this.quoteModel[0].out.LoanCosts.interestReserve = this.interestReserve

    // RateFeeLLPA
    this.quoteModel[0].out.RateFeeLLPA.baseRate = this.baseRate
    this.quoteModel[0].out.RateFeeLLPA.totalRateLLPA = this.totalRateLLPA
    this.quoteModel[0].out.RateFeeLLPA.finalRate = this.finalRate
    this.quoteModel[0].out.RateFeeLLPA.baseOrigFee = this.baseOrigFee
    this.quoteModel[0].out.RateFeeLLPA.totalOrigFeeLLPA = this.totalOrigFeeLLPA
    this.quoteModel[0].out.RateFeeLLPA.referralFee = this.LLPAreferallFee
    this.quoteModel[0].out.RateFeeLLPA.finalOrigFee = this.finalOrigFee

    // Payments
    this.quoteModel[0].out.Payments.initMonthlyPayment = this.initMonthlyPayment
    this.quoteModel[0].out.Payments.paymentFullyDrawn = this.paymentFullyDrawn
  }

  /**
   * method setCalcModel()
   *
   * sets the attributes of the calcModel in preparation for the
   * calculation API call
   */
  setCalcModel(): void {
    // Top Level Fields
    this.calcModel[0].username = this.username
    this.calcModel[0].quoteType = 'STF'
    this.calcModel[0].overrideMaster = this.masterOverride
    this.calcModel[0].quoteName = this.title

    this.calcModel[0].exceptionPricingOverride.enabled = false
    if (this.tierOverride || this.originationOverride || this.rateOverride) {
      this.normalizePercentages()
      this.calcModel[0].exceptionPricingOverride.rate = Number(this.finalRate)
      this.calcModel[0].exceptionPricingOverride.tier = this.borrowerTier.toString()
      this.calcModel[0].exceptionPricingOverride.fee = this.finalOrigFee
      this.calcModel[0].exceptionPricingOverride.enabled = true
    }

    // Loan Summary Fields
    this.calcModel[0].loanSummary.loanPurpose = this.loanPurpose
    this.calcModel[0].loanSummary.loanProduct = this.product
    this.calcModel[0].loanSummary.propertyAddress = this.address
    this.calcModel[0].loanSummary.city = this.city
    this.calcModel[0].loanSummary.state = this.state
    this.calcModel[0].loanSummary.zip = this.zipCode
    this.calcModel[0].loanSummary.propertyType = this.propertyType

    // Loan Term Fields
    this.calcModel[0].loanTerm.channel = this.channel
    this.calcModel[0].loanTerm.referralFee = this.referralFee
    // if (Number(this.brokerFee) > 1) {
    //   this.brokerFee = Number(this.brokerFee) * 0.01
    // }
    this.calcModel[0].loanTerm.brokerFee = Number(this.brokerFee)
    this.calcModel[0].loanTerm.loanTerm = this.loanTerm
    this.calcModel[0].loanTerm.guaranty = this.guaranty
    this.calcModel[0].loanTerm.appraisalFee = Number(this.appraisalFee)
    this.calcModel[0].loanTerm.brokerProcessingFee = Number(
      this.brokerProcessingFee,
    )

    // Deal Term Fields
    this.calcModel[0].dealTerms.targetCloseDate = this.targetCloseDate
    this.calcModel[0].dealTerms.timeCloseToPayoff = Number(
      this.timeCloseToPayoff,
    )
    this.calcModel[0].dealTerms.lesserOfLotOrPurchase = Number(
      this.lesserOfLotOrPurchase,
    )
    this.calcModel[0].dealTerms.rehabConBudget = Number(this.rehabConBudget)
    this.calcModel[0].dealTerms.afterCompletedVal = Number(
      this.afterCompletedVal,
    )
    this.calcModel[0].dealTerms.refinance = this.refinance
    this.calcModel[0].dealTerms.existingDebtStatus = this.existingDebtStatus
    this.calcModel[0].dealTerms.cashOut = this.cashout
    this.calcModel[0].dealTerms.heavyRehab = this.rehab

    // BorrowerInfo
    this.calcModel[0].borrowerInfo.primaryGuarantor = this.primaryGuarantor
    this.calcModel[0].borrowerInfo.secondaryGuarantor = this.secondaryGuarantor
    this.calcModel[0].borrowerInfo.borrowingEntity = this.borrowingEntity
    this.calcModel[0].borrowerInfo.fnfCompletedInThreeYears = Number(
      this.fnfCompletedInThreeYears,
    )
    this.calcModel[0].borrowerInfo.buildsCompletedInThreeYears = Number(
      this.buildsCompletedInThreeYears,
    )
    this.calcModel[0].borrowerInfo.rentalUnitsOwned = Number(
      this.rentalUnitsOwned,
    )
    this.calcModel[0].borrowerInfo.qualifyingFICO = Number(this.qualifyingFICO)
    this.calcModel[0].borrowerInfo.combinedLiquidity = Number(
      this.combinedLiquidity,
    )
    this.calcModel[0].borrowerInfo.usCitizen = this.citizen
  }

  /**
   * method setOutputs()
   *
   * stores the output values returned from the calc API call
   */
  setOutputs(): void {
    // Outputs
    this.validationErrors = this.outputs.out.validationErrors
    this.validQuote = this.outputs.out.validQuote

    // BlendedLTCCalc
    this.blendedLTCMaxLoan = Number(
      this.outputs.out.BlendedLTCCalc.blendedLTCMaxLoan,
    )
    this.maxBlendedLTCRatio = Number(
      this.outputs.out.BlendedLTCCalc.maxBlendedLTCRatio,
    )
    this.totalCost = Number(this.outputs.out.BlendedLTCCalc.totalCost)

    // Borrower Info
    this.borrowerTier = this.outputs.out.BorrowerInfo.borrowerTier

    // Cash to Close
    this.conBudget = Number(this.outputs.out.CashToClose.conBudget)
    this.estimatedCashAtClose = Number(
      this.outputs.out.CashToClose.estimatedCashAtClose,
    )
    this.estimatedLoanAmt = Number(
      this.outputs.out.CashToClose.estimatedLoanAmt,
    )
    this.existingPayoff = Number(this.outputs.out.CashToClose.existingPayoff)
    this.purchase = Number(this.outputs.out.CashToClose.purchase)
    this.totalLoanFeesCost = Number(
      this.outputs.out.CashToClose.totalLoanFeesCost,
    )

    // LTCCalcOnPurchaseRehab
    this.LTCConBudget = Number(
      this.outputs.out.LTCCalcOnPurchaseRehab.LTCConBudget,
    )
    this.effectiveBlendedLTC = Number(
      this.outputs.out.LTCCalcOnPurchaseRehab.effectiveBlendedLTC,
    )
    this.initFundedLoanAmt = Number(
      this.outputs.out.LTCCalcOnPurchaseRehab.initFundedLoanAmt,
    )
    this.loanAmt = Number(this.outputs.out.LTCCalcOnPurchaseRehab.loanAmt)
    this.maxLTCOnPurchase = Number(
      this.outputs.out.LTCCalcOnPurchaseRehab.maxLTCOnPurchase,
    )
    // RehabConBudget: any

    // LTCARVCalc
    this.LTVMaxLoan = Number(this.outputs.out.LTVARVCalc.LTVMaxLoan)
    this.afterCompletedValLTV = Number(
      this.outputs.out.LTVARVCalc.afterCompletedVal,
    )
    this.maxLTVRatio = Number(
      this.outputs.out.LTVARVCalc.maxLTVRatio.toFixed(2),
    )
    this.totalLoanAmt = Number(
      this.outputs.out.LTVARVCalc.totalLoanAmt.toFixed(2),
    )

    // Loan Costs
    this.brokerFeeCosts = Number(this.outputs.out.LoanCosts.brokerFee)
    this.interestReserve = Number(this.outputs.out.LoanCosts.interestReserve)
    this.originationFee = Number(this.outputs.out.LoanCosts.originationFee)
    this.processingFee = Number(this.outputs.out.LoanCosts.processingFee)
    this.projectInspectionFee = Number(
      this.outputs.out.LoanCosts.projectInspectionFee,
    )
    this.totalLoanCostFees = Number(
      this.outputs.out.LoanCosts.totalLoanCostFees,
    )

    // Payments
    this.initMonthlyPayment = Number(
      this.outputs.out.Payments.initMonthlyPayment,
    )
    this.paymentFullyDrawn = Number(this.outputs.out.Payments.paymentFullyDrawn)

    // Rate Fee LLPA
    this.baseOrigFee = Number(
      this.outputs.out.RateFeeLLPA.baseOrigFee.toFixed(2),
    )
    this.baseRate = Number(this.outputs.out.RateFeeLLPA.baseRate)
    this.finalOrigFee = Number(this.outputs.out.RateFeeLLPA.finalOrigFee)
    this.finalRate = Number(this.outputs.out.RateFeeLLPA.finalRate)
    this.LLPAreferallFee = Number(this.outputs.out.RateFeeLLPA.referralFee)
    this.totalOrigFeeLLPA = Number(
      this.outputs.out.RateFeeLLPA.totalOrigFeeLLPA,
    )
    this.totalRateLLPA = Number(this.outputs.out.RateFeeLLPA.totalRateLLPA)
  }

  /**
   * method setInputs()
   *
   * sets the input values returned from the GET existing quote call
   */
  setInputs(): void {
    // Inputs
    // Loan Summary
    this.title = this.currentQuote[0].in.quoteName
    this.address = this.currentQuote[0].in.loanSummary.propertyAddress
    this.zipCode = this.currentQuote[0].in.loanSummary.zip
    this.city = this.currentQuote[0].in.loanSummary.city
    this.product = this.currentQuote[0].in.loanSummary.loanProduct
    this.loanPurpose = this.currentQuote[0].in.loanSummary.loanPurpose
    this.propertyType = this.currentQuote[0].in.loanSummary.propertyType
    this.state = this.currentQuote[0].in.loanSummary.state

    // Borrower Info
    this.borrowingEntity = this.currentQuote[0].in.borrowerInfo.borrowingEntity
    this.buildsCompletedInThreeYears = this.currentQuote[0].in.borrowerInfo.buildsCompletedInThreeYears
    this.combinedLiquidity = Number(
      this.currentQuote[0].in.borrowerInfo.combinedLiquidity,
    )
    this.fnfCompletedInThreeYears = this.currentQuote[0].in.borrowerInfo.fnfCompletedInThreeYears
    this.primaryGuarantor = this.currentQuote[0].in.borrowerInfo.primaryGuarantor
    this.qualifyingFICO = this.currentQuote[0].in.borrowerInfo.qualifyingFICO
    this.rentalUnitsOwned = this.currentQuote[0].in.borrowerInfo.rentalUnitsOwned
    this.secondaryGuarantor = this.currentQuote[0].in.borrowerInfo.secondaryGuarantor
    this.citizen = this.currentQuote[0].in.borrowerInfo.usCitizen

    // Deal Terms
    this.afterCompletedVal = Number(
      this.currentQuote[0].in.dealTerms.afterCompletedVal,
    )
    this.lesserOfLotOrPurchase = Number(
      this.currentQuote[0].in.dealTerms.lesserOfLotOrPurchase,
    )
    this.payoff = Number(this.currentQuote[0].in.dealTerms.payoff)
    this.rehabConBudget = Number(
      this.currentQuote[0].in.dealTerms.rehabConBudget,
    )
    this.targetCloseDate = this.currentQuote[0].in.dealTerms.targetCloseDate
    this.timeCloseToPayoff = Number(
      this.currentQuote[0].in.dealTerms.timeCloseToPayoff,
    )
    this.cashout = this.currentQuote[0].in.dealTerms.cashOut
    this.existingDebtStatus = this.currentQuote[0].in.dealTerms.existingDebtStatus
    this.originalPurchaseDate = this.currentQuote[0].in.dealTerms.originalPurchaseDate
    this.refinance = this.currentQuote[0].in.dealTerms.refinance
    this.rehab = this.currentQuote[0].in.dealTerms.heavyRehab

    // Loan Term
    this.brokerFee = Number(this.currentQuote[0].in.loanTerm.brokerFee)
    this.guaranty = this.currentQuote[0].in.loanTerm.guaranty
    this.channel = this.currentQuote[0].in.loanTerm.channel
    this.loanTerm = this.currentQuote[0].in.loanTerm.loanTerm
    this.referralFee = this.currentQuote[0].in.loanTerm.referralFee
    this.appraisalFee = this.currentQuote[0].in.loanTerm.appraisalFee
    this.brokerProcessingFee = this.currentQuote[0].in.loanTerm.brokerProcessingFee

    // OUTPUTS
    if (this.currentQuote[0].out.validationErrors) {
      this.validationErrors = this.currentQuote[0].out.validationErrors
    }
    this.validQuote = this.currentQuote[0].out.validQuote

    // BlendedLTCCalc
    this.blendedLTCMaxLoan = Number(
      this.currentQuote[0].out.BlendedLTCCalc.blendedLTCMaxLoan,
    )
    this.maxBlendedLTCRatio = Number(
      this.currentQuote[0].out.BlendedLTCCalc.maxBlendedLTCRatio,
    )
    this.totalCost = Number(this.currentQuote[0].out.BlendedLTCCalc.totalCost)

    // Borrower Info
    this.borrowerTier = this.currentQuote[0].out.BorrowerInfo.borrowerTier

    // Cash to Close
    this.conBudget = Number(this.currentQuote[0].out.CashToClose.conBudget)
    this.estimatedCashAtClose = Number(
      this.currentQuote[0].out.CashToClose.estimatedCashAtClose,
    )
    this.estimatedLoanAmt = Number(
      this.currentQuote[0].out.CashToClose.estimatedLoanAmt,
    )
    this.existingPayoff = Number(
      this.currentQuote[0].out.CashToClose.existingPayoff,
    )
    this.purchase = Number(this.currentQuote[0].out.CashToClose.purchase)
    this.totalLoanFeesCost = Number(
      this.currentQuote[0].out.CashToClose.totalLoanFeesCost,
    )

    // LTCCalcOnPurchaseRehab
    this.LTCConBudget = Number(
      this.currentQuote[0].out.LTCCalcOnPurchaseRehab.LTCConBudget,
    )
    this.effectiveBlendedLTC = Number(
      this.currentQuote[0].out.LTCCalcOnPurchaseRehab.effectiveBlendedLTC,
    )
    this.initFundedLoanAmt = Number(
      this.currentQuote[0].out.LTCCalcOnPurchaseRehab.initFundedLoanAmt,
    )
    this.loanAmt = Number(
      this.currentQuote[0].out.LTCCalcOnPurchaseRehab.loanAmt,
    )
    this.maxLTCOnPurchase = Number(
      this.currentQuote[0].out.LTCCalcOnPurchaseRehab.maxLTCOnPurchase,
    )
    // RehabConBudget: any

    // LTCARVCalc
    this.LTVMaxLoan = Number(this.currentQuote[0].out.LTVARVCalc.LTVMaxLoan)
    this.afterCompletedValLTV = Number(
      this.currentQuote[0].out.LTVARVCalc.afterCompletedVal,
    )
    this.maxLTVRatio = Number(this.currentQuote[0].out.LTVARVCalc.maxLTVRatio)
    this.totalLoanAmt = Number(this.currentQuote[0].out.LTVARVCalc.totalLoanAmt)

    // Loan Costs
    this.brokerFeeCosts = Number(this.currentQuote[0].out.LoanCosts.brokerFee)
    this.interestReserve = Number(
      this.currentQuote[0].out.LoanCosts.interestReserve,
    )
    this.originationFee = Number(
      this.currentQuote[0].out.LoanCosts.originationFee,
    )
    this.processingFee = Number(
      this.currentQuote[0].out.LoanCosts.processingFee,
    )
    this.projectInspectionFee = Number(
      this.currentQuote[0].out.LoanCosts.projectInspectionFee,
    )
    this.totalLoanCostFees = Number(
      this.currentQuote[0].out.LoanCosts.totalLoanCostFees,
    )

    // Payments
    this.initMonthlyPayment = Number(
      this.currentQuote[0].out.Payments.initMonthlyPayment,
    )
    this.paymentFullyDrawn = Number(
      this.currentQuote[0].out.Payments.paymentFullyDrawn,
    )

    // Rate Fee LLPA
    this.baseOrigFee = Number(this.currentQuote[0].out.RateFeeLLPA.baseOrigFee)
    this.baseRate = Number(this.currentQuote[0].out.RateFeeLLPA.baseRate)
    this.finalOrigFee = Number(
      this.currentQuote[0].out.RateFeeLLPA.finalOrigFee,
    )
    this.finalRate = Number(this.currentQuote[0].out.RateFeeLLPA.finalRate)
    this.LLPAreferallFee = Number(
      this.currentQuote[0].out.RateFeeLLPA.referralFee,
    )
    this.totalOrigFeeLLPA = Number(
      this.currentQuote[0].out.RateFeeLLPA.totalOrigFeeLLPA,
    )
    this.totalRateLLPA = Number(
      this.currentQuote[0].out.RateFeeLLPA.totalRateLLPA,
    )
  }

  constructor(private route: ActivatedRoute) {
    this.output = false
    this.tierOverride = false
    this.originationOverride = false
    this.rateOverride = false
    this.masterOverride = false
    this.savable = false
    this.id = ''
    this.validationErrors = ''
    this.frontEndValidation = []
    this.validQuote = false
    this.ficoWarning = false
    this.zipCodeWarning = false

    // loan summary
    this.title = ''
    this.loanPurpose = 'Purchase'
    this.product = 'FixNFlip'
    this.address = ''
    this.zipCode = ''
    this.city = ''
    this.state = 'AK'
    this.propertyType = 'SFR'

    // Loan Terms
    this.channel = 'Inside Sales'
    this.loanTerm = '13'
    this.refinance = 'Yes'
    this.referralFee = 'Yes'
    this.appraisalFee = ''
    this.brokerProcessingFee = ''

    this.borrowingEntity = ''
    this.buildsCompletedInThreeYears = ''
    this.combinedLiquidity = ''
    this.fnfCompletedInThreeYears = ''
    this.primaryGuarantor = ''
    this.qualifyingFICO = ''
    this.rentalUnitsOwned = ''
    this.secondaryGuarantor = ''
    this.citizen = 'Yes'

    // Deal Terms
    this.afterCompletedVal = ''
    this.lesserOfLotOrPurchase = ''
    this.rehabConBudget = ''
    this.targetCloseDate = ''
    this.timeCloseToPayoff = ''
    this.existingDebtStatus = 'Purchase'
    this.cashout = 'Yes'

    this.brokerFee = ''
    this.guaranty = 'Full Recourse'

    this.blendedLTCMaxLoan = ''
    this.maxBlendedLTCRatio = ''
    this.totalCost = ''
    this.rehab = 'No'

    this.borrowerTier = ''

    this.conBudget = ''
    this.estimatedCashAtClose = ''
    this.estimatedLoanAmt = ''
    this.existingPayoff = ''
    this.purchase = ''
    this.totalLoanFeesCost = ''

    this.LTCConBudget = ''
    this.effectiveBlendedLTC = ''
    this.initFundedLoanAmt = ''
    this.loanAmt = ''
    this.maxLTCOnPurchase = ''

    this.LTVMaxLoan = ''
    this.afterCompletedValLTV = ''
    this.maxLTVRatio = ''
    this.totalLoanAmt = ''

    this.brokerFeeCosts = ''
    this.interestReserve = ''
    this.originationFee = ''
    this.processingFee = ''
    this.projectInspectionFee = ''
    this.totalLoanCostFees = ''

    this.initMonthlyPayment = ''
    this.paymentFullyDrawn = ''

    this.baseOrigFee = ''
    this.baseRate = ''
    this.finalOrigFee = ''
    this.finalRate = ''
    this.referralFee = 'Yes'
    this.totalOrigFeeLLPA = ''
    this.totalRateLLPA = ''

    this.calcModel = [
      {
        username: 'string',
        quoteType: 'STF',
        overrideMaster: false,
        exceptionPricingOverride: {
          rate: 0,
          tier: '',
          fee: 0,
          enabled: false,
        },
        loanSummary: {
          loanPurpose: '',
          loanProduct: '',
          propertyAddress: '',
          city: '',
          state: '',
          zip: '',
          propertyType: '',
        },
        loanTerm: {
          channel: 'Broker',
          referralFee: '',
          brokerFee: 0,
          loanTerm: '',
          guaranty: '',
        },
        dealTerms: {
          targetCloseDate: '',
          timeCloseToPayoff: 0,
          lesserOfLotOrPurchase: 0,
          rehabConBudget: 0,
          afterCompletedVal: 0,
          refinance: '',
          existingDebtStatus: '',
          cashOut: '',
          heavyRehab: 'No',
        },
        borrowerInfo: {
          primaryGuarantor: '',
          secondaryGuarantor: '',
          borrowingEntity: '',
          fnfCompletedInThreeYears: 0,
          buildsCompletedInThreeYears: 0,
          rentalUnitsOwned: 0,
          qualifyingFICO: 0,
          combinedLiquidity: 0,
          usCitizen: '',
        },
      },
    ]

    this.quoteModel = [
      {
        in: {
          username: '',
          quoteType: 'STF',
          overrideMaster: false,
          exceptionPricingOverride: {
            rate: 0,
            tier: '',
            fee: 0,
            enabled: false,
          },
          loanSummary: {
            loanPurpose: 'Refinance',
            loanProduct: 'FixNFlip',
            propertyAddress: '2 Joseph Mathis Way (Lot 31)',
            city: 'Greenville',
            state: 'SC',
            zip: '29662',
            propertyType: 'SFR',
          },
          loanTerm: {
            channel: 'Broker',
            referralFee: 'Yes',
            brokerFee: 0.25,
            loanTerm: '13',
            guaranty: 'Full Recourse',
          },
          dealTerms: {
            targetCloseDate: '2015-05-23',
            timeCloseToPayoff: 9,
            lesserOfLotOrPurchase: 0,
            rehabConBudget: 0,
            afterCompletedVal: 0,
            refinance: 'No',
            existingDebtStatus: 'Purchase',
            payoff: 100,
            cashOut: 'No',
            heavyRehab: 'No',
          },
          borrowerInfo: {
            primaryGuarantor: 'Reid Hill',
            secondaryGuarantor: 'TBD',
            borrowingEntity: 'TBD',
            fnfCompletedInThreeYears: 30,
            buildsCompletedInThreeYears: 30,
            rentalUnitsOwned: 30,
            qualifyingFICO: 760,
            combinedLiquidity: 1000000,
            usCitizen: 'Yes',
          },
        },
        out: {
          id: '',
          variantId: '',
          validQuote: false,
          workbookId: '01H6UJLMLAWT4HE752N5GKCAEIBSJKOO5V',
          termSheetTitle: 'Fix & Flip Term Sheet',
          validationErrors: '',
          quoteName: '',
          BorrowerInfo: {
            borrowerTier: '',
          },
          BlendedLTCCalc: {
            maxBlendedLTCRatio: 0,
            totalCost: 0,
            blendedLTCMaxLoan: 0,
          },
          LTCCalcOnPurchaseRehab: {
            maxLTCOnPurchase: 0,
            initFundedLoanAmt: 0,
            LTCConBudget: 0,
            loanAmt: 0,
            effectiveBlendedLTC: 0,
          },
          LTVARVCalc: {
            afterCompletedVal: 0,
            maxLTVRatio: 0,
            LTVMaxLoan: 0,
            totalLoanAmt: 0,
          },
          CashToClose: {
            purchase: 0,
            existingPayoff: 0,
            conBudget: 0,
            totalLoanFeesCost: 0,
            estimatedLoanAmt: 0,
            estimatedCashAtClose: 0,
          },
          LoanCosts: {
            brokerFee: 0,
            originationFee: 0,
            processingFee: 0,
            projectInspectionFee: 0,
            totalLoanCostFees: 0,
            interestReserve: 0,
          },
          RateFeeLLPA: {
            baseRate: 0,
            totalRateLLPA: 0,
            finalRate: 0,
            baseOrigFee: 0,
            totalOrigFeeLLPA: 0,
            referralFee: 0,
            finalOrigFee: 0,
          },
          Payments: {
            initMonthlyPayment: 0,
            paymentFullyDrawn: 0,
          },
        },
      },
    ]

    // Set the dropdown options
    this.stateInputs = [
      'AK',
      'AL',
      'AR',
      'AS',
      'AZ',
      'CA',
      'CO',
      'CT',
      'DC',
      'DE',
      'FL',
      'GA',
      'GU',
      'HI',
      'IA',
      'ID',
      'IL',
      'IN',
      'KS',
      'KY',
      'LA',
      'MA',
      'MD',
      'ME',
      'MI',
      'MN',
      'MO',
      'MP',
      'MS',
      'MT',
      'NC',
      'ND',
      'NE',
      'NH',
      'NJ',
      'NM',
      'NV',
      'NY',
      'OH',
      'OK',
      'OR',
      'PA',
      'PR',
      'RI',
      'SC',
      'SD',
      'TN',
      'TX',
      'UM',
      'UT',
      'VA',
      'VI',
      'VT',
      'WA',
      'WI',
      'WV',
      'WY',
    ]
    this.loanPurposeInput = ['Purchase', 'Refinance']
    this.propertyTypeOptions = ['SFR', '2-4 Unit', 'Condo']
    this.loanProductOptions = [
      'FixNFlip',
      'BridgePlus',
      'New Construction',
      'Fix2Rent',
    ]
    this.channelInputs = [
      'Inside Sales',
      'Broker',
      'Business Development',
      'Commercial',
    ]
    this.referralFeeInputs = ['Yes', 'No']
    this.loanTermInputs = ['13', '19']
    this.refinanceOptions = ['Yes', 'No']
    this.existingDebtInputs = [
      'Purchase',
      'Free & Clear',
      'Payoff Existing Mortgage',
    ]
    this.guarantyOptions = ['Full Recourse', 'Non-Recourse']
    this.citizenInputs = ['Yes', 'No']
    this.cashoutInputs = ['Yes', 'No']

    this.loading = false
    this.savable = false
    this.printable = false
    this.CurrentDate = new Date()
    this.username = this.route.snapshot.params.username

    if (this.route.snapshot.params.id) {
      this.id = this.route.snapshot.params.id
      this.getQuoteBackEnd()
      this.output = true
      this.printable = true
      this.savable = true
    }

    this.whiteLabelButton = false
    this.whiteLabel = false
  }
}
