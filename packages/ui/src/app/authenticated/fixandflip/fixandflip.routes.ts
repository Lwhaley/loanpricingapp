import { Routes } from '@angular/router'
import { FixandflipComponent } from './fixandflip.component'

export const routes: Routes = [
  {
    path: 'new/:username',
    component: FixandflipComponent,
  },
  {
    path: 'new/:id/:username',
    component: FixandflipComponent,
  },
]
