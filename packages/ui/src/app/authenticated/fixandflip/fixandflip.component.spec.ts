import { ComponentFixture, TestBed } from '@angular/core/testing'

import { FixandflipComponent } from './fixandflip.component'

describe('FixandflipComponent', () => {
  let component: FixandflipComponent
  let fixture: ComponentFixture<FixandflipComponent>

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [FixandflipComponent],
    }).compileComponents()
  })

  beforeEach(() => {
    fixture = TestBed.createComponent(FixandflipComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
