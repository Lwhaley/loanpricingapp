import { CommonModule } from '@angular/common'
import { NgModule } from '@angular/core'
import { FooterComponent } from './components/footer/footer.component'
import { LogoComponentComponent } from './components/logo-component/logo-component.component'
import { SpinnerComponent } from './components/spinner/spinner.component'

@NgModule({
  declarations: [FooterComponent, LogoComponentComponent, SpinnerComponent],
  imports: [CommonModule],
  exports: [FooterComponent, LogoComponentComponent, SpinnerComponent],
})
export class SharedModule {}
