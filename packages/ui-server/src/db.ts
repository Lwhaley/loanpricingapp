import pg from 'pg'

const config = {
  host: process.env.DB_HOST || 'localhost',
  user: process.env.DB_USERNAME || 'virgil',
  password: process.env.DB_PASSWORD || '',
  database: process.env.DB_NAME || 'virgil',
  port: Number(process.env.DB_PORT) || 5432,
  ssl: false,
}

const queryDatabase = async (query: string) => {
  try {
    const client = new pg.Client(config)
    await client.connect()
    const result = await client.query(query)
    await client.end()
    return result
  } catch (e) {
    throw e
  }
}

export const readFNFQuotes = async () => {
  const query = 'SELECT * FROM fnf_quotes;'

  const quotes = await queryDatabase(query)

  // FIXME: remove console.log and add logic
  // tslint:disable-next-line: no-console
  return quotes.rows
}
