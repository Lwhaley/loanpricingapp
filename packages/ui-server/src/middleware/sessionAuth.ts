import { ExpressOIDC } from '@okta/oidc-middleware'
import expressSession from 'express-session'

// Production
// const appBaseUrl = 'https://pricingprod.azurewebsites.net'
// const clientId = '0oa94sd9dkX1hhXNE357'
// const clientSecret = 'WLBrXfnwZTtAo-38wuw4f_pIjDOlZcbyj-ymTG8E'

// QA
// const appBaseUrl = 'https://pricingqa.azurewebsites.net'
// const clientId = '0oa94l41f2JGMGBmH357'
// const clientSecret = 'aCEot4dkf8QSTPHZ3IVVBjvACDRljjKub06Xd5jk'

// UAT
// const appBaseUrl = 'https://pricinguat.azurewebsites.net'
// const clientId = '0oa9u22y6h2U26ey2357'
// const clientSecret = 'dPAT7-aVVXPuenk3EsQlnxMhK5g8VNQ-caXdjlkE'

// Local
const appBaseUrl = 'http://localhost:3000'
const clientId = '0oa9crkfv54tx5J9T357'
const clientSecret = 'aH_gE7i4dAvCZaEIZdbK2a9wRbnqVqrmU1bsf2Hm'

export const register = (app: any) => {
  // Create the OIDC client
  // `${ process.env.OKTA_CLIENT_ID}`,
  // `${process.env.OKTA_CLIENT_SECRET}`,
  const oidc = new ExpressOIDC({
    appBaseUrl,
    client_id: `${clientId}`,
    client_secret: `${clientSecret}`,
    issuer: 'https://limaone.okta.com',
    redirect_uri: `${appBaseUrl}/authorization-code/callback`,
    scope: 'openid profile',
  })

  // Configure Express to use authentication sessions
  app.use(
    expressSession({
      resave: true,
      saveUninitialized: false,
      secret: `${process.env.SESSION_SECRET}`,
    }),
  )

  // Configure Express to use the OIDC client router
  app.use(oidc.router)

  // add the OIDC client to the app.locals
  app.locals.oidc = oidc
}
