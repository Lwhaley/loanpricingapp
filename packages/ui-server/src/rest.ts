import axios from 'axios'
import { json } from 'body-parser'

// Production
// const url = 'https://pricingengineprod.us-e2.cloudhub.io/api/'
// const clientId =
// const clientSecret =

// QA
// const url = 'https://pricingengine.us-e2.cloudhub.io/api/'
// const clientId = '96a9cbaf5a8147668a9d8388efcf2997'
// const clientSecret = '23C1d378842F46e7A0Ac34BC0C20Ba14'

// UAT
const url = 'https://pricingengineuat.us-e2.cloudhub.io/api/'
const clientId = '14e1ed2555f249d9b051a51c7ae31799'
const clientSecret = '266765Af480c4cd8965CC20F5F86dd2e'

axios.interceptors.request.use(
  (config: any) => {
    return config
  },
  (error: any) => {
    return Promise.reject(error)
  },
)

axios.interceptors.response.use(
  (response: any) => {
    return response
  },
  (error: any) => {
    return Promise.reject(error)
  },
)

// POST quote summaries
export const quoteSummaries = async (id: any) => {
  try {
    const results = await axios({
      data: JSON.stringify(id),
      method: 'POST',
      url: `${url}quoteSummaries`,
      headers: {
        client_id: clientId,
        client_secret: clientSecret,
        'Content-Type': 'application/json',
      },
    })
    return results
  } catch (error) {
    if (error.response) {
      // tslint:disable-next-line: no-console
      console.log(error.response.data)
      // tslint:disable-next-line: no-console
      console.log(error.response.status)
      // tslint:disable-next-line: no-console
      console.log(error.response.headers)
    } else if (error.request) {
      // tslint:disable-next-line: no-console
      console.log(error.request)
    } else {
      // tslint:disable-next-line: no-console
      console.log('Error', error.message)
    }
    // tslint:disable-next-line: no-console
    console.log(error.config)
  }
}

// GET quote by id
export const getQuote = async (id: any) => {
  try {
    const result = await axios({
      method: 'GET',
      url: `${url}quotes/${id}`,
      headers: {
        client_id: clientId,
        client_secret: clientSecret,
        'Content-Type': 'application/json',
      },
    })
    return result
  } catch (error) {
    if (error.response) {
      // tslint:disable-next-line: no-console
      console.log(error.response.data)
      // tslint:disable-next-line: no-console
      console.log(error.response.status)
      // tslint:disable-next-line: no-console
      console.log(error.response.headers)
    } else if (error.request) {
      // tslint:disable-next-line: no-console
      console.log(error.request)
    } else {
      // tslint:disable-next-line: no-console
      console.log('Error', error.message)
    }
    // tslint:disable-next-line: no-console
    console.log(error.config)
  }
}

// POST do calculate
export const runCalc = async (req: any) => {
  try {
    const quoteResult = await axios({
      method: 'POST',
      url: `${url}calculations`,
      data: req,
      headers: {
        client_id: clientId,
        client_secret: clientSecret,
        'Content-Type': 'application/json',
      },
    })
    return quoteResult
  } catch (error) {
    if (error.response) {
      // tslint:disable-next-line: no-console
      console.log(error.response.data)
      // tslint:disable-next-line: no-console
      console.log(error.response.status)
      // tslint:disable-next-line: no-console
      console.log(error.response.headers)
    } else if (error.request) {
      // tslint:disable-next-line: no-console
      console.log(error.request)
    } else {
      // tslint:disable-next-line: no-console
      console.log('Error', error.message)
    }
    // tslint:disable-next-line: no-console
    console.log(error.config)
  }
}

// POST save quote
export const saveQuote = async (req: any) => {
  try {
    const saveResult = await axios({
      method: 'POST',
      url: `${url}quotes`,
      data: req,
      headers: {
        client_id: clientId,
        client_secret: clientSecret,
        'Content-Type': 'application/json',
      },
    })
    return saveResult
  } catch (error) {
    if (error.response) {
      // tslint:disable-next-line: no-console
      console.log(error.response.data)
      // tslint:disable-next-line: no-console
      console.log(error.response.status)
      // tslint:disable-next-line: no-console
      console.log(error.response.headers)
    } else if (error.request) {
      // tslint:disable-next-line: no-console
      console.log(error.request)
    } else {
      // tslint:disable-next-line: no-console
      console.log('Error', error.message)
    }
    // tslint:disable-next-line: no-console
    console.log(error.config)
  }
}

// PUT update quote
export const updateQuote = async (id: any, req: any) => {
  try {
    const updateResult = await axios({
      method: 'PUT',
      url: `${url}quotes/${id}`,
      data: req,
      headers: {
        client_id: clientId,
        client_secret: clientSecret,
        'Content-Type': 'application/json',
      },
    })
    return updateResult
  } catch (error: any) {
    if (error.response) {
      // tslint:disable-next-line: no-console
      console.log(error.response.data)
      // tslint:disable-next-line: no-console
      console.log(error.response.status)
      // tslint:disable-next-line: no-console
      console.log(error.response.headers)
    } else if (error.request) {
      // tslint:disable-next-line: no-console
      console.log(error.request)
    } else {
      // tslint:disable-next-line: no-console
      console.log('Error', error.message)
    }
    // tslint:disable-next-line: no-console
    console.log(error.config)
  }
}
