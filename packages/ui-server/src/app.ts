import express from 'express'
import bodyParser from 'body-parser'
import path from 'path'
import morgan from 'morgan'
import * as sessionAuth from './middleware/sessionAuth'

import {
  getQuote,
  quoteSummaries,
  runCalc,
  saveQuote,
  updateQuote,
} from './rest'

const url = 'https://pricingengine.us-e2.cloudhub.io/api/'
const clientId = '96a9cbaf5a8147668a9d8388efcf2997'
const clientSecret = '23C1d378842F46e7A0Ac34BC0C20Ba14'

const app = express()
const port = process.env.PORT || 3000

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))

app.use(morgan('dev'))

// CORS for dev
app.use((_req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*')
  res.header(
    'Access-Control-Allow-Headers',
    'Authorization, Origin, Content-Type, Accept',
  )
  next()
})

// Configure session auth
sessionAuth.register(app)
const oidc = app.locals.oidc

app.use(
  oidc.router,
  oidc.ensureAuthenticated(),
  express.static(path.join(__dirname, '../front_end/')),
)

app.get('/logout', (req: any, res: any) => {
  req.logout()
  res.redirect('/')
})

// TEST API CALL
app.get('/api/v1', oidc.ensureAuthenticated(), (_req, res) =>
  res.send({ res: 'hello world' }),
)

// Authenticated Routes
// PUT API CALL FOR UPDATING QUOTE
app.post('/api/v1/quotes/:id', oidc.ensureAuthenticated(), async (req, res) => {
  try {
    const update = await updateQuote(req.params.id, req.body)
    res.send({ res: update?.data })
  } catch (e) {
    res.send({ res: e })
  }
})

// POST API CALL FOR SAVING QUOTE
app.post('/api/v1/quotes', oidc.ensureAuthenticated(), async (req, res) => {
  try {
    const quote = await saveQuote(req.body)
    res.send({ res: quote?.data })
  } catch (e) {
    res.send({ res: e })
  }
})

// GET API CALL FOR QUOTE BY ID
app.get('/api/v1/quote/:id', oidc.ensureAuthenticated(), async (req, res) => {
  try {
    const quote = await getQuote(req.params.id)
    res.send({ res: quote?.data })
  } catch (e) {
    // do nothing
  }
})

// POST API CALL FOR ALL USER QUOTES
app.post(
  '/api/v1/quoteSummaries',
  oidc.ensureAuthenticated(),
  async (req: any, res) => {
    req.body.username = req.userContext.userinfo.preferred_username
    const quotes = await quoteSummaries(req.body)
    const returnObject = [
      {
        username: req.userContext.userinfo.preferred_username,
        quotes: quotes?.data,
      },
    ]
    res.send({ res: returnObject })
  },
)

// POST API CALL FOR CALCULATION
app.post(
  '/api/v1/calculations',
  oidc.ensureAuthenticated(),
  async (req, res) => {
    try {
      const calculation = await runCalc(req.body)
      res.send({ res: calculation?.data })
    } catch (e) {
      res.send({ res: e })
    }
  },
)

// Send all other requests to the Angular app
app.get('*', (_req, res) => {
  res.sendFile(path.join(__dirname, '../front_end/index.html'))
})

// tslint:disable-next-line: no-console
app.listen(port, () => console.log(`Example app listening on ${port}`))
