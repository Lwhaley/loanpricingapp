# LoanPricingApp

This applications is setup in a monorepo using [lerna](https://github.com/lerna/lerna).

Lerna bootstrap will handle installing dependencies for development for the relevant packages.

## Developer Setup

1. install nodenv
	- https://github.com/nodenv/nodenv#homebrew-on-macos
	- nodenv install 14.15.4
2. To install dependencies: `npm run bootstrap`
3. To start a dev app: `npm run start:watch`
4. For running prod builds: `npm run build:prod && npm start`

## ui-server

The application server is developed using [Node.js](https://nodejs.org/en/).

The server will be hosted at [http://localhost:3000](http://localhost:3000)

## ui

The UI is developed using [Angular](https://angular.io/).

The UI is application will be hosted at [http://localhost:4200](http://localhost:4200)
